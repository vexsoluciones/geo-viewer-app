angular.module('controller.publish', [])

.controller('PublishCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,TwitterFactory,FacebookFactory,ApiUrl,FileFactory,InstagramFactory,UserAccountFactory,$cordovaOauth,LoginFaceFactory,CameraFactory,
  $cordovaFileTransfer,$cordovaGeolocation,$cordovaFacebook,UserMediaFactory) {

  $scope.user = UserService.getUser();
  
  $scope.vStatus = true;
  $scope.vPhoto = false;
  $scope.vUpload = true;

  var myDropzone = null;

  $scope.publish = {
    message : '',
    status_photo : '',
    photos : '',
    lat: '',
    long: ''
  };

  $scope.upload = {
    image : ''
  };

  $scope.chk = {
    facebook : false,
    twitter : false
  };

  $scope.disabled = {
    facebook : true,
    facebookA : false,
    twitter : true,
    twitterA : false
  };

  var vImages = [];

  var init = function(){

    myDropzone = new Dropzone("#photo-upload", { 
      url: ApiUrl+"/file/upload",
      addRemoveLinks : true,
      dictRemoveFile : "Delete",
      maxFiles : 1,
      dictDefaultMessage : 'Select your photo'
    });

    myDropzone.on("addedfile", function(file) {
      vImages.push(file.name);
    });

    myDropzone.on("removedfile", function(file) {

      FileFactory.deleted(file.name).success(function(data, status, headers, config) {

      });

    });

    if (localStorage.urlIntent) {

      uploadPhoto(localStorage.urlIntent);

    }

    listAccountAsociate();

  };

  var listAccountAsociate = function(){

     UserAccountFactory.getAllActive($scope.user.user_id).success(function(data, status, headers, config) {

        angular.forEach(data, function(item){

            if (item.socialAccount == "facebook") {
              $scope.user.id_face = item.idPerfil;
              $scope.user.fb_token = item.accessToken;
              $scope.chk.facebook = true; 

              $scope.disabled.facebook = false;
              $scope.disabled.facebookA = true;
 
            }/*else{
              $scope.chk.facebook = false; 
              $scope.disabled.facebookA = false;
              $scope.disabled.facebook = true;
            }*/

            if (item.socialAccount == "twitter") {

            	console.log(item.socialAccount);

              $scope.user.id_twit = item.idPerfil;
              $scope.user.twit_token= item.accessToken;
              $scope.chk.twitter = true;

              $scope.disabled.twitter = false;
              $scope.disabled.twitterA = true;

            }/*else{
              $scope.chk.twitter = false;
              $scope.disabled.twitterA = false;
              $scope.disabled.twitter = true;
            }*/

            if (item.socialAccount == "instagram") {
              $scope.user.id_inst = item.idPerfil;
              $scope.user.insta_token= item.accessToken; 
            }



        });

		UserService.setUser($scope.user);

     });
  };

  $scope.asociarCuenta = function(social){

    var vDataPerfil = {
      id_perfil: $scope.user.id,
      access_token : '',
      email: $scope.user.email,
      picture : ''
    };

    switch(social) {
      case 'facebook':

        $cordovaOauth.facebook(FACEBOOK_APP_ID, 
          ['email',
          'public_profile',
          'user_location',
          'user_friends',
          'user_photos',
          ],{redirect_uri: "https://www.facebook.com/connect/login_success.html"}).then(function(result) {

            LoginFaceFactory.getFacebookProfileInfo(result.access_token).success(function(resultData) {

             vDataPerfil.id_perfil = resultData.id;
             vDataPerfil.access_token = result.access_token;
             vDataPerfil.email = resultData.email;
             vDataPerfil.picture = "http://graph.facebook.com/"+ resultData.id +"/picture?type=large";

             $scope.user.id_face = resultData.id;
             $scope.user.fb_token = result.access_token;
             $scope.user.emailFace = resultData.email;

             UserService.setUser($scope.user);

             asociar(social,vDataPerfil);

           });

          }, function(error) {

          });
      
      break;

      case 'twitter':

        var clientId = 'z3W5b6lvxAHNrueE1iREpJMxQ';
        var clientSecret = 'k0vab1S1drvsLxeryDF8BWR0gqy7Kr2Ct3Ppoyi7zSFg3unax1';

        $cordovaOauth.twitter(clientId,clientSecret,{redirect_uri: "https://tinyurl.com/krmpchb"}).then(function(result) {
          var vParams = {
            user_id : result.user_id,
            access_token : result.oauth_token
          };

          TwitterFactory.getPerfilData(vParams).success(function(data) {

            var vData = {
              name : data.name,
              first_name : data.name,
              last_name : '',
              id_twit : data.id,
              picture : data.profile_image_url.replace('_normal', ''),
              id : data.id,
              email : data.id +'@tada.com',
              access_token : result.oauth_token,
              twit_token : result.oauth_token,
              token_secret : result.oauth_token_secret
            };

            vDataPerfil.id_perfil = data.id;
            vDataPerfil.access_token = result.oauth_token;
            vDataPerfil.email = vData.email;
            vDataPerfil.picture = vData.picture;

            UserService.setUser(vData);

            asociar(social,vDataPerfil);

          });

        }, function(error) {
          alert(JSON.stringify(error));
          $ionicLoading.hide();
        });
      
      break;

      case 'instagram':
      break;
    
    }
  };

  var asociar = function(social,dataPerfil){

    var vDataValida = {
      user_id : $scope.user.user_id,
      social : social
    };

    UserAccountFactory.associateAccount(vDataValida,dataPerfil).then(function(response){

      if (response === 0 || response == "0"){

        $ionicLoading.show({ template: "Account is registered", noBackdrop: true, duration: 2000 });

      }else{

        switch(social) {
          case 'facebook':
          $scope.disabled.facebook = false;
          $scope.disabled.facebookA = true;
          $scope.chk.facebook = true;
          break;

          case 'twitter':
          $scope.disabled.twitter = false;
          $scope.disabled.twitterA = true;
          $scope.chk.twitter = true;
          break;

          case 'instagram':
          break;

        }

      }

    });
  };


  $scope.publishStatus = function(){

    if(vImages.length > 0 && $scope.publish.status_photo !== ""){

      if($scope.chk.facebook === true || $scope.chk.twitter === true){

        $ionicLoading.show({
          template: 'Publishing...'
        });

        if(typeof($scope.user.id_face) != "undefined" && $scope.user.id_face !=="" && $scope.chk.facebook === true){
          publishStatusFacebook();
        }

        setTimeout(function(){
          if(typeof($scope.user.id_twit) != "undefined" && $scope.user.id_twit !=="" && $scope.chk.twitter === true){
            publishStatusTwitter();
          }

          $ionicLoading.hide();

          setTimeout(function(){

            $ionicLoading.show({
              template: 'Published Successfully'
            });

            setTimeout(function(){

              deleteImages();

              resetForm();

              $ionicLoading.hide();

              //$state.go('app.publish','',{ reload: true });

            },3000);

          },2500);

        },11500);

      }else{

        $ionicLoading.show({
          template: 'Select at least 1 network to publish'
        });

        setTimeout(function(){
          $ionicLoading.hide();
        },5000);

      }

    }else{

      $ionicLoading.show({
        template: 'Select image and insert text'
      });

      setTimeout(function(){
        $ionicLoading.hide();
      },5000);

    }
  };

  var resetForm = function(){

    myDropzone.removeAllFiles();

    $scope.vUpload = true;
    $scope.vPhoto = false;
    $scope.upload.image = '';
    $scope.publish.status_photo = '';
  };

  var deleteImages = function(){

      angular.forEach(vImages, function(item){

        FileFactory.deleted(item);
              
      });

      vImages.length = 0;
  };

  var publishStatusFacebook = function(){

    $scope.publish.photos = vImages;
    $scope.publish.access_token = $scope.user.fb_token;
    $scope.publish.user_id = $scope.user.id_face;

    FacebookFactory.publishFeed($scope.publish).success(function(data, status, headers, config) {

      var vOptions = {timeout : 10000 , enableHighAccuracy : false};

      $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

        var vDataMedia = {
          user_id : $scope.user.user_id,
          media_id : data,
          social : 'facebook',
          lat : position.coords.latitude,
          lng : position.coords.longitude
        };

        UserMediaFactory.addMedia(vDataMedia).success(function(data){

          console.log("se registro la data" + JSON.stringify(vDataMedia));

        });

      },function(){});

    });

  };

  var publishStatusTwitter = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

      $scope.publish.photos = vImages;
      $scope.publish.access_token = $scope.user.twit_token;
      $scope.publish.access_token_secret = $scope.user.token_secret;

      if ($scope.publish.lat === "") {

        $scope.publish.lat = position.coords.latitude;
        $scope.publish.long = position.coords.longitude;
      }

      TwitterFactory.publishStatus($scope.publish);

    });

  };

  $scope.publishStatusInstagram = function(){

    $scope.publish.photos = vImages;

    console.log(JSON.stringify($scope.publish));

    InstagramFactory.publishStatus($scope.publish).success(function(data, status, headers, config) {

      console.log("res: "+JSON.stringify(data));

      angular.forEach(vImages, function(item){

        FileFactory.deleted(item);

      });

      vImages.length = 0;


    });
  };

  $scope.FacebookOptions = function() {
    if($scope.vfacebook === false){
      $scope.vfacebook = true;
    }else{
      $scope.vfacebook = false;
    }
  };

  $scope.TwitterOptions = function() {
    if($scope.vtwitter === false){
      $scope.vtwitter = true;
    }else{
      $scope.vtwitter = false;
    }
  };

  $scope.InstagramOptions = function() {
    if($scope.vinstagram === false){
      $scope.vinstagram = true;
    }else{
      $scope.vinstagram = false;
    }
  };

  $scope.capturePhoto = function(){

    CameraFactory.getPicture({
      quality: 85,
      targetWidth: 620,
      targetHeight: 620,
      saveToPhotoAlbum: false
    }).then(function(imageURI) {
    
      uploadPhoto(imageURI);
    }, function(err) {
      alert(err);
    });
  };

  var uploadPhoto = function(uriPhoto){

    var options = {
        fileKey: "file",
        fileName: uriPhoto.split("/").pop(),
        chunkedMode: true,
        mimeType: "image/jpg"
    };

    $cordovaFileTransfer.upload(ApiUrl+"/file/upload", uriPhoto,options)
      .then(function(result) {

        $scope.vPhoto = true;
        $scope.vUpload = false;

        if (localStorage.urlIntent) {

          window.FilePath.resolveNativePath(uriPhoto, 

            function(data){

              $scope.upload.image = data;

            }, function(error){

              alert("error " + error);

            }
          );

          localStorage.removeItem("urlIntent");

        }else{

          $scope.upload.image = uriPhoto;

          CordovaExif.readData(uriPhoto, 
            function(exifObject) {

              if (exifObject !==undefined && exifObject.GPSLatitude) {

                $scope.publish.lat = exifObject.GPSLatitude;
                $scope.publish.long = exifObject.GPSLongitude;

              }
            },function(error){
              //alert("error " + error);
            }
          );
        }

        vImages.push(options.fileName);
        
      }, function(err) {
         //alert("error" + err);
      }, function (progress) {
        // constant progress updates
      });
  };

  init();
})

;