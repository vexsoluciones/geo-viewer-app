angular.module('controller.app', [])

.controller('AppCtrl', function($scope, $state, $ionicPopup, UserService, $ionicLoading, FACEBOOK_APP_ID,$rootScope,
  $ionicHistory,$cordovaFacebook) {
   
   $scope.user = UserService.getUser();

   // A confirm dialog to be displayed when the user wants to log out
   $scope.showConfirmLogOut = function() {
     var confirmPopup = $ionicPopup.confirm({
       title: 'Log out',
       template: 'Are you sure you want to log out?'
     });
     confirmPopup.then(function(res) {
       if(res) {
         //logout
         $ionicLoading.show({
           template: 'Loging out...'
         });

          UserService.deleteUser();

          $rootScope.auth = false;

          $cordovaFacebook.logout();
      
          $ionicHistory.clearHistory();
          $ionicHistory.clearCache();

          $ionicLoading.hide();
          $state.go('login');
  
       }
     });
   };

})

.controller('InviteCtrl', function($scope, $state, UserService,FacebookFactory,$cordovaFacebook) {


  $scope.inviteFacebookContact = function(){

     FacebookFactory.inviteFriends();

  };

  $scope.inviteFacebookContact();


})

;