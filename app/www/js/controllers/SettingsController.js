angular.module('controller.settings', [])

.controller('SettingsCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,$http,ApiUrl,
  UserFactory,LoginFaceFactory,UserAccountFactory,TwitterFactory,InstagramFactory,$ionicPopup,$rootScope,$cordovaOauth) {

  $scope.user = UserService.getUser(); 

  $scope.socialButton = {
    facebook : '',
    twitter : '',
    instagram : ''
  };

  $scope.userPass = {
    new_pass: '',
    user_pass: '',
    current_pass: '',
    valida_new_pass: '',
    valida_user_pass: ''
  };

  var vValChangePass = 0;

  $scope.showAlertSocialAccount = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'Alert',
       template: 'Associate social networks'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
  };

  $scope.changeVisibleRadius = function(){
  	document.getElementById('label-radius').innerHTML = $scope.user.visible_radius;
  };

  $scope.getSocialAccount = function(){

    UserAccountFactory.getAllActive($scope.user.user_id).success(function(data, status, headers, config) {

       angular.forEach(data, function(item){

          if (item.socialAccount == "facebook") {

            $scope.socialButton.facebook = true;

            $scope.user.fb_token = item.accessToken;
            $scope.user.id_face = item.idPerfil;
            $scope.user.emailFace = item.email;
          }

          if (item.socialAccount == "twitter") {
            
            $scope.socialButton.twitter = true;

            $scope.user.twit_token = item.accessToken;
            $scope.user.id_twit = item.idPerfil;
            $scope.user.emailTwit = item.email; 
          }

          if (item.socialAccount == "instagram") {
            
            $scope.socialButton.instagram = true;

            $scope.user.insta_token = item.accessToken;
            $scope.user.id_inst = item.idPerfil;
            $scope.user.emailInsta = item.email;
          }

       });
    });
  };

  $scope.pushRedSocialChange = function(social){

    var vDataPerfil = {
      id_perfil: $scope.user.id,
      access_token : '',
      email: $scope.user.email,
      picture : ''
    };

    switch(social) {
      case "facebook":

        if ($scope.socialButton.facebook === true) {

          $ionicLoading.show({
            template: 'associating ...'
          });

          if($rootScope.social == 'facebook'){

            vDataPerfil.access_token = $scope.user.fb_token;
            vDataPerfil.id_perfil = $scope.user.id_face;
            vDataPerfil.picture = "http://graph.facebook.com/"+$scope.user.id_face +"/picture?type=large";

            if(typeof($scope.user.emailFace) != "undefined"){

              vDataPerfil.email = $scope.user.emailFace;

            }

            asociarCuentaSocial('facebook',vDataPerfil);

          }else{

            $cordovaOauth.facebook(FACEBOOK_APP_ID, 
              ['email',
              'public_profile',
              'user_location',
              'user_friends',
              'user_photos',
              'user_posts'],{redirect_uri: "https://www.facebook.com/connect/login_success.html"}).then(function(result) {

              LoginFaceFactory.getFacebookProfileInfo(result.access_token).success(function(resultData) {

                 vDataPerfil.id_perfil = resultData.id;
                 vDataPerfil.access_token = result.access_token;
                 vDataPerfil.email = resultData.email;
                 vDataPerfil.picture = "http://graph.facebook.com/"+ resultData.id +"/picture?type=large";

                 $scope.user.id_face = resultData.id;
                 $scope.user.fb_token = result.access_token;
                 $scope.user.emailFace = resultData.email;

                 UserService.setUser($scope.user);

                 asociarCuentaSocial('facebook',vDataPerfil);

              }).error(function(error){
                $ionicLoading.hide();

                $scope.socialButton.facebook = false;

                $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
              });

            }, function(error) {
              $ionicLoading.hide();

              $scope.socialButton.facebook = false;

              $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
            });

          }

        }else{

          $ionicLoading.show({
            template: 'Disassociating ...'
          });

          if(typeof($scope.user.emailFace) != "undefined"){

            vDataPerfil.id_perfil = $scope.user.id_face;
            vDataPerfil.email = $scope.user.emailFace;
            vDataPerfil.picture = "http://graph.facebook.com/"+$scope.user.id_face +"/picture?type=large";

          }

          asociarCuentaSocial('facebook',vDataPerfil);

        }

      break;

      case "twitter":

        if ($scope.socialButton.twitter === true) {

          $ionicLoading.show({
            template: 'Associating. ...'
          });

          if ($rootScope.social == "twitter") {
              vDataPerfil.access_token = $scope.user.twit_token;  

              asociarCuentaSocial('twitter',vDataPerfil);       
   
          }else{

            var clientId = 'z3W5b6lvxAHNrueE1iREpJMxQ';
            var clientSecret = 'k0vab1S1drvsLxeryDF8BWR0gqy7Kr2Ct3Ppoyi7zSFg3unax1';

            $cordovaOauth.twitter(clientId,clientSecret,{redirect_uri: "https://tinyurl.com/krmpchb"}).then(function(result) {
              var vParams = {
                user_id : result.user_id,
                access_token : result.oauth_token
              };

              TwitterFactory.getPerfilData(vParams).success(function(data) {

                vDataPerfil.access_token = vParams.access_token;
                vDataPerfil.id_perfil = data.id;
                vDataPerfil.email = data.id +'@tada.com';

                $scope.user.twit_token = vParams.access_token;
                $scope.user.id_twit = data.id;
                $scope.user.emailTwit = data.id +'@tada.com';

                UserService.setUser($scope.user);

                asociarCuentaSocial('twitter',vDataPerfil);

              }).error(function(error){

                $ionicLoading.hide();

                error = error || 'Twitter login problems. Try again.';

                $scope.socialButton.twitter = false;
                $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });

              });

            }, function(error) {
              error = error || 'Twitter login problems. Try again.';

              $scope.socialButton.twitter = false;
              $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
            });


          }

        }else{

          $ionicLoading.show({
            template: 'Disassociating ...'
          });

          vDataPerfil.id_perfil = $scope.user.id_twit;
          vDataPerfil.email = $scope.user.emailTwit;

          asociarCuentaSocial('twitter',vDataPerfil);

        }
       
      break;

      case "instagram":

        if ($scope.socialButton.instagram === true) {

          $ionicLoading.show({
            template: 'Aassociating ...'
          });

          if ($rootScope.social == "instagram") {

            vDataPerfil.access_token = $scope.user.insta_token;  
            asociarCuentaSocial('instagram',vDataPerfil);  

          }else{

            $cordovaOauth.instagram("3d2337eca9ed4277a07962a821579891", ['likes','relationships']).then(function(result) {

              var vAccessToken = result.access_token;

              var vPos = vAccessToken.indexOf('.');
              var vId = vAccessToken.substring(0,vPos);

              var vParams = {
                user_id : vId,
                access_token : vAccessToken
              };

              InstagramFactory.getUserDataLogin(vParams).success(function(response) {

                vDataPerfil.access_token = vAccessToken;
                vDataPerfil.id_perfil = response.data.id;
                vDataPerfil.email = response.data.id +'@tada.com';

                $scope.user.twit_token = vAccessToken;
                $scope.user.id_inst = response.data.id;
                $scope.user.emailInsta = response.data.id +'@tada.com';

                UserService.setUser($scope.user);
                asociarCuentaSocial('instagram',vDataPerfil);

              });

            }, function(error) {
              $ionicLoading.hide();

              $scope.socialButton.instagram = false;

              $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
            });

          }     
   
        }else{

          $ionicLoading.show({
            template: 'Disassociating ...'
          });
       
          vDataPerfil.id_perfil = $scope.user.id_inst;
          vDataPerfil.email = $scope.user.emailInsta;
          
          asociarCuentaSocial('instagram',vDataPerfil);
        }

     
      break;
      
    }
  };

  var asociarCuentaSocial = function(social,dataPerfil){

      var vDataValida = {
        user_id : $scope.user.user_id,
        social : social
      };

      UserAccountFactory.associateAccount(vDataValida,dataPerfil).then(function(response){

        $ionicLoading.hide();

        if (response === 0){

          switch(social) {
            case "facebook":
              $scope.socialButton.facebook = false;
            break;

            case "twitter":
              $scope.socialButton.twitter = false;
            break;

            case "instagram":
              $scope.socialButton.instagram = false;
            break;
            
          }

          $ionicLoading.show({ template: "Account is registered", noBackdrop: true, duration: 2000 });
        }

      
      });
  };
  
  $scope.updateData = function(formData){

    var userData = {
        first_name: $scope.user.first_name,
        last_name:  $scope.user.last_name,
        user_email: $scope.user.email,
        user_pass:  $scope.user.current_pass,
        fb_token:   '',
        insta_token: '',
        twit_token: '',
        visible_radius : $scope.user.visible_radius,
        status: '',
        id: $scope.user.user_id
    };

    if (formData.$valid) {

      if($scope.userPass.new_pass !== ""){

          userData.user_pass = $scope.userPass.new_pass;
      }

      if ($scope.userPass.new_pass == $scope.userPass.user_pass) {

        $ionicLoading.show({
          template: 'Saved changes ...'
        });

        UserFactory.update(userData).success(function(data, status, headers, config) {

          $scope.user.name = userData.first_name +" "+ userData.last_name;
          //$scope.user.current_pass = userData.user_pass;
          
          UserService.setUser($scope.user);

          $ionicLoading.hide();

          if (vValChangePass == 1) {

            $ionicPopup.alert({
             title: 'Success!',
             template: 'Password Changed Successfully'
            });

          }

          $state.go('app.settings',{ reload: true });

        });

      }else{

       $ionicPopup.alert({
         title: 'Alert!',
         template: 'Error password '
       });

     }

    }
  };

  $scope.currentPass = function(vCurrentPass){

    if (vCurrentPass != $scope.user.password) {

      $ionicPopup.alert({
        title: 'Alert!',
        template: 'the password is incorrect'
      });

      $scope.userPass.current_pass = true;

    }else{
      $scope.userPass.current_pass = false;
    }
  };

  $scope.newPass = function(vNewPass){

    if (vNewPass == $scope.user.current_pass || vNewPass == $scope.user.password) {

      $ionicPopup.alert({
        title: 'Alert!',
        template: 'use different password than current'
      });

      $scope.userPass.valida_new_pass = true;

    }else{
      $scope.userPass.valida_new_pass = false;
    }

  };

  $scope.confirmPass = function(vConfirmPass){

    if (vConfirmPass != $scope.userPass.new_pass) {

      $scope.userPass.valida_user_pass = true;

      vValChangePass = 0;

    }else{

      $scope.userPass.valida_user_pass = false;

      vValChangePass = 1;
    }

  };

  var init = function(){
    document.getElementById('label-radius').innerHTML = $scope.user.visible_radius;
  };

  init();
  $scope.getSocialAccount();
})

;