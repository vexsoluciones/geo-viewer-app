angular.module('controller.map', [])

.controller('MapCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,TwitterFactory,
  InstagramFactory,FacebookFactory,UserAccountFactory,$rootScope,$cordovaGeolocation,$timeout,
  $cordovaNetwork,ConnectivityFactory,$interval,MapFactory) {

  var markersArray = [];
  var bounds = new google.maps.LatLngBounds();
  var map = null;
  var infowindow = 0;
  var geocoder = new google.maps.Geocoder();
  var searchBox = null;
  var markerCluster = null;
  var markerMe = 0;
  var circle = null;

  $scope.listSocialMap = true;

  $scope.option = {
    facebook : false,
    twitter : false,
    instagram : false
  };

  $scope.listTwitter = true;
  $scope.listInstagram = true;
  $scope.listFacebook = true;

  $scope.user = UserService.getUser();

  $scope.feedLoad = {
    data : ""
  };

  var vContactInst = [];
  var vContactFace = [];
  var vContactTwit = [];

  var vIniContact = 0;
  var vFinContact = 20;

  var vConnectivity = "";

  var inputSearch = document.getElementById('search');
  
  var geoTracker = function (){

    var vOptions = { frequency: 1000, timeout: 30000 };

    var onSuccess = function(position){

      /*console.log('latitute: ' + position.coords.latitude);
      console.log('longitude: ' + position.coords.longitude);
      console.log('$scope.user.user_id: ' + $scope.user.user_id);*/

      MapFactory.centerOnMe(position, true,map,markerMe);
      geoTracker();

	  };

	  var promise = $cordovaGeolocation.getCurrentPosition(vOptions);
	  //var watchID = navigator.geolocation.watchPosition(onSuccess, onGeolocationError, vOptions);
	  
	  promise.then(onSuccess, MapFactory.onGeolocationError);
  };

  $scope.visibleOptionSocial = function(){

    if($scope.listSocialMap === true){
      $scope.listSocialMap = false;
    }else{
      $scope.listSocialMap = true;
    }
  };

  $scope.selectSocialMap = function(){
    loadPosition("checked");  
  };

  var socialCheckedMap = function(position){

    var vData = {
        user_id : '',
        access_token : '',
        lat : position.coords.latitude,
        lng : position.coords.longitude,
        distance : '10000',
        geocode : position.coords.latitude + "," + position.coords.longitude + ",1mi",
    };

    clearMarkers();

    if ($scope.option.facebook === true || $scope.option.instagram === true || $scope.option.twitter === true) {

      $ionicLoading.show({
        template: 'Loading Map...'
      });

    }

    if($scope.option.facebook === true){

      console.log("listando data face");

      vData.user_id = $scope.user.id_face;
      vData.access_token = $scope.user.fb_token;

      friendsDataFacebook(vData);

    }

    if($scope.option.instagram === true){

      console.log("listando data insta");

      vData.user_id = $scope.user.id_inst;
      vData.access_token = $scope.user.insta_token;

      friendsDataInstagram(vData);
    }

    if($scope.option.twitter === true){

      console.log("listando data twitter");

      vData.user_id = $scope.user.id_twit;
      vData.access_token = $scope.user.twit_token;

      friendsDataTwitter(vData);
    }
  };
 
  $scope.loadPosition = function(vOpcion){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    switch(vOpcion) {

      case 'init':

        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          loadMaps(position);
        }, MapFactory.onGeolocationError);
      
      break;

      case 'search':
        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          socialCheckedMap(position);
        }, MapFactory.onGeolocationError);

      break;

      case 'mi-ubicacion':

       $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          MapFactory.centerOnMe(position,null,map,markerMe);
       }, MapFactory.onGeolocationError);
        
      break;
    }  
  };

  var escHtml = function (text) {
      return text.replace(/</g, '&lt;').replace(/>/g, '&gt;');
  };

  var clearMarkers = function () {
    while (markersArray.length) {
      markersArray.pop().setMap(null);
    }
    markerCluster.clearMarkers();
  };

  $scope.enterSearch = function(keyEvent){

    if (keyEvent.which === 13){
      MapFactory.searchLocation(inputSearch,searchBox,map);
    }
  };

	var loadMaps = function(position) {

    $ionicLoading.show({
      template: 'Loading Map...'
    });

    var vData = {
      user_id : '',
      access_token : '',
      lat : position.coords.latitude,
      lng : position.coords.longitude,
      distance : '10000',
      geocode : position.coords.latitude + "," + position.coords.longitude + ",1mi"

    };

    $scope.feedLoad.data = "(Loading ...)";

    if($scope.user.id_face){

      vData.user_id = $scope.user.id_face;
      vData.access_token = $scope.user.fb_token;

      friendsDataFacebook(vData,vContactFace);

      $scope.listFacebook = false;
      $scope.option.facebook = true;

    }

    if($scope.user.id_twit){

      vData.user_id = $scope.user.id_twit;
      vData.access_token = $scope.user.twit_token;

      var vTwitterCont = vContactTwit.slice(vIniContact,vFinContact);

      friendsDataTwitter(vData,vTwitterCont);
      
      $scope.listTwitter = false;
      $scope.option.twitter = true;

    }

    if($scope.user.id_inst){

      vData.user_id = $scope.user.id_inst;
      vData.access_token = $scope.user.insta_token;

      //var vInstagramCont = vContactInst.slice(vIniContact,vFinContact);

      friendsDataInstagram(vData,"mapa");
      
      $scope.listInstagram = false;
      $scope.option.instagram = true;
 
    }

   /* vIniContact +=100;
    vFinContact +=100; */

    $timeout(function() {

     $ionicLoading.hide();

    }, 3000);
	};

  var friendsDataFacebook = function(vData,vFriends){

    FacebookFactory.getFriendsData(vData,vFriends).then(function(data){

      $scope.feedLoad.data = "";

      angular.forEach(data, function(friends) {

          var vUser = friends.user;

          angular.forEach(friends.feed, function(feed) {

            var locationName, id, locationType, content;

            locationName = null;
            id = "loc-" + friends.id;
            locationType = "loc-gps";
            content = '';
            picture = '';
           
            content = MapFactory.infoMarkerFacebook(feed,friends,vUser,"si");

            circle = MapFactory.addPin(feed.place.location.latitude, feed.place.location.longitude, locationName, id, locationType, content,"img/facebook-pin.png",feed.visible_radius,feed.estado,markersArray,circle,map);
            bounds.extend(new google.maps.LatLng(feed.place.location.latitude, feed.place.location.longitude));
                
          });

      });

      if(data.length === 0){

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }

      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);

      var vJsonUser = JSON.stringify($scope.user);

      if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_twit") == -1){

        if (vContactFace.length === 0 || data.length <=1) {

          $ionicLoading.show({ template: "Invite to your friends to use tada to see their geolocalized photos", noBackdrop: true, duration: 6000 });

          $timeout(function() {

           FacebookFactory.inviteFriends();

         }, 9000);

        }

        MapFactory.centerOnMe({
          coords: {
            latitude: vData.lat,
            longitude: vData.lng
          }
        },null,map,markerMe);

      }

    },function(error){
 
      bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));

      MapFactory.mapBounds(map,bounds,"","");

    });
  };

  var friendsDataTwitter = function(vData,vContact){

      TwitterFactory.getTweetsLocation(vData,vContact).then(function(data){

        $scope.feedLoad.data = "";

        if(data.length === 0){

          bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
        }else{

          angular.forEach(data, function(photos) {

            var locationName, id, locationType, content;

            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";

            content = MapFactory.infoMarkerTwitter(photos,"si");

            circle = MapFactory.addPin(photos.geo.coordinates[0], photos.geo.coordinates[1], locationName, id, locationType, content,"img/twitter-pin.png",photos.user.visible_radius,photos.estado,markersArray,circle,map);
            bounds.extend(new google.maps.LatLng(photos.geo.coordinates[0], photos.geo.coordinates[1]));

          });

        }

        MapFactory.mapBounds(map,bounds,markerCluster,markersArray);

        var vJsonUser = JSON.stringify($scope.user);

        if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_face") == -1){

          MapFactory.centerOnMe({
            coords: {
              latitude: vData.lat,
              longitude: vData.lng
            }
          },null,map,markerMe);

        }
 
      });
  };

  var friendsDataInstagram = function(vData,vOpcion){

    var vTotal = vContactInst.length;
    var vInstagramCont = [];

    do {

      vInstagramCont = vContactInst.slice(vIniContact,vFinContact);

      InstagramFactory.getFriendsLocation(vData,vInstagramCont,vOpcion).then(function(data) {

        if(data.length > 0){

         angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          if ( photos.location.id) {
            locationName = photos.location.name;
            id = photos.location.id;
            locationType = "loc-place";

          } else {
            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";
          }

          content = MapFactory.infoMarkerInstagram(photos,"si");

          circle = MapFactory.addPin(photos.location.latitude, photos.location.longitude, locationName, id, locationType, content,"img/instagram-pin.png",photos.user.visible_radius,photos.estado,markersArray,circle,map);
          bounds.extend(new google.maps.LatLng(photos.location.latitude, photos.location.longitude));

          });

        } else{

          bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));

        }
      
      });

      vIniContact +=20;
      vFinContact +=20;

    }

    while (vInstagramCont.length > 0);

    $timeout(function() {

      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);

      $scope.feedLoad.data = "";

    }, 9000);
  };

  var init = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

        var mapOptions = {
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center : {lat: position.coords.latitude, lng: position.coords.longitude}
        };

        map = new google.maps.Map(document.getElementById("map"),mapOptions);

        searchBox = new google.maps.places.SearchBox(inputSearch);

        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        listContacts();

        searchBox.addListener('places_changed', function() {

        });

        //geoTracker();

      }, MapFactory.onGeolocationError);
  };

  $scope.more = function(){

    var vInsta = vContactInst.slice(vIniContact,vFinContact);
    var vTwit = vContactTwit.slice(vIniContact,vFinContact);

    if(vInsta.length > 0 || vTwit.length > 0){

      $scope.loadPosition("init");

    }else{
      $ionicLoading.show({ template: "No content", noBackdrop: true, duration: 2000 });
    }

    $scope.$broadcast('scroll.refreshComplete');
  };

  var listContacts = function (){

    var vParams = {};

    var vTwitterDefer = [];
    var vInstaDefer = [];
    var vFacebookDefer = [];

    var vDefer = $q.defer();

    if ($scope.user.id_twit) {

      vParams = {
        user_id : $scope.user.id_twit,
        access_token : $scope.user.twit_token
      };

      vTwitterDefer = listContactTwitter(vParams);

    }else{

      vDefer.resolve("");
      vTwitterDefer = vDefer.promise;

    }

    if ($scope.user.id_face) {

      vParams = {
        user_id : $scope.user.id_face,
        access_token : $scope.user.fb_token
      };

      vFacebookDefer = listContactFacebook(vParams);

    }else{

      vDefer.resolve("");
      vFacebookDefer = vDefer.promise;

    }

    if ($scope.user.insta_token) {

      vParams = {
        user_id : $scope.user.id_inst,
        access_token : $scope.user.insta_token
      };

      vInstaDefer = listContactInstagram(vParams);

    }else{

      vDefer.resolve("");
      vInstaDefer = vDefer.promise;

    }

    vTwitterDefer.then(function(data){

      vContactTwit = data;

    }).then(function(){

       vInstaDefer.then(function(data){

        vContactInst = data;

       }).then(function(){

        vFacebookDefer.then(function(data){

          vContactFace = data.data || data;

          $scope.loadPosition("init");

        });

       });

    });
  };

  var listContactInstagram = function(vData){

    var deferInsta = $q.defer();

    InstagramFactory.getContacts(vData).then(function(data) {

      deferInsta.resolve(data);

    }).catch(function(error){

      deferInsta.reject("[]");

    });

    return deferInsta.promise;
  };

  var listContactTwitter = function(vData){

    var deferTwitter = $q.defer();

    TwitterFactory.getContacts(vData).then(function(data) {

      deferTwitter.resolve(data);

    },function(error){

      deferTwitter.reject("[]");

    });

    return deferTwitter.promise;
  };

  var listContactFacebook = function(vData){

    var deferFacebook = $q.defer();

    FacebookFactory.getFriends(vData).success(function(data, status, headers, config) {

      //alert("status" + status);

      deferFacebook.resolve(data);

    }).error(function(error){

      alert("error" +JSON.stringify(error));

      deferFacebook.reject("[]");

    });

    return deferFacebook.promise;
  };
  

  ionic.Platform.ready(init);
  

});