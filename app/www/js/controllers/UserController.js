angular.module('controller.user', [])

.controller('UserCtrl', function($scope, $state, UserService,$stateParams,$cordovaGeolocation,$ionicLoading,TwitterFactory,
  InstagramFactory,FacebookFactory,ImageFactory,UserAccountFactory,UserFactory,$timeout,$rootScope,MapFactory) {

  var markersArray = [];
  var bounds = new google.maps.LatLngBounds();
  var map = null;
  var infowindow = 0;
  var geocoder = new google.maps.Geocoder();
  var searchBox = null;
  var markerCluster = null;
  var markerMe = 0;
  var circle = null;

  $scope.user = UserService.getUser();

  $scope.contact = {
    picture : '',
    name : ''
  };

  $scope.feedLoad = {
    data : ""
  };

  $scope.loadPosition = function(vOpcion,vData){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    switch(vOpcion) {

      case 'init':

        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          loadMaps(position,vData);
        }, MapFactory.onGeolocationError);
      
      break;

      case 'mi-ubicacion':

       $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          MapFactory.centerOnMe(position,true,map,markerMe);
       }, MapFactory.onGeolocationError);
        
      break;
    }  
  };

  var verPerfil = function(){

    var vParams = {};

    UserAccountFactory.existsPerfilSocial($stateParams.id).success(function(data){

      if (data.length > 0) {

        $scope.contact.picture = data[0].picture;

        UserFactory.getUser(data[0].userId).success(function(data){

          $scope.contact.name = data[0].firstName +" "+ data[0].lastName;

          $ionicLoading.hide();

          UserAccountFactory.getAllActive(data[0].id).success(function(perfiles){

            $scope.loadPosition("init",perfiles);

         });

        });

      }else{

        vParams = {
          user_id : $stateParams.id,
          access_token : ''
        };

        var vFoto = "";

        if ($stateParams.social == "twitter") {

          vParams.access_token = $scope.user.twit_token;

          TwitterFactory.getPerfilData(vParams).success(function(data) {

            vFoto = ImageFactory.getPictureFullSize(data.profile_image_url,$stateParams.social);

            $scope.contact.picture = vFoto;
            $scope.contact.name = data.name;

          });

        }

        if ($stateParams.social == "instagram") {

          vParams.access_token = $scope.user.insta_token;

          InstagramFactory.getUserDataLogin(vParams).success(function(response) {

            vFoto = ImageFactory.getPictureFullSize(response.data.profile_picture,$stateParams.social);

            $scope.contact.picture = vFoto;
            $scope.contact.name = response.data.full_name;

          });

        }

        if ($stateParams.social == "facebook") {

         vParams.access_token = $scope.user.fb_token;

         FacebookFactory.getContactInfo(vParams).success(function(resultData) {

            $scope.contact.picture = resultData.picture.data.url;
            $scope.contact.name = resultData.name;

         });

        }

        $ionicLoading.hide();

        $scope.loadPosition("init",null);

      }

    });

  };

  var loadMaps = function(position,data) {

    $ionicLoading.show({
      template: 'Loading Map...'
    });

    $scope.feedLoad.data = "(Loading ...)";

    if (data !== null) {
        
        angular.forEach(data,function(perfiles){

          switch(perfiles.socialAccount) {

            case 'facebook':

              listarMapaContact("facebook",perfiles.idPerfil,perfiles.accessToken,position);

            break;

            case 'twitter':

              listarMapaContact("twitter",perfiles.idPerfil,perfiles.accessToken,position);

            break;

            case 'instagram':

              listarMapaContact("instagram",perfiles.idPerfil,perfiles.accessToken,position);

            break;

          }

        });

    }else{

      if($stateParams.social == "twitter"){

        listarMapaContact("twitter",$stateParams.id,$scope.user.twit_token,position);

      }

      if($stateParams.social == "instagram"){

        listarMapaContact("instagram",$stateParams.id,$scope.user.insta_token,position);

      }

      if($stateParams.social == "facebook"){

        listarMapaContact("facebook",$stateParams.id,$scope.user.fb_token,position);

      }

    }

    $timeout(function() {

     $ionicLoading.hide();

    }, 5000);

  };

  var listarMapaContact = function(vSocial,vId,vAccessToken,vPosition){

    var vData = {
      user_id : vId,
      access_token : vAccessToken,
      lat : vPosition.coords.latitude,
      lng : vPosition.coords.longitude,

    };

    switch(vSocial) {

      case 'facebook':

        var vDataFace = [];

        vDataFace.push({id:vId});

        friendsDataFacebook(vData,vDataFace);

      break;

      case 'twitter':

        var vDataTwit = [];

        vDataTwit.push({id_str:vId});

        friendsDataTwitter(vData,vDataTwit);

      break;

      case 'instagram':

        var vDataInsta = [];

        vDataInsta.push({id:vId});

        friendsDataInstagram(vData,vDataInsta,"perfil");

      break;

    }

  };

  var friendsDataInstagram = function(vData,vContact,vOpcion){

    InstagramFactory.getFriendsLocation(vData,vContact,vOpcion).then(function(data) {

      console.log("data final instagram total" + data.length);

      $scope.feedLoad.data = "";
    
      if(data.length > 0){

       angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          if ( photos.location.id) {
            locationName = null;
            id = photos.location.id;
            locationType = "loc-place";
          } else {
            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";
          }

          content = MapFactory.infoMarkerInstagram(photos);

          circle = MapFactory.addPin(photos.location.latitude, photos.location.longitude, locationName, id, locationType, content,"img/instagram-pin.png",photos.user.visible_radius,photos.estado,markersArray,circle,map);
          bounds.extend(new google.maps.LatLng(photos.location.latitude, photos.location.longitude));

       });
         
      }else{

        $ionicLoading.show({ template: "data not found", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));

      }
      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);

    });
  };

  var friendsDataTwitter = function(vData,vContact){

    TwitterFactory.getTweetsLocation(vData,vContact).then(function(data){

      $scope.feedLoad.data = "";

      if(data.length === 0){

        $ionicLoading.show({ template: "data not found", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }else{

        angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          locationName = null;
          id = "loc-" + photos.id;
          locationType = "loc-gps";

          content = MapFactory.infoMarkerTwitter(photos,"no");

          MapFactory.addPin(photos.geo.coordinates[0], photos.geo.coordinates[1], locationName, id, locationType, content,"img/twitter-pin.png",photos.visible_radius,photos.estado,markersArray,circle,map);
          bounds.extend(new google.maps.LatLng(photos.geo.coordinates[0], photos.geo.coordinates[1]));

        });

      }

      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);
 
    });
  };

  var friendsDataFacebook = function(vData,vId){

    FacebookFactory.getFriendsData(vData,vId).then(function(data){

      $scope.feedLoad.data = "";

      angular.forEach(data, function(friends) {

          var vUser = friends.user;

          angular.forEach(friends.feed, function(feed) {

            var locationName, id, locationType, content;

            locationName = null;
            id = "loc-" + friends.id;
            locationType = "loc-gps";

            content = MapFactory.infoMarkerFacebook(feed,friends,vUser,"no");
      
            MapFactory.addPin(feed.place.location.latitude, feed.place.location.longitude, locationName, id, locationType, content,"img/facebook-pin.png",feed.visible_radius,feed.estado,markersArray,circle,map);
            bounds.extend(new google.maps.LatLng(feed.place.location.latitude, feed.place.location.longitude));
                
          });

      });

      if(data.length === 0){

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }

      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);

      var vJsonUser = JSON.stringify($scope.user);

      if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_twit") == -1){

        MapFactory.centerOnMe({
          coords: {
            latitude: vData.lat,
            longitude: vData.lng
          }
        },null,map,markerMe);

      }

    },function(error){

      bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
      MapFactory.mapBounds(map,bounds,"","");

    });
  };


  var init = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

      var mapOptions = {
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center : {lat: position.coords.latitude, lng: position.coords.longitude}
      };

      map = new google.maps.Map(document.getElementById("mapa"),mapOptions);

    }, MapFactory.onGeolocationError);

  };

  ionic.Platform.ready(init);

  verPerfil();

})
;