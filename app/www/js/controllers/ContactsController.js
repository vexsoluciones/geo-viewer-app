angular.module('controller.contacts', [])

.controller('ContactsCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,InstagramFactory,
  UserAccountFactory,TwitterFactory,FacebookFactory,$rootScope,$cordovaFacebook) {
  
  $scope.user = UserService.getUser();

  $scope.contact = {
    facebook : '',
    twitter : '',
    instagram : '',
  };

  $scope.select = {
    socialOptions : '',
    data : ''
  };

  $scope.boxSelect = false;

  $scope.totalContact = {
    facebook : '',
    twitter : '',
    instagram : '',
    total : ''
  };


var listSocialSelect = function(){

    var vSocialData = [];
    var itemSocial = {};

    if($scope.user.id_face){

      itemSocial = {id: 'facebook',name: 'Facebook'};
      vSocialData.push(itemSocial);

    }

    if($scope.user.id_twit){

      itemSocial = {id: 'twitter',name: 'Twitter'};
      vSocialData.push(itemSocial);
      
    }

    if($scope.user.id_inst){

      itemSocial = {id: 'instagram',name: 'Instagram'};
      vSocialData.push(itemSocial);
      
    }

    $scope.select.socialOptions = vSocialData;

    switch(vSocialData.length) {

        case 2:
          $scope.select.socialDefault = vSocialData[1];
        break;

        case 1:
          $scope.select.socialDefault = vSocialData[0];
        break;

        default:
          $scope.select.socialDefault = vSocialData[0];
        break;

      }
};

$scope.visibleContactFacebook = function(option){

    if (option == "contacts") {

      $scope.tabs.facebookContact = false;
      $scope.tabs.facebookTada = true;

      $scope.totalContact.total = $scope.totalContact.facebook;

    }else{

      $scope.tabs.facebookContact = true;
      $scope.tabs.facebookTada = false;

      $scope.totalContact.total = $scope.totalContact.facebookTada;

    }
};

$scope.filterContact = function(select){

    switch(select.id) {

      case 'facebook':
        $scope.contacts = $scope.contact.facebook;
    
        $scope.totalContact.total = $scope.totalContact.facebook;

      break;

      case 'twitter':
        $scope.contacts = $scope.contact.twitter;

        $scope.totalContact.total = $scope.totalContact.twitter;

      break;

      case 'instagram':
        $scope.contacts = $scope.contact.instagram;

        $scope.totalContact.total = $scope.totalContact.instagram;

      break;
    }
};

var listContact = function(){

    $ionicLoading.show({
      template: 'Loading Contacts...'
    });

    var vParams = {};

    if ($scope.user.id_face) {

      vParams = {
        user_id : $scope.user.id_face,
        access_token : $scope.user.fb_token
      };

      listContactFacebook(vParams);
    }

    if ($scope.user.id_twit) {

      vParams = {
        user_id : $scope.user.id_twit,
        access_token : $scope.user.twit_token
      };

      listContactTwitter(vParams);
    }

    if ($scope.user.insta_token) {

      vParams = {
        user_id : $scope.user.id_inst,
        access_token : $scope.user.insta_token
      };

      listContactInstagram(vParams);
    }
};

var listContactTwitter = function(vData){

    var vContactData = [];

    TwitterFactory.getContacts(vData).then(function(data){

      var vContact = {};

      angular.forEach(data, function(item){

        var vFoto = getFoto(item.profile_image_url,'twitter');

        vContact = {
          name : item.name,
          picture : item.profile_image_url,
          picture_original : vFoto,
          location: item.location,
          id : item.id_str,
          social : 'twitter'
        };

        vContactData.push(vContact);

      });

      $scope.contact.twitter= vContactData;

      $scope.contacts = $scope.contact.twitter;

      $scope.totalContact.twitter = vContactData.length;

      $scope.totalContact.total = vContactData.length;

      $ionicLoading.hide();

    });
};

var listContactFacebook = function(vData){

    FacebookFactory.getFriends(vData).success(function(data) {

      var vContact = {};

      var vContactFace = [];

      angular.forEach(data.data, function(item){

        vContact = {
          name : item.name,
          picture : item.picture.data.url,
          id : item.id,
          social : "facebook"
        };

        vContactFace.push(vContact);

      });

      $scope.contact.facebook = vContactFace;

      $scope.contacts = $scope.contact.facebook;

      $scope.totalContact.facebook= vContactFace.length;

      $scope.totalContact.total = vContactFace.length;

      $ionicLoading.hide();

    });
};

var listContactInstagram = function(vData){

    var vContactData = [];

    InstagramFactory.getContacts(vData).then(function(data) {

      var vContact = {};

      angular.forEach(data, function(item){

        var vFoto = getFoto(item.profile_picture,'instagram');

        vContact = {
          name : item.full_name ? item.full_name  : item.username ,
          picture : item.profile_picture,
          picture_original : vFoto,
          social : 'instagram',
          id : item.id
        };

        vContactData.push(vContact);
      });

      $scope.contact.instagram = vContactData;
      $scope.contacts = $scope.contact.instagram;

      $scope.totalContact.instagram= vContactData.length;

      $scope.totalContact.total = vContactData.length;

      $ionicLoading.hide();

    });
};

var getFoto = function(vUrl,vSocial){

    var vFoto = "";

    switch(vSocial) {

      case 'facebook':
      break;

      case 'twitter':
        vFoto = vUrl.replace("_normal", "");
      break;

      case 'instagram':
        vFoto = vUrl.replace("/s150x150/", "/");
      break;

    }

    return vFoto;
};

var getUrl = function(url) {
    var path = new URL(url).pathname;
    var parts = path.split('/');

    parts = parts.filter(function(part) {
    	return part.length !== 0;
    });

    return parts[parts.length - 1];
};

var getPageId = function(vUrl) {
	var vPos1 = vUrl.search("_");

	var vUrl2 = vUrl.substring(vPos1);
	var vPos2 = vUrl2.search("_");

	var vUrl3 = vUrl2.substring(vPos2+1);
	var vPos3 = vUrl3.search("_");

	var vUrl4 = vUrl3.substring(0,vPos3);

	return vUrl4;
};

listSocialSelect();

listContact();

})