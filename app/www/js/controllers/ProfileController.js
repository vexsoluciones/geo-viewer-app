angular.module('controller.profile', [])

.controller('ProfileCtrl', function($scope, $state, UserService, $cordovaFacebook,$cordovaGeolocation,$ionicLoading,TwitterFactory,
  InstagramFactory,FacebookFactory,ImageFactory,$timeout,MapFactory) {

  var markersArray = [];
  var bounds = new google.maps.LatLngBounds();
  var map = null;
  var infowindow = 0;
  var geocoder = new google.maps.Geocoder();
  var searchBox = null;
  var markerCluster = null;
  var markerMe = 0;
  var circle = null;

  $scope.user = UserService.getUser();

  $scope.feedLoad = {
    data : ""
  };

  var init = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

      var mapOptions = {
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center : {lat: position.coords.latitude, lng: position.coords.longitude}
      };

      map = new google.maps.Map(document.getElementById("mapa"),mapOptions);

      $scope.loadPosition("init");

    }, MapFactory.onGeolocationError);

  };


  $scope.loadPosition = function(vOpcion){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    switch(vOpcion) {

      case 'init':

        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          loadMaps(position);
        }, MapFactory.onGeolocationError);
      
      break;

      case 'mi-ubicacion':

       $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          MapFactory.centerOnMe(position,true,map,markerMe);
       }, MapFactory.onGeolocationError);
        
      break;
    }  
  };

  var loadMaps = function(position) {

    $ionicLoading.show({
      template: 'Loading Map...'
    });

    var vData = {
      user_id : '',
      access_token : '',
      lat : position.coords.latitude,
      lng : position.coords.longitude,
      id : $scope.user.user_id

    };

    $scope.feedLoad.data = "(Loading ...)";

    if($scope.user.fb_token){

      vData.user_id = $scope.user.id_face;
      vData.access_token = $scope.user.fb_token;

      var vDataFace = [];

      vDataFace.push({id:$scope.user.id_face});

      feedFacebook(vData,vDataFace);

    }

    if($scope.user.twit_token){

      vData.access_token = $scope.user.twit_token;

      var vDataTwit = [];

      var vIdTwitter = {id_str:$scope.user.id_twit};

      vDataTwit.push(vIdTwitter);

      feedTwitter(vData,vDataTwit);
   
    }

    if($scope.user.insta_token){

      vData.access_token = $scope.user.insta_token;

      var vDataInsta = [];

      var vIdInsta = {id:$scope.user.id_inst};

      vDataInsta.push(vIdInsta);

      feedInstagram(vData,vDataInsta,"perfil");

    }

    $timeout(function() {

     $ionicLoading.hide();

    }, 3000);

  };

  var feedInstagram = function(vData,vContact,vOpcion){

    InstagramFactory.getFriendsLocation(vData,vContact,vOpcion).then(function(data) {

      console.log("data final instagram total" + data.length);

      $scope.feedLoad.data = "";
    
      if(data.length > 0){

       angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          if ( photos.location.id) {
            locationName = null;
            id = photos.location.id;
            locationType = "loc-place";
          } else {
            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";
          }

          content = MapFactory.infoMarkerInstagram(photos,"no");
    
          MapFactory.addPin(photos.location.latitude, photos.location.longitude, locationName, id, locationType, content,"img/instagram-pin.png",photos.user.visible_radius,photos.estado,markersArray,circle,map);
          bounds.extend(new google.maps.LatLng(photos.location.latitude, photos.location.longitude));

       });
         
      }else{

        $ionicLoading.show({ template: "data not found instagram", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));

      }

      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);

    });
  };

  var feedTwitter = function(vData,vContact){

    TwitterFactory.getTweetsLocation(vData,vContact).then(function(data){

      $scope.feedLoad.data = "";

      if(data.length === 0){

        $ionicLoading.show({ template: "data not found twitter", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }else{

        angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          locationName = null;
          id = "loc-" + photos.id;
          locationType = "loc-gps";

          content = MapFactory.infoMarkerTwitter(photos,"no");

          MapFactory.addPin(photos.geo.coordinates[0], photos.geo.coordinates[1], locationName, id, locationType, content,"img/twitter-pin.png",photos.visible_radius,photos.estado,markersArray,circle,map);
          bounds.extend(new google.maps.LatLng(photos.geo.coordinates[0], photos.geo.coordinates[1]));

        });

      }

      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);
 
    });
  };

  var feedFacebook = function(vData,vId){

    FacebookFactory.getFriendsData(vData,vId).then(function(data){

      $scope.feedLoad.data = "";

      angular.forEach(data, function(friends) {

          var vUser = friends.user;

          angular.forEach(friends.feed, function(feed) {

          	console.log("feedFacebook : " + JSON.stringify(feed.place));

            var locationName, id, locationType, content;

            locationName = null;
            id = "loc-" + friends.id;
            locationType = "loc-gps";

            content = MapFactory.infoMarkerFacebook(feed,friends,vUser,"no");

            MapFactory.addPin(feed.place.location.latitude, feed.place.location.longitude, locationName, id, locationType, content,"img/facebook-pin.png",feed.visible_radius,feed.estado,markersArray,circle,map);
            bounds.extend(new google.maps.LatLng(feed.place.location.latitude, feed.place.location.longitude));
                
          });

      });

      if(data.length === 0){

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }

      MapFactory.mapBounds(map,bounds,markerCluster,markersArray);

      var vJsonUser = JSON.stringify($scope.user);

      if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_twit") == -1){

        MapFactory.centerOnMe({
          coords: {
            latitude: vData.lat,
            longitude: vData.lng
          }
        },null,map,markerMe);

      }

    },function(error){

      bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
      MapFactory.mapBounds(map,bounds,"","");

    });
  };


  ionic.Platform.ready(init);

})