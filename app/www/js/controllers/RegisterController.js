angular.module('controller.register', [])

.controller('RegisterCtrl', function($scope, $state, UserFactory, $ionicLoading,$rootScope,$ionicPopup) {

  	$scope.registerData = {
  		nombre : '',
  		email : '',
  		pass : ''
  	};

  	$scope.register = function(){

  		if ($scope.registerData.email !== "" && $scope.registerData.pass !== "") {

  			UserFactory.exists($scope.registerData.email).success(function(response){

  				if (response.length > 0) {

  					$ionicPopup.alert({
  						title: 'Alert',
  						template: 'Mail already exists'
  					});

  				}else{

  					var vUserData = {
  						user_email : $scope.registerData.email,
  						user_pass : $scope.registerData.pass,
  						last_name : '',
  						visible_radius: 500
  					};

  					UserFactory.register(vUserData).success(function(response){

  						$ionicPopup.alert({
  							title: 'Success',
  							template: 'Registered user'
  						});

  						$scope.registerData = {
  							nombre : '',
  							email : '',
  							pass : ''
  						};

  						$state.go('login');

  					}).error(function(error){

  						$ionicPopup.alert({
  							title: 'Alert',
  							template: 'User not registered'
  						});

  					});

  				}

  			}); 

  		}else{
  			$ionicPopup.alert({
  				title: 'Alert',
  				template: 'Insert data'
  			});
  		}
  		
  	};

})

;