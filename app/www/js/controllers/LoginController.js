angular.module('controller.login', [])

.controller('LoginCtrl', function($scope, $state, $q, UserService, $ionicLoading, FACEBOOK_APP_ID,$http,
  ApiUrl,ApiSocialUrl,UserFactory,LoginFaceFactory,TwitterFactory,UserAccountFactory,InstagramFactory,$cordovaOauth,$rootScope,$ionicPopup,$timeout,MailFactory,$cordovaFacebook,$cordovaNetwork) {

  $scope.slideIndex = 0;

  $rootScope.auth = false;

  $scope.loginData = {
      user_email: '',
      user_pass: ''
  };

  if (typeof($rootScope.idSession) != "undefined") {
    $state.go('app.settings');
  }


  $scope.loginInstagram = function(){

      $ionicLoading.show({
          template: 'Loging in...'
      });

      $cordovaOauth.instagram("3d2337eca9ed4277a07962a821579891", ['likes','relationships']).then(function(result) {

        var vAccessToken = result.access_token;

        var vPos = vAccessToken.indexOf('.');
        var vId = vAccessToken.substring(0,vPos);

        var vParams = {
          user_id : vId,
          access_token : vAccessToken
        };

        InstagramFactory.getUserDataLogin(vParams).success(function(response) {

          var vData = {
            name : response.data.full_name,
            first_name : response.data.displayName,
            last_name : '',
            id_inst : response.data.id,
            picture : response.data.profile_picture,
            id : response.data.id,
            email : response.data.id +'@tada.com',
            insta_token : vAccessToken,
            access_token : vAccessToken
          };

          sessionUser('instagram',vData);
        }).error(function (error){

          $ionicLoading.hide();
        	
          $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
        	
        });
      }, function(error) {

        $ionicLoading.hide();
    	  $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });

      });
  };

  $scope.loginTwitter = function(){

    $ionicLoading.show({
        template: 'Loging in...'
    });

    var clientId = 'z3W5b6lvxAHNrueE1iREpJMxQ';
    var clientSecret = 'k0vab1S1drvsLxeryDF8BWR0gqy7Kr2Ct3Ppoyi7zSFg3unax1';

    $cordovaOauth.twitter(clientId,clientSecret,{redirect_uri: "https://tinyurl.com/krmpchb"}).then(function(result) {
        var vParams = {
          user_id : result.user_id,
          access_token : result.oauth_token
        };

        TwitterFactory.getPerfilData(vParams).success(function(data) {

          var vData = {
            name : data.name,
            first_name : data.name,
            last_name : '',
            id_twit : data.id,
            picture : data.profile_image_url.replace('_normal', ''),
            id : data.id,
            email : data.id +'@tada.com',
            access_token : result.oauth_token,
            twit_token : result.oauth_token,
            token_secret : result.oauth_token_secret
          };

          sessionUser('twitter',vData);

        });
                
    }, function(error) {

      $ionicLoading.hide();
    	error = error || 'Twitter login problems. Try again.';
		  $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
    });
  };

  $scope.login = function() {
    if ($scope.loginData.user_email!==""  && $scope.loginData.user_pass!=="") {

        if ($scope.loginData.user_email !="undefined") {

          $ionicLoading.show({
            template: 'Loging in...'
          });
          
          UserFactory.login($scope.loginData).success(function(data, status, headers, config) {

            var vValida = JSON.stringify(data);

            $ionicLoading.hide();

            if (vValida.indexOf("id") != -1) {

              var userData = {
                picture : '',
                access_token : '',
                fb_token : '',
                id_face : '',
                first_name : data[0].firstName,
                last_name : '',
                emailFace : '',
                name : data[0].firstName +" "+ data[0].lastName,
                id : data[0].id,
                email : data[0].userEmail
              };

              sessionUser("",userData);
              
            }else{

              $ionicLoading.show({ template: "Incorrect password and user", noBackdrop: true, duration: 2000 });

            }
      
           });
    
        }else{

           $ionicLoading.show({ template: "Incorrect email", noBackdrop: true, duration: 2000 });

        }

    }else{

       $ionicLoading.show({ template: "Incorrect email and password", noBackdrop: true, duration: 2000 });

    }
  };

  $scope.loginFacebook = function(){

    $ionicLoading.show({
          template: 'Loging in...'
        });

    $cordovaFacebook.getLoginStatus()
      	.then(function(success) {

        if (success.status == "connected") {

          loginFacebookExtend(success.authResponse.accessToken);

        }else{

          $cordovaFacebook.login(['email',
            //'publish_actions',//no deja loguear
            'public_profile',
            'user_location',
            'user_friends',
            'user_photos',
            'user_posts'])

          .then(function(success) {

            $cordovaFacebook.api(
              'me',
              ['publish_actions'],
              function (res)
              {
                 
              },
              function (err)
              {
            
              });

              loginFacebookExtend(success.authResponse.accessToken);

          }, function (error) {

            $ionicLoading.hide();
            $ionicLoading.show({ template: JSON.stringify(error), noBackdrop: true, duration: 2000 });

          });

        }

    });
  };

  var loginFacebookExtend = function(accessToken){

    LoginFaceFactory.getFacebookProfileInfo(accessToken).success(function(resultData) {

      var userData = {
        picture : resultData.picture.data.url,
        access_token : accessToken,
        fb_token : accessToken,
        id_face : resultData.id,
        first_name : resultData.name,
        last_name : '',
        emailFace : resultData.email,
        name : resultData.name,
        id : resultData.id,
        email : resultData.email
      };

      sessionUser("facebook",userData);

    }).error(function(response){

      $ionicLoading.hide();

    });
  };

  var setUserData = function(data,userSession){

    var userData = userSession;
    var userDefer = $q.defer();

    angular.forEach(data, function(item){
        userData.user_id = item.id;
        userData.password = item.userPass;
        userData.email = item.userEmail;

        userData.visible_radius = item.visibleRadius;

        if (item.firstName !=="") {
          userData.first_name = item.firstName;
        }

        if (item.lastName !=="") {
          userData.last_name = item.lastName;
        }

        if (item.lastName !== "" && item.firstName !== "") {
          userData.name = item.firstName +" "+ item.lastName;
        }else{
          userData.name = item.firstName;
        }
                   
    });

    UserAccountFactory.getAllActive(userData.user_id).success(function(data, status, headers, config) {

      var vDataAccount = {};

      angular.forEach(data, function(item){
          
        if(item.socialAccount == "facebook"){

          vDataAccount.fb_token = item.accessToken;
          vDataAccount.id_face = item.idPerfil;

        }

        if(item.socialAccount == "twitter"){

          vDataAccount.twit_token = item.accessToken;
          vDataAccount.id_twit = item.idPerfil;

        }

        if(item.socialAccount == "instagram"){

          vDataAccount.insta_token = item.accessToken;
          vDataAccount.id_inst = item.idPerfil;

        }

        if (item.picture !=="") {

          vDataAccount.picture = item.picture;

        }
 
      });

      userDefer.resolve(vDataAccount);

    });

    $rootScope.auth = true;

    userDefer.promise.then(function(response){

      var valida = JSON.stringify(response);

      if(valida.indexOf("id_face") >-1){

        userData.fb_token = response.fb_token;
        userData.id_face = response.id_face;

      }

      if(valida.indexOf("id_twit") >-1){

        userData.twit_token = response.twit_token;
        userData.id_twit = response.id_twit;

      }

      if(valida.indexOf("id_inst") >-1){

        userData.insta_token = response.insta_token;
        userData.id_inst = response.id_inst;
      }

      if(valida.indexOf("picture") >-1){

        userData.picture = response.picture;

      }

      return userData;
      

    }).then(function(data){

      UserService.setUser(data);

      $ionicLoading.hide();

      if (localStorage.urlIntent) {

        $state.go('app.publish');

      }else{

        var vUserValida = UserService.getUser();

        if (vUserValida.id_inst || vUserValida.id_twit || vUserValida.id_face) {

          $state.go('app.map');

        }else{
          $state.go('app.settings');
        }

        

      }
      

    });
  };

  var sessionUser = function(social,userSession){

    var userData = userSession;

    var myPopup = null;

    var vDataValida = {
      user_id : '',
      social : social
    };

    var vDataPerfil = {
      id_perfil: userData.id,
      access_token : userData.access_token,
      email: userData.email,
      picture : userData.picture
    };

    UserFactory.exists(userData.email).success(function(data, status, headers, config) {

      if (data.length > 0) {

        $rootScope.idSession = userData.id;
        $rootScope.social = social;

        vDataValida.user_id = data[0].id;

        if (social !=="") {

          UserAccountFactory.associateAccount(vDataValida,vDataPerfil).then(function(response){

            setUserData(data,userSession);

          });

        }else{

          setUserData(data,userSession);

        }
  
      }else{

        UserAccountFactory.existsPerfilSocial(userData.id).success(function(datas, status, headers, config) {

          if (datas.length > 0) {

            $rootScope.idSession = userData.id;
            $rootScope.social = social;

            var vIdUser = "";

            angular.forEach(datas, function(item){
              vIdUser = item.userId;
            });

            vDataValida.user_id = vIdUser;

            UserFactory.getUser(vIdUser).success(function(data, status, headers, config) {

              UserAccountFactory.associateAccount(vDataValida,vDataPerfil).then(function(response){

                setUserData(data,userSession);

              });

            });

          }else{

            var dataUsuario = {
              first_name: userData.first_name,
              last_name:  userData.last_name,
              user_email: userData.email,
              user_pass: '',
              status: '0',
              visible_radius : '500'
            };

            if (social == "twitter" || social == "instagram") {

              $ionicLoading.hide();

              $scope.data = {};

              myPopup = $ionicPopup.show({
                template: '<input type="email" ng-model="data.email" placeholder=" e-mail"></br> <input type="password" ng-model="data.password" placeholder=" password">',
                title: 'Important',
                subTitle: 'Define your email and password for your Tada Account',
                scope: $scope,
                buttons: [

                {
                  text: 'Cancel',
                  onTap: function(e) {
                    return $scope.data;
                  }
                },
          
                {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  onTap: function(e) {

                    if (!$scope.data.email || !$scope.data.password) {
                      e.preventDefault();
                    } else {
                      return $scope.data;
                    }                      
                  }
                }
                ]
              });

              myPopup.then(function(response) {

                if (response.email!== undefined) {

                  UserFactory.exists(response.email).success(function(data, status, headers, config) {

                    if(data.length > 0){
                      
                      $ionicLoading.show({
                        template: 'This email is already being used'
                      });

                      $timeout(function() {
                        myPopup.close(); 
                        $ionicLoading.hide();
                      }, 5000);

                    }else{

                      dataUsuario.user_email = response.email;
                      dataUsuario.user_pass = response.password;

                      userData.email = response.email;
                      //userData.current_pass = response.password;

                      registrarUsuario(dataUsuario,social,vDataValida,vDataPerfil,userData,userSession);
                    }

                  });

                }

              });

            }else{

              $ionicLoading.hide();

              $scope.data = {};

              var myPopupFace = $ionicPopup.show({
                template: '<input type="password" ng-model="data.password" placeholder=" password">',
                title: 'Define your pass for your Tada Account',
                subTitle: 'your pass',
                scope: $scope,
                buttons: [

                {
                  text: 'Cancel',
                  onTap: function(e) {
                    return $scope.data;
                  }
                },
          
                {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  onTap: function(e) {

                    if (!$scope.data.password) {
                      e.preventDefault();
                    } else {
                      return $scope.data;
                    }                      
                  }
                }
                ]
              });

              myPopupFace.then(function(response) {

                if (response.password!== undefined) {

                  dataUsuario.user_pass = response.password;

                 // userData.current_pass = response.password;

                  registrarUsuario(dataUsuario,social,vDataValida,vDataPerfil,userData,userSession);

                }

              });

            }

          }

        }).error(function(error){

          $ionicLoading.hide();

        });

      }

    }).error(function(error){

      $ionicLoading.hide();

    });
  };

  var registrarUsuario = function(dataUsuario,social,vDataValida,vDataPerfil,userData,userSession){

     alert("register 215454 "+ JSON.stringify(dataUsuario));

    UserFactory.register(dataUsuario).success(function(data, status, headers, config) {

      $rootScope.idSession = vDataPerfil.id_perfil;
      $rootScope.social = social;

      UserFactory.exists(dataUsuario.user_email).success(function(data, status,headers,config){

        alert(" UserFactory.exists "+ JSON.stringify(data));

        vDataValida.user_id = data[0].id;

        UserAccountFactory.associateAccount(vDataValida,vDataPerfil).then(function(response){

          alert("  UserAccountFactory"+ JSON.stringify(response));

          setUserData(data,userSession);

          var vEmailNotify = {
              email : dataUsuario.user_email,
              password : dataUsuario.user_pass
          };

          MailFactory.registerNotify(vEmailNotify);

        });

      });

    });
  };

})
;