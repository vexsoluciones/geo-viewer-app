// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('tada', ['ionic', 'tada.controllers', 'tada.services','ngCordova'])

.run(function($ionicPlatform, $rootScope, $state,$interval,ConnectivityFactory) {


  $ionicPlatform.on("deviceready", function(){

    if (typeof($rootScope.idSession) == "undefined") {
      $state.go('login');
    }

    var vConnectivity = "";

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    /*if(window.cordova && cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }/*/

    window.plugins.webintent.getExtra(window.plugins.webintent.EXTRA_STREAM,
      function(url) {

        localStorage.setItem("urlIntent",url);

      }, function() {

      }
    );

    $interval(function(){ 

      console.log(ConnectivityFactory.isOnline());

      if (ConnectivityFactory.isOnline() === false) {

        vConnectivity = "false";

      }

      if (vConnectivity == "false" && ConnectivityFactory.isOnline() === true) {

        vConnectivity = "true";

        window.location.reload(true);
      }

   }, 3000);

  });

  $ionicPlatform.on("resume", function(){
    /*facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status != 'connected'){
        $state.go('login');
      }
    */
  });

    // UI Router Authentication Check
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    if (toState.data.authenticate) {

      if ($rootScope.auth === false) {
          event.preventDefault();
          $state.go('login'); 
      }

    }

  });
})

.config(function($stateProvider, $urlRouterProvider) {

   
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl',
    cache : false
  })

  .state('login', {
    url: "/",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl',
    data: {
      authenticate: false
    },
     
  })

  .state('register', {
    url: "/register",
    templateUrl: "templates/register.html",
    controller: 'RegisterCtrl',
    cache : false,
      data: {
      authenticate: false
    },
     
  })

  .state('forgotpass', {
    url: "/",
    templateUrl: "templates/forgot-password.html",
    controller: 'forgotpassCtrl',
    data: {
      authenticate: false
    }
  })

  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "templates/profile.html",
        controller: 'ProfileCtrl'
      }
    },
    data: {
      authenticate: true
    },
    cache : false
  })

  .state('app.user', {
    url: "/user/:id/social/:social", //name/:name/picture/:picture/social/:social",
    views: {
      'menuContent': {
        templateUrl: "templates/user.html",
        controller: 'UserCtrl'
      }
    },
    data: {
      authenticate: true
    },
    cache : false
  })

  .state('app.publish', {
    url: "/publish",
    views: {
      'menuContent': {
        templateUrl: "templates/publish.html",
        controller: 'PublishCtrl'
      }
    },
    data: {
      authenticate: true
    },
    cache : false
  })
  
  .state('app.map', {
    url: "/map",
    views: {
      'menuContent': {
        templateUrl: "templates/map.html",
        controller: 'MapCtrl'
      }
    },
    data: {
      authenticate: true
    },
    cache : false
  })

  .state('app.contacts', {
    url: "/contacts",
    views: {
      'menuContent': {
        templateUrl: "templates/contacts.html",
        controller: 'ContactsCtrl'
      }
    },
    data: {
      authenticate: true
    },
    //cache : false
  })
  
  .state('app.settings', {
    url: "/settings",
    views: {
      'menuContent': {
        templateUrl: "templates/settings.html",
        controller: 'SettingsCtrl'
      }
    },
    data: {
      authenticate: true
    },
    cache : false
  })

  .state('app.invite', {
    url: "/invite",
    views: {
      'menuContent': {
        templateUrl: "templates/invite.html",
        controller: 'InviteCtrl'
      }
    },
    data: {
      authenticate: true
    },
    cache : false
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');
});


