angular.module('starter.services', [])

.constant("FACEBOOK_APP_ID", "1576276665934195")//antes
.constant("ApiUrl", 'http://ec2-52-89-203-255.us-west-2.compute.amazonaws.com:3000')
.constant("ApiSocialUrl", 'http://ec2-52-89-203-255.us-west-2.compute.amazonaws.com:3000/satellizer')


/*.constant("ApiUrl", 'http://localhost:3000')
.constant("ApiSocialUrl", 'http://localhost:3000/satellizer')*/


.service('UserService', function() {

//for the purpose of this example I will store user data on ionic local storage but you should save it on a database
  /*
  id_face
  id_twit
  id_inst
  user_id // identificador del usuario en la tabla
  */

  var setUser = function(user_data) {
    window.localStorage['user'] = JSON.stringify(user_data);
  };

  var getUser = function(){
    return JSON.parse(window.localStorage['user'] || '{}');
  };

  var deleteUser = function(){
      window.localStorage.clear();
  };

  return {
    getUser: getUser,
    setUser : setUser,
    deleteUser : deleteUser
  };
})

.factory('UserFactory',function($http,ApiUrl){

  return {
    login: function(vData){
      return $http.get(ApiUrl + '/user/email/'+vData.user_email+'/password/'+vData.user_pass); 
    },
    register : function(vData){
      return $http.post(ApiUrl + '/user',vData); 
    },
    update : function(vData){
      return $http.put(ApiUrl + '/user',vData); 
    },
    exists : function(vEmail){
      return $http.get(ApiUrl + '/user/email/'+vEmail); 
    },
    getUser : function(vId){
      return $http.get(ApiUrl + '/user/id/'+vId); 
    } 
  };

})


.factory('UserAccountFactory',function($http,ApiUrl,$q){

  return{
    register : function(vData){
      return $http.post(ApiUrl + '/user-account',vData); 
    },
    exists : function(vData){
      return $http.get(ApiUrl + '/user-account/userId/'+vData.user_id+'/social/'+vData.social); 
    },
    update : function(vData){
      return $http.put(ApiUrl + '/user-account',vData); 
    },
    getAll : function(vUserId){
      return $http.get(ApiUrl + '/user-account/userId/'+vUserId); 
    },
    getAllActive : function(vUserId){
      return $http.get(ApiUrl + '/user-account/userId/'+vUserId+'/active'); 
    },
    existsPerfilSocial : function(vIdPerfil){
      return $http.get(ApiUrl + '/user-account/id-perfil/'+vIdPerfil); 
    },
    associateAccount : function(vDataValida, dataPerfil){

      var me = this;
      var deferAssociate = $q.defer();

      me.existsPerfilSocial(dataPerfil.id_perfil.toString()).success(function(data){

        var vData = {
          user_id: vDataValida.user_id,
          id_perfil: dataPerfil.id_perfil.toString(),
          social_account : vDataValida.social,
          access_token : dataPerfil.access_token,
          email: dataPerfil.email,
          picture : dataPerfil.picture

        };

        if (data.length > 0) {

          if(data[0].userId == vDataValida.user_id){

            me.update(vData).success(function(data) {

              deferAssociate.resolve(true);

            });

          }else{

            deferAssociate.resolve("0");//esta cuenta ya esta asociado a otro usuario
          }

        }else{

          me.register(vData).success(function(data) {

            deferAssociate.resolve(true);

          });

        }

      });

      /*me.exists(vDataValida).success(function(data, status, headers, config) {

        var vData = {
          user_id: vDataValida.user_id,
          id_perfil: dataPerfil.id_perfil.toString(),
          social_account : vDataValida.social,
          access_token : dataPerfil.access_token,
          email: dataPerfil.email,
          picture : dataPerfil.picture

        }

        if (data.length > 0) {

          me.update(vData).success(function(data) {

            deferAssociate.resolve(true);

          });

        }else{  

          me.register(vData).success(function(data) {

            deferAssociate.resolve(true);

          });

        }

      });*/

      return deferAssociate.promise;

    }
  };

})



.factory('UserMediaFactory', function($rootScope, ApiUrl,$http){

  return {
    addMedia : function(vData){
       return $http.post(ApiUrl + '/user-media',vData); 
    },
    getMediaAll : function(vData){
      return $http.get(ApiUrl + '/user-media/userId/'+vData.user_id+'/social/'+vData.social); 
    },
    getMedia : function(vData){
      return $http.get(ApiUrl + '/user-media/mediaId/'+vData.media+'/social/'+vData.social); 
    }
  };

})


;
