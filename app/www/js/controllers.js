angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $state, $ionicPopup, UserService, $ionicLoading, FACEBOOK_APP_ID,$rootScope,
  $ionicHistory,$cordovaFacebook) {
   $scope.user = UserService.getUser();

   // A confirm dialog to be displayed when the user wants to log out
   $scope.showConfirmLogOut = function() {
     var confirmPopup = $ionicPopup.confirm({
       title: 'Log out',
       template: 'Are you sure you want to log out?'
     });
     confirmPopup.then(function(res) {
       if(res) {
         //logout
         $ionicLoading.show({
           template: 'Loging out...'
         });

          UserService.deleteUser();

          $rootScope.auth = false;

          $cordovaFacebook.logout();
      
          $ionicHistory.clearHistory();
          $ionicHistory.clearCache();

          $ionicLoading.hide();
          $state.go('login');
  
       }
     });
   };

})

.controller('LoginCtrl', function($scope, $state, $q, UserService, $ionicLoading, FACEBOOK_APP_ID,$http,
  ApiUrl,ApiSocialUrl,UserFactory,LoginFaceFactory,TwitterFactory,UserAccountFactory,InstagramFactory,$cordovaOauth,$rootScope,$ionicPopup,$timeout,MailFactory,$cordovaFacebook,$cordovaNetwork) {

  $scope.slideIndex = 0;

  $rootScope.auth = false;

  $scope.loginData = {
      user_email: '',
      user_pass: ''
  };

  if (typeof($rootScope.idSession) != "undefined") {
    $state.go('app.settings');
  }


  $scope.loginInstagram = function(){

      $ionicLoading.show({
          template: 'Loging in...'
      });

      $cordovaOauth.instagram("3d2337eca9ed4277a07962a821579891", ['likes','relationships']).then(function(result) {

        var vAccessToken = result.access_token;

        var vPos = vAccessToken.indexOf('.');
        var vId = vAccessToken.substring(0,vPos);

        var vParams = {
          user_id : vId,
          access_token : vAccessToken
        };

        InstagramFactory.getUserDataLogin(vParams).success(function(response) {

          var vData = {
            name : response.data.full_name,
            first_name : response.data.displayName,
            last_name : '',
            id_inst : response.data.id,
            picture : response.data.profile_picture,
            id : response.data.id,
            email : response.data.id +'@tada.com',
            insta_token : vAccessToken,
            access_token : vAccessToken
          };

          sessionUser('instagram',vData);
        }).error(function (error){

          $ionicLoading.hide();
        	
          $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
        	
        });
      }, function(error) {

        $ionicLoading.hide();
    	  $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });

      });
  };

  $scope.loginTwitter = function(){

    $ionicLoading.show({
        template: 'Loging in...'
    });

    var clientId = 'z3W5b6lvxAHNrueE1iREpJMxQ';
    var clientSecret = 'k0vab1S1drvsLxeryDF8BWR0gqy7Kr2Ct3Ppoyi7zSFg3unax1';

    $cordovaOauth.twitter(clientId,clientSecret,{redirect_uri: "https://tinyurl.com/krmpchb"}).then(function(result) {
        var vParams = {
          user_id : result.user_id,
          access_token : result.oauth_token
        };

        TwitterFactory.getPerfilData(vParams).success(function(data) {

          var vData = {
            name : data.name,
            first_name : data.name,
            last_name : '',
            id_twit : data.id,
            picture : data.profile_image_url.replace('_normal', ''),
            id : data.id,
            email : data.id +'@tada.com',
            access_token : result.oauth_token,
            twit_token : result.oauth_token,
            token_secret : result.oauth_token_secret
          };

          sessionUser('twitter',vData);

        });
                
    }, function(error) {

      $ionicLoading.hide();
    	error = error || 'Twitter login problems. Try again.';
		  $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
    });
  };

  $scope.login = function() {
    if ($scope.loginData.user_email!==""  && $scope.loginData.user_pass!=="") {

        if ($scope.loginData.user_email !="undefined") {

          $ionicLoading.show({
            template: 'Loging in...'
          });
          
          UserFactory.login($scope.loginData).success(function(data, status, headers, config) {

            var vValida = JSON.stringify(data);

            $ionicLoading.hide();

            if (vValida.indexOf("id") != -1) {

              var userData = {
                picture : '',
                access_token : '',
                fb_token : '',
                id_face : '',
                first_name : data[0].firstName,
                last_name : '',
                emailFace : '',
                name : data[0].firstName +" "+ data[0].lastName,
                id : data[0].id,
                email : data[0].userEmail
              };

              sessionUser("",userData);
              
            }else{

              $ionicLoading.show({ template: "Incorrect password and user", noBackdrop: true, duration: 2000 });

            }

                  
           });
    
        }else{

           $ionicLoading.show({ template: "Incorrect email", noBackdrop: true, duration: 2000 });

        }

    }else{

       $ionicLoading.show({ template: "Incorrect email and password", noBackdrop: true, duration: 2000 });

    }
  };

  $scope.loginFacebook = function(){

    $ionicLoading.show({
          template: 'Loging in...'
        });

    $cordovaFacebook.getLoginStatus()
      .then(function(success) {

        if (success.status == "connected") {

          loginFacebookExtend(success.authResponse.accessToken);

        }else{

          $cordovaFacebook.login(['email',
            //'publish_actions',//no deja loguear
            'public_profile',
            'user_location',
            'user_friends',
            'user_photos',
            'user_status'])

          .then(function(success) {

            $cordovaFacebook.api(
              'me',
              ['publish_actions'],
              function (res)
              {
                 
              },
              function (err)
              {
            
              });

              loginFacebookExtend(success.authResponse.accessToken);

          }, function (error) {

            $ionicLoading.hide();
            $ionicLoading.show({ template: JSON.stringify(error), noBackdrop: true, duration: 2000 });

          });

        }

    });

   /*$cordovaOauth.facebook(FACEBOOK_APP_ID, 
      ['email',
      'public_profile',
      'user_location',
      'user_friends',
      'user_photos',
      'publish_actions',
      //'user_posts',
      'user_status'],{redirect_uri: "https://www.facebook.com/connect/login_success.html"}).then(function(result) {

          LoginFaceFactory.getFacebookProfileInfo(result.access_token).success(function(resultData) {

              var userData = {
                picture : resultData.picture.data.url,
                access_token : result.access_token,
                fb_token : result.access_token,
                id_face : resultData.id,
                first_name : resultData.name,
                last_name : '',
                emailFace : resultData.email,
                name : resultData.name,
                id : resultData.id,
                email : resultData.email
              };

              sessionUser("facebook",userData);

          }).error(function(response){
              $ionicLoading.hide();
          });

      }, function(error) {

	  	  $ionicLoading.hide();
		    $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });

      });*/
  };

  var loginFacebookExtend = function(accessToken){

    LoginFaceFactory.getFacebookProfileInfo(accessToken).success(function(resultData) {

      var userData = {
        picture : resultData.picture.data.url,
        access_token : accessToken,
        fb_token : accessToken,
        id_face : resultData.id,
        first_name : resultData.name,
        last_name : '',
        emailFace : resultData.email,
        name : resultData.name,
        id : resultData.id,
        email : resultData.email
      };

      sessionUser("facebook",userData);

    }).error(function(response){

      $ionicLoading.hide();

    });
  };

  var setUserData = function(data,userSession){

    var userData = userSession;
    var userDefer = $q.defer();

    angular.forEach(data, function(item){
        userData.user_id = item.id;
        userData.password = item.userPass;
        userData.email = item.userEmail;

        userData.visible_radius = item.visibleRadius;

        if (item.firstName !=="") {
          userData.first_name = item.firstName;
        }

        if (item.lastName !=="") {
          userData.last_name = item.lastName;
        }

        if (item.lastName !== "" && item.firstName !== "") {
          userData.name = item.firstName +" "+ item.lastName;
        }else{
          userData.name = item.firstName;
        }
                   
    });

    UserAccountFactory.getAllActive(userData.user_id).success(function(data, status, headers, config) {

      var vDataAccount = {};

      angular.forEach(data, function(item){
          
        if(item.socialAccount == "facebook"){

          vDataAccount.fb_token = item.accessToken;
          vDataAccount.id_face = item.idPerfil;

        }

        if(item.socialAccount == "twitter"){

          vDataAccount.twit_token = item.accessToken;
          vDataAccount.id_twit = item.idPerfil;

        }

        if(item.socialAccount == "instagram"){

          vDataAccount.insta_token = item.accessToken;
          vDataAccount.id_inst = item.idPerfil;

        }

        if (item.picture !=="") {

          vDataAccount.picture = item.picture;

        }
 
      });

      userDefer.resolve(vDataAccount);

    });

    $rootScope.auth = true;

    userDefer.promise.then(function(response){

      var valida = JSON.stringify(response);

      if(valida.indexOf("id_face") >-1){

        userData.fb_token = response.fb_token;
        userData.id_face = response.id_face;

      }

      if(valida.indexOf("id_twit") >-1){

        userData.twit_token = response.twit_token;
        userData.id_twit = response.id_twit;

      }

      if(valida.indexOf("id_inst") >-1){

        userData.insta_token = response.insta_token;
        userData.id_inst = response.id_inst;
      }

      if(valida.indexOf("picture") >-1){

        userData.picture = response.picture;

      }

      return userData;
      

    }).then(function(data){

      UserService.setUser(data);

      $ionicLoading.hide();

      if (localStorage.urlIntent) {

        $state.go('app.publish');

      }else{

        $state.go('app.map');

      }
      

    });
  };

  var sessionUser = function(social,userSession){

    var userData = userSession;

    var myPopup = null;

    var vDataValida = {
      user_id : '',
      social : social
    };

    var vDataPerfil = {
      id_perfil: userData.id,
      access_token : userData.access_token,
      email: userData.email,
      picture : userData.picture
    };

    UserFactory.exists(userData.email).success(function(data, status, headers, config) {

      if (data.length > 0) {

        $rootScope.idSession = userData.id;
        $rootScope.social = social;

        vDataValida.user_id = data[0].id;

        if (social !=="") {

          UserAccountFactory.associateAccount(vDataValida,vDataPerfil).then(function(response){

            setUserData(data,userSession);

          });

        }else{

          setUserData(data,userSession);

        }
  
      }else{

        UserAccountFactory.existsPerfilSocial(userData.id).success(function(datas, status, headers, config) {

          if (datas.length > 0) {

            $rootScope.idSession = userData.id;
            $rootScope.social = social;

            var vIdUser = "";

            angular.forEach(datas, function(item){
              vIdUser = item.userId;
            });

            vDataValida.user_id = vIdUser;

            UserFactory.getUser(vIdUser).success(function(data, status, headers, config) {

              UserAccountFactory.associateAccount(vDataValida,vDataPerfil).then(function(response){

                setUserData(data,userSession);

              });

            });

          }else{

            var dataUsuario = {
              first_name: userData.first_name,
              last_name:  userData.last_name,
              user_email: userData.email,
              user_pass: '',
              status: '0',
              visible_radius : '500'
            };

            if (social == "twitter" || social == "instagram") {

              $ionicLoading.hide();

              $scope.data = {};

              myPopup = $ionicPopup.show({
                template: '<input type="email" ng-model="data.email" placeholder=" e-mail"></br> <input type="password" ng-model="data.password" placeholder=" password">',
                title: 'Important',
                subTitle: 'Define your email and password for your Tada Account',
                scope: $scope,
                buttons: [

                {
                  text: 'Cancel',
                  onTap: function(e) {
                    return $scope.data;
                  }
                },
          
                {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  onTap: function(e) {

                    if (!$scope.data.email || !$scope.data.password) {
                      e.preventDefault();
                    } else {
                      return $scope.data;
                    }                      
                  }
                }
                ]
              });

              myPopup.then(function(response) {

                if (response.email!== undefined) {

                  UserFactory.exists(response.email).success(function(data, status, headers, config) {

                    if(data.length > 0){
                      
                      $ionicLoading.show({
                        template: 'This email is already being used'
                      });

                      $timeout(function() {
                        myPopup.close(); 
                        $ionicLoading.hide();
                      }, 5000);

                    }else{

                      dataUsuario.user_email = response.email;
                      dataUsuario.user_pass = response.password;

                      userData.email = response.email;
                      //userData.current_pass = response.password;

                      registrarUsuario(dataUsuario,social,vDataValida,vDataPerfil,userData,userSession);
                    }

                  });

                }

              });

            }else{

              $ionicLoading.hide();

              $scope.data = {};

              myPopup = $ionicPopup.show({
                template: '<input type="password" ng-model="data.password" placeholder=" password">',
                title: 'Define your pass for your Tada Account',
                subTitle: 'your pass',
                scope: $scope,
                buttons: [

                {
                  text: 'Cancel',
                  onTap: function(e) {
                    return $scope.data;
                  }
                },
          
                {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  onTap: function(e) {

                    if (!$scope.data.password) {
                      e.preventDefault();
                    } else {
                      return $scope.data;
                    }                      
                  }
                }
                ]
              });

              myPopup.then(function(response) {

                if (response.password!== undefined) {

                  dataUsuario.user_pass = response.password;

                 // userData.current_pass = response.password;

                  registrarUsuario(dataUsuario,social,vDataValida,vDataPerfil,userData,userSession);

                }

              });

            }

          }

        }).error(function(error){

          $ionicLoading.hide();

        });

      }

    }).error(function(error){

      $ionicLoading.hide();

    });
  };

  var registrarUsuario = function(dataUsuario,social,vDataValida,vDataPerfil,userData,userSession){

    UserFactory.register(dataUsuario).success(function(data, status, headers, config) {

      $rootScope.idSession = vDataPerfil.id_perfil;
      $rootScope.social = social;

      UserFactory.exists(dataUsuario.user_email).success(function(data, status,headers,config){

        vDataValida.user_id = data[0].id;

        UserAccountFactory.associateAccount(vDataValida,vDataPerfil).then(function(response){

          setUserData(data,userSession);

          var vEmailNotify = {
              email : dataUsuario.user_email,
              password : dataUsuario.user_pass
          };

          MailFactory.registerNotify(vEmailNotify);

        });

      });

    });
  };

})

.controller('ProfileCtrl', function($scope, $state, UserService, $cordovaFacebook,$cordovaGeolocation,$ionicLoading,TwitterFactory,
  InstagramFactory,FacebookFactory,ImageFactory,$timeout) {

  var markersArray = [];
  var bounds = new google.maps.LatLngBounds();
  var map = null;
  var infowindow = 0;
  var geocoder = new google.maps.Geocoder();
  var searchBox = null;
  var markerCluster = null;
  var markerMe = 0;
  var circle = null;

  $scope.user = UserService.getUser();

  $scope.feedLoad = {
    data : ""
  };


  var init = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

      var mapOptions = {
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center : {lat: position.coords.latitude, lng: position.coords.longitude}
      };

      map = new google.maps.Map(document.getElementById("mapa"),mapOptions);

      $scope.loadPosition("init");

    }, onGeolocationError);

  };

  var centerOnMe = function(position, noCenter){

    var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    if (markerMe !==0) {
     // markerMe.setMap(null)
      markerMe.setPosition( myLatLng );
    } else {
 
      markerMe = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'You are here',
        icon : 'img/blue_marker.png'
      });
    }
    
    if( ! noCenter ){
      map.setZoom(10);
      map.setCenter(myLatLng);
    }
    var infoWindowMe = 0;
    
    google.maps.event.addListener(markerMe, "click", function() {
      if (infoWindowMe) infoWindowMe.close();

      infoWindowMe = new google.maps.InfoWindow({
        content: "<div style='font-weight:bold'>You  are here</div>"
        //maxWidth: 300,
    
      });

      infoWindowMe.open(map, markerMe);

    });
  };

  var onGeolocationError = function (error){
  
   var message = 'GPS problem. Try again.';

    switch(error.code) {
      case error.PERMISSION_DENIED:
      message = "User denied the request for Geolocation. Please enable your GPS and restart the application";
      break;
      case error.POSITION_UNAVAILABLE:
      message = "Location information is unavailable. Please enable your GPS and restart the application";
      break;
      case error.TIMEOUT:
      message = "The request to get user location timed out. Please restart the application";
      break;
      case error.UNKNOWN_ERROR:
      message = "An unknown error occurred. Please restart the application";
      break;
    }
        
    $ionicLoading.show({ template: message, noBackdrop: true, duration: 2000 });
  };

  $scope.loadPosition = function(vOpcion){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    switch(vOpcion) {

      case 'init':

        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          loadMaps(position);
        }, onGeolocationError);
      
      break;

      case 'mi-ubicacion':

       $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          centerOnMe(position);
       }, onGeolocationError);
        
      break;
    }  
  };

  var loadMaps = function(position) {

    $ionicLoading.show({
      template: 'Loading Map...'
    });

    var vData = {
      user_id : '',
      access_token : '',
      lat : position.coords.latitude,
      lng : position.coords.longitude,
      id : $scope.user.user_id

    };

    $scope.feedLoad.data = "(Loading ...)";

    if($scope.user.fb_token){

      vData.user_id = $scope.user.id_face;
      vData.access_token = $scope.user.fb_token;

      var vDataFace = [];

      vDataFace.push({id:$scope.user.id_face});

      feedFacebook(vData,vDataFace);

    }

    if($scope.user.twit_token){

      vData.access_token = $scope.user.twit_token;

      var vDataTwit = [];

      var vIdTwitter = {id_str:$scope.user.id_twit};

      vDataTwit.push(vIdTwitter);

      feedTwitter(vData,vDataTwit);
   
    }

    if($scope.user.insta_token){

      vData.access_token = $scope.user.insta_token;

      var vDataInsta = [];

      var vIdInsta = {id:$scope.user.id_inst};

      vDataInsta.push(vIdInsta);

      feedInstagram(vData,vDataInsta,"perfil");

    }

    $timeout(function() {

     $ionicLoading.hide();

    }, 3000);

  };

  var feedInstagram = function(vData,vContact,vOpcion){

    InstagramFactory.getFriendsLocation(vData,vContact,vOpcion).then(function(data) {

      console.log("data final instagram total" + data.length);

      $scope.feedLoad.data = "";
    
      if(data.length > 0){

       angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          if ( photos.location.id) {
            locationName = null;
            id = photos.location.id;
            locationType = "loc-place";
            content = '';
          } else {
            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";
            content = '';
          }

          var picture = photos.images.low_resolution.url ? photos.images.low_resolution.url : "";

          var styleImage = "";

          if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

            styleImage = "max-height:85%;";

          }else{

            styleImage = "max-width:45%;";

          }

          content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px'  href='#'  >" + photos.user.username + "</a><br/></div>";

          content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
          content += "<div  style='display:block;font-weight:bold;color: #000'>";

          if(photos.estado == "0"){

            content += "<div style='display:block;text-align:center'>"+photos.caption.text+"</div>";

          }else{

            content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of 500m from this photo.</div>";
          
          }

          content += "</div></div>";

    
          addPin(photos.location.latitude, photos.location.longitude, locationName, id, locationType, content,"",photos.user.visible_radius,photos.estado);
          bounds.extend(new google.maps.LatLng(photos.location.latitude, photos.location.longitude));

       });
         
      }else{

        $ionicLoading.show({ template: "data not found instagram", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));

      }
      mapBounds();

    });
  };

  var feedTwitter = function(vData,vContact){

    TwitterFactory.getTweetsLocation(vData,vContact).then(function(data){

      $scope.feedLoad.data = "";

      if(data.length === 0){

        $ionicLoading.show({ template: "data not found twitter", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }else{

        angular.forEach(data, function(photos) {

          var locationName, id, locationType, content, picture;

          locationName = null;
          id = "loc-" + photos.id;
          locationType = "loc-gps";
          content = '';

          picture = photos.entities.media[0].media_url ;

          var styleImage = "";

          if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

            styleImage = "max-height:85%;";

          }else{

            styleImage = "max-width:45%;";

          }

          content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_image_url + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' >" + photos.user.name+ "</a><br/></div>";

          content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' style='"+styleImage+"' border='0'></div>";
          content += "<div  style='display:block;font-weight:bold;color: #000'>";

          if(photos.estado == "0"){

           content += "<div style='display:block;'>"+photos.text+"</div>";
          
          }else{

            content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of 500m from this photo.</div>";
          }

          content += "</div></div>";

          addPin(photos.geo.coordinates[0], photos.geo.coordinates[1], locationName, id, locationType, content,"",photos.visible_radius,photos.estado);
          bounds.extend(new google.maps.LatLng(photos.geo.coordinates[0], photos.geo.coordinates[1]));

        });

      }

      mapBounds();
 
    });
  };

  var feedFacebook = function(vData,vId){

    FacebookFactory.getFriendsData(vData,vId).then(function(data){

      $scope.feedLoad.data = "";

      angular.forEach(data, function(friends) {

          var vUser = friends.user;

          angular.forEach(friends.feed, function(feed) {

            var locationName, id, locationType, content, picture;

            locationName = null;
            id = "loc-" + friends.id;
            locationType = "loc-gps";
            content = '';
            picture = '';

            angular.forEach(feed.image, function(photo) {

              picture = photo.source;

            });

            console.log(picture);

            var styleImage = "";

            if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

              styleImage = "max-height:85%;";

            }else{

              styleImage = "max-width:45%;";

            }

            content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
            content += "<div  style='display:block;font-weight:bold;color: #000'>";

            if(feed.estado == "0"){

              if (typeof(feed.name) != "undefined") {
                content += "<div style='display:block;text-align:center'>"+feed.name+"</div>";
              }

              content += "<div style='display:block;text-align:center'><img src='" + vUser[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#' target='_blank'>" + friends.name + "</a><br/></div>";

            }else{

              content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of "+feed.visible_radius+"m from this photo.</div>";

              content += "<div style='display:block;text-align:center'><img src='" + vUser[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#' target='_blank'>" + friends.name + "</a><br/></div>";

            }

            content += "</div></div>";
            
            addPin(feed.place.location.latitude, feed.place.location.longitude, locationName, id, locationType, content,"",feed.visible_radius,feed.estado);
            bounds.extend(new google.maps.LatLng(feed.place.location.latitude, feed.place.location.longitude));
                
          });

      });

      if(data.length === 0){

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }

      mapBounds();

      var vJsonUser = JSON.stringify($scope.user);

      if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_twit") == -1){

        centerOnMe({
          coords: {
            latitude: vData.lat,
            longitude: vData.lng
          }
        });

      }

    },function(error){

      bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
      mapBounds();

    });
  };

  var escHtml = function (text) {
      return text.replace(/</g, '&lt;').replace(/>/g, '&gt;');
  };

  var mapBounds = function () {
      map.fitBounds(bounds);
      map.setZoom(2);
  };

  var addPin = function (lat, lng, name, id, type, content, icon,radius,status){

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat, lng),
      map: map,
      title: name,
      icon: icon,
      radius : radius,
      lat : lat,
      lng : lng,
      status : status
    });

    marker.type = type;

    google.maps.event.addListener(marker, "click", function() {
      if (infowindow) infowindow.close();

      infowindow = new google.maps.InfoWindow({
        content: content
    
      });

      if (marker.status == "1") {

        if (circle !==null) {
          circle.setMap(null);
        }

        circle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          map: map,
          center: {lat: marker.lat, lng: marker.lng},
          radius: marker.radius

        });

        circle.bindTo('center', marker, 'position');

      }

      infowindow.open(map, marker);

      google.maps.event.addListener(infowindow, 'closeclick', function() {
       //showLocationPhotos(1, id, name)
      });
      //showLocationPhotos(0, id, name)
    });
    markersArray.push(marker);

    return marker;
  };

  ionic.Platform.ready(init);

})

.controller('UserCtrl', function($scope, $state, UserService,$stateParams,$cordovaGeolocation,$ionicLoading,TwitterFactory,
  InstagramFactory,FacebookFactory,ImageFactory,UserAccountFactory,UserFactory,$timeout,$rootScope) {

  var markersArray = [];
  var bounds = new google.maps.LatLngBounds();
  var map = null;
  var infowindow = 0;
  var geocoder = new google.maps.Geocoder();
  var searchBox = null;
  var markerCluster = null;
  var markerMe = 0;
  var circle = null;

  $scope.user = UserService.getUser();

  $scope.contact = {
    picture : '',
    name : ''
  };

  $scope.feedLoad = {
    data : ""
  };

  var centerOnMe = function(position, noCenter){

    var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    if (markerMe !==0) {
     // markerMe.setMap(null)
      markerMe.setPosition( myLatLng );
    } else {
 
      markerMe = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'You are here',
        icon : 'img/blue_marker.png'
      });
    }
    
    if( ! noCenter ){
      map.setZoom(10);
      map.setCenter(myLatLng);
    }
    var infoWindowMe = 0;
    
    google.maps.event.addListener(markerMe, "click", function() {
      if (infoWindowMe) infoWindowMe.close();

      infoWindowMe = new google.maps.InfoWindow({
        content: "<div style='font-weight:bold'>You  are here</div>"
        //maxWidth: 300,
    
      });

      infoWindowMe.open(map, markerMe);

    });
  };

  var onGeolocationError = function (error){
  
   var message = 'GPS problem. Try again.';

    switch(error.code) {
      case error.PERMISSION_DENIED:
      message = "User denied the request for Geolocation. Please enable your GPS and restart the application";
      break;
      case error.POSITION_UNAVAILABLE:
      message = "Location information is unavailable. Please enable your GPS and restart the application";
      break;
      case error.TIMEOUT:
      message = "The request to get user location timed out. Please restart the application";
      break;
      case error.UNKNOWN_ERROR:
      message = "An unknown error occurred. Please restart the application";
      break;
    }
        
    $ionicLoading.show({ template: message, noBackdrop: true, duration: 2000 });
  };

  $scope.loadPosition = function(vOpcion,vData){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    switch(vOpcion) {

      case 'init':

        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          loadMaps(position,vData);
        }, onGeolocationError);
      
      break;

      case 'mi-ubicacion':

       $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          centerOnMe(position);
       }, onGeolocationError);
        
      break;
    }  
  };

  var verPerfil = function(){

    var vParams = {};

    UserAccountFactory.existsPerfilSocial($stateParams.id).success(function(data){

      if (data.length > 0) {

        $scope.contact.picture = data[0].picture;

        UserFactory.getUser(data[0].userId).success(function(data){

          $scope.contact.name = data[0].firstName +" "+ data[0].lastName;

          $ionicLoading.hide();

          UserAccountFactory.getAllActive(data[0].id).success(function(perfiles){

            $scope.loadPosition("init",perfiles);

         });

        });

      }else{

        vParams = {
          user_id : $stateParams.id,
          access_token : ''
        };

        var vFoto = "";

        if ($stateParams.social == "twitter") {

          vParams.access_token = $scope.user.twit_token;

          TwitterFactory.getPerfilData(vParams).success(function(data) {

            vFoto = ImageFactory.getPictureFullSize(data.profile_image_url,$stateParams.social);

            $scope.contact.picture = vFoto;
            $scope.contact.name = data.name;

          });

        }

        if ($stateParams.social == "instagram") {

          vParams.access_token = $scope.user.insta_token;

          InstagramFactory.getUserDataLogin(vParams).success(function(response) {

            vFoto = ImageFactory.getPictureFullSize(response.data.profile_picture,$stateParams.social);

            $scope.contact.picture = vFoto;
            $scope.contact.name = response.data.full_name;

          });

        }

        if ($stateParams.social == "facebook") {

         vParams.access_token = $scope.user.fb_token;

         FacebookFactory.getContactInfo(vParams).success(function(resultData) {

            $scope.contact.picture = resultData.picture.data.url;
            $scope.contact.name = resultData.name;

         });

        }

        $ionicLoading.hide();

        $scope.loadPosition("init",null);

      }

    });

  };

  var loadMaps = function(position,data) {

    $ionicLoading.show({
      template: 'Loading Map...'
    });

    $scope.feedLoad.data = "(Loading ...)";

    if (data !== null) {
        
        angular.forEach(data,function(perfiles){

          switch(perfiles.socialAccount) {

            case 'facebook':

              listarMapaContact("facebook",perfiles.idPerfil,perfiles.accessToken,position);

            break;

            case 'twitter':

              listarMapaContact("twitter",perfiles.idPerfil,perfiles.accessToken,position);

            break;

            case 'instagram':

              listarMapaContact("instagram",perfiles.idPerfil,perfiles.accessToken,position);

            break;

          }

        });

    }else{

      if($stateParams.social == "twitter"){

        listarMapaContact("twitter",$stateParams.id,$scope.user.twit_token,position);

      }

      if($stateParams.social == "instagram"){

        listarMapaContact("instagram",$stateParams.id,$scope.user.insta_token,position);

      }

      if($stateParams.social == "facebook"){

        listarMapaContact("facebook",$stateParams.id,$scope.user.fb_token,position);

      }

    }

    $timeout(function() {

     $ionicLoading.hide();

    }, 5000);

  };

  var listarMapaContact = function(vSocial,vId,vAccessToken,vPosition){

    var vData = {
      user_id : vId,
      access_token : vAccessToken,
      lat : vPosition.coords.latitude,
      lng : vPosition.coords.longitude,

    };

    switch(vSocial) {

      case 'facebook':

        var vDataFace = [];

        vDataFace.push({id:vId});

        friendsDataFacebook(vData,vDataFace);

      break;

      case 'twitter':

        var vDataTwit = [];

        vDataTwit.push({id_str:vId});

        friendsDataTwitter(vData,vDataTwit);

      break;

      case 'instagram':

        var vDataInsta = [];

        vDataInsta.push({id:vId});

        friendsDataInstagram(vData,vDataInsta,"perfil");

      break;

    }

  };

  var friendsDataInstagram = function(vData,vContact,vOpcion){

    InstagramFactory.getFriendsLocation(vData,vContact,vOpcion).then(function(data) {

      console.log("data final instagram total" + data.length);

      $scope.feedLoad.data = "";
    
      if(data.length > 0){

       angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          if ( photos.location.id) {
            locationName = null;
            id = photos.location.id;
            locationType = "loc-place";
            content = '';
          } else {
            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";
            content = '';
          }

          var picture = photos.images.low_resolution.url ? photos.images.low_resolution.url : "";

          var styleImage = "";

          if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

            styleImage = "max-height:85%;";

          }else{

            styleImage = "max-width:45%;";

          }

          content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px'  href='#'  >" + photos.user.username + "</a><br/></div>";

          content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
          content += "<div  style='display:block;font-weight:bold;color: #000'>";

          if(photos.estado == "0"){

            content += "<div style='display:block;text-align:center'>"+photos.caption.text+"</div>";

          }else{

            content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br>  radius of "+photos.user.visible_radius+"m from this photo.</div>";
          
          }

          content += "</div></div>";

    
          addPin(photos.location.latitude, photos.location.longitude, locationName, id, locationType, content,"",photos.user.visible_radius,photos.estado);
          bounds.extend(new google.maps.LatLng(photos.location.latitude, photos.location.longitude));

       });
         
      }else{

        $ionicLoading.show({ template: "data not found", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));

      }
      mapBounds();

    });
  };

  var friendsDataTwitter = function(vData,vContact){

    TwitterFactory.getTweetsLocation(vData,vContact).then(function(data){

      $scope.feedLoad.data = "";

      if(data.length === 0){

        $ionicLoading.show({ template: "data not found", noBackdrop: true, duration: 2000 });

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }else{

        angular.forEach(data, function(photos) {

          var locationName, id, locationType, content, picture;

          locationName = null;
          id = "loc-" + photos.id;
          locationType = "loc-gps";
          content = '';

          picture = photos.entities.media[0].media_url ;

          var styleImage = "";

          if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

            styleImage = "max-height:85%;";

          }else{

            styleImage = "max-width:45%;";

          }

          content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_image_url + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' >" + photos.user.name+ "</a><br/></div>";

          content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' style='"+styleImage+"' border='0'></div>";
          content += "<div  style='display:block;font-weight:bold;color: #000'>";

          if(photos.estado == "0"){

           content += "<div style='display:block;'>"+photos.text+"</div>";
          
          }else{

            content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of 500m from this photo.</div>";
          }

          content += "</div></div>";

          addPin(photos.geo.coordinates[0], photos.geo.coordinates[1], locationName, id, locationType, content,"",photos.visible_radius,photos.estado);
          bounds.extend(new google.maps.LatLng(photos.geo.coordinates[0], photos.geo.coordinates[1]));

        });

      }

      mapBounds();
 
    });
  };

  var friendsDataFacebook = function(vData,vId){

    FacebookFactory.getFriendsData(vData,vId).then(function(data){

      $scope.feedLoad.data = "";

      angular.forEach(data, function(friends) {

          var vUser = friends.user;

          angular.forEach(friends.feed, function(feed) {

            var locationName, id, locationType, content, picture;

            locationName = null;
            id = "loc-" + friends.id;
            locationType = "loc-gps";
            content = '';
            picture = '';

            angular.forEach(feed.image, function(photo) {

              picture = photo.source;

            });

            var styleImage = "";

            if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

              styleImage = "max-height:85%;";

            }else{

              styleImage = "max-width:45%;";

            }

            content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
            content += "<div  style='display:block;font-weight:bold;color: #000'>";

            if(feed.estado == "0"){

              if (typeof(feed.name) != "undefined") {
                content += "<div style='display:block;'>"+feed.name+"</div>";
              }

              content += "<div style='display:block;text-align:center'><img src='" + vUser[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#' target='_blank'>" + friends.name + "</a><br/></div>";

            }else{

              content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of "+feed.visible_radius+"m from this photo.</div>";

              content += "<div style='display:block;text-align:center'><img src='" + vUser[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#' target='_blank'>" + friends.name + "</a><br/></div>";

            }

            content += "</div></div>";
            
            addPin(feed.place.location.latitude, feed.place.location.longitude, locationName, id, locationType, content,"",feed.visible_radius,feed.estado);
            bounds.extend(new google.maps.LatLng(feed.place.location.latitude, feed.place.location.longitude));
                
          });

      });

      if(data.length === 0){

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }

      mapBounds();

      var vJsonUser = JSON.stringify($scope.user);

      if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_twit") == -1){

        centerOnMe({
          coords: {
            latitude: vData.lat,
            longitude: vData.lng
          }
        });

      }

    },function(error){

      bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
      mapBounds();

    });
  };

  var escHtml = function (text) {
      return text.replace(/</g, '&lt;').replace(/>/g, '&gt;');
  };

  var mapBounds = function () {
      map.fitBounds(bounds);
      map.setZoom(2);
  };

  var addPin = function (lat, lng, name, id, type, content, icon,radius,status){

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat, lng),
      map: map,
      title: name,
      icon: icon,
      radius : radius,
      lat : lat,
      lng : lng,
      status : status
    });

    marker.type = type;

    google.maps.event.addListener(marker, "click", function() {
      if (infowindow) infowindow.close();

      infowindow = new google.maps.InfoWindow({
        content: content
    
      });

      if (marker.status == "1") {

        if (circle !==null) {
          circle.setMap(null);
        }

        circle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          map: map,
          center: {lat: marker.lat, lng: marker.lng},
          radius: marker.radius

        });

        circle.bindTo('center', marker, 'position');

      }

      infowindow.open(map, marker);

      google.maps.event.addListener(infowindow, 'closeclick', function() {
       //showLocationPhotos(1, id, name)
      });
      //showLocationPhotos(0, id, name)
    });
    markersArray.push(marker);

    return marker;
  };

  var init = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

      var mapOptions = {
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center : {lat: position.coords.latitude, lng: position.coords.longitude}
      };

      map = new google.maps.Map(document.getElementById("mapa"),mapOptions);

    }, onGeolocationError);

  };

  ionic.Platform.ready(init);

  verPerfil();

})

.controller('PublishCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,TwitterFactory,FacebookFactory,ApiUrl,FileFactory,InstagramFactory,UserAccountFactory,$cordovaOauth,LoginFaceFactory,CameraFactory,
  $cordovaFileTransfer,$cordovaGeolocation,$cordovaFacebook,UserMediaFactory) {

  $scope.user = UserService.getUser();
  
  $scope.vStatus = true;
  $scope.vPhoto = false;
  $scope.vUpload = true;

  var myDropzone = null;

  $scope.publish = {
    message : '',
    status_photo : '',
    photos : '',
    lat: '',
    long: ''
  };

  $scope.upload = {
    image : ''
  };

  $scope.chk = {
    facebook : false,
    twitter : false
  };

  $scope.disabled = {
    facebook : true,
    facebookA : true,
    twitter : true,
    twitterA : true
  };

  var vImages = [];

  var init = function(){

    myDropzone = new Dropzone("#photo-upload", { 
      url: ApiUrl+"/file/upload",
      addRemoveLinks : true,
      dictRemoveFile : "Delete",
      maxFiles : 1,
      dictDefaultMessage : 'Select your photo'
    });

    myDropzone.on("addedfile", function(file) {
      vImages.push(file.name);
    });

    myDropzone.on("removedfile", function(file) {

      FileFactory.deleted(file.name).success(function(data, status, headers, config) {

      });

    });

    if (localStorage.urlIntent) {

      uploadPhoto(localStorage.urlIntent);

    }

    listAccountAsociate();

  };

  var listAccountAsociate = function(){

     UserAccountFactory.getAllActive($scope.user.user_id).success(function(data, status, headers, config) {

        angular.forEach(data, function(item){

            if (item.socialAccount == "facebook") {
              $scope.user.id_face = item.idPerfil;
              $scope.user.fb_token = item.accessToken;
              $scope.chk.facebook = true; 

              $scope.disabled.facebook = false;
              $scope.disabled.facebookA = true;
 
            }else{
              $scope.chk.facebook = false; 
              $scope.disabled.facebookA = false;
              $scope.disabled.facebook = true;
            }

            if (item.socialAccount == "twitter") {

              $scope.user.id_twit = item.idPerfil;
              $scope.user.twit_token= item.accessToken;
              $scope.chk.twitter = true;

              $scope.disabled.twitter = false;
              $scope.disabled.twitterA = true;

            }else{
              $scope.chk.twitter = false;
              $scope.disabled.twitterA = false;
              $scope.disabled.twitter = true;
            }

            if (item.socialAccount == "instagram") {
              $scope.user.id_inst = item.idPerfil;
              $scope.user.insta_token= item.accessToken; 
            }

        });

        UserService.setUser($scope.user);

     });
  };

  $scope.asociarCuenta = function(social){

    var vDataPerfil = {
      id_perfil: $scope.user.id,
      access_token : '',
      email: $scope.user.email,
      picture : ''
    };

    switch(social) {
      case 'facebook':

        $cordovaOauth.facebook(FACEBOOK_APP_ID, 
          ['email',
          'public_profile',
          'user_location',
          'user_friends',
          'user_photos',
          ],{redirect_uri: "https://www.facebook.com/connect/login_success.html"}).then(function(result) {

            LoginFaceFactory.getFacebookProfileInfo(result.access_token).success(function(resultData) {

             vDataPerfil.id_perfil = resultData.id;
             vDataPerfil.access_token = result.access_token;
             vDataPerfil.email = resultData.email;
             vDataPerfil.picture = "http://graph.facebook.com/"+ resultData.id +"/picture?type=large";

             $scope.user.id_face = resultData.id;
             $scope.user.fb_token = result.access_token;
             $scope.user.emailFace = resultData.email;

             UserService.setUser($scope.user);

             asociar(social,vDataPerfil);

           });

          }, function(error) {

          });
      
      break;

      case 'twitter':

        var clientId = 'z3W5b6lvxAHNrueE1iREpJMxQ';
        var clientSecret = 'k0vab1S1drvsLxeryDF8BWR0gqy7Kr2Ct3Ppoyi7zSFg3unax1';

        $cordovaOauth.twitter(clientId,clientSecret,{redirect_uri: "https://tinyurl.com/krmpchb"}).then(function(result) {
          var vParams = {
            user_id : result.user_id,
            access_token : result.oauth_token
          };

          TwitterFactory.getPerfilData(vParams).success(function(data) {

            var vData = {
              name : data.name,
              first_name : data.name,
              last_name : '',
              id_twit : data.id,
              picture : data.profile_image_url.replace('_normal', ''),
              id : data.id,
              email : data.id +'@tada.com',
              access_token : result.oauth_token,
              twit_token : result.oauth_token,
              token_secret : result.oauth_token_secret
            };

            vDataPerfil.id_perfil = data.id;
            vDataPerfil.access_token = result.oauth_token;
            vDataPerfil.email = vData.email;
            vDataPerfil.picture = vData.picture;

            UserService.setUser(vData);

            asociar(social,vDataPerfil);

          });

        }, function(error) {
          alert(JSON.stringify(error));
          $ionicLoading.hide();
        });
      
      break;

      case 'instagram':
      break;
    
    }
  };

  var asociar = function(social,dataPerfil){

    var vDataValida = {
      user_id : $scope.user.user_id,
      social : social
    };

    UserAccountFactory.associateAccount(vDataValida,dataPerfil).then(function(response){

      if (response === 0 || response == "0"){

        $ionicLoading.show({ template: "Account is registered", noBackdrop: true, duration: 2000 });

      }else{

        switch(social) {
          case 'facebook':
          $scope.disabled.facebook = false;
          $scope.disabled.facebookA = true;
          $scope.chk.facebook = true;
          break;

          case 'twitter':
          $scope.disabled.twitter = false;
          $scope.disabled.twitterA = true;
          $scope.chk.twitter = true;
          break;

          case 'instagram':
          break;

        }

      }

    });
  };

  $scope.selectOptionPublish = function(vOption){

    /*if (vOption == 'status') {

      $scope.vStatus = false;
      $scope.vPhoto = true;

    }else{

      $scope.vStatus = true;
      $scope.vPhoto = false;

    }*/
  };

  $scope.publishStatus = function(){

    if(vImages.length > 0 && $scope.publish.status_photo !== ""){

      if($scope.chk.facebook === true || $scope.chk.twitter === true){

        $ionicLoading.show({
          template: 'Publishing...'
        });

        if(typeof($scope.user.id_face) != "undefined" && $scope.user.id_face !=="" && $scope.chk.facebook === true){
          publishStatusFacebook();
        }

        setTimeout(function(){
          if(typeof($scope.user.id_twit) != "undefined" && $scope.user.id_twit !=="" && $scope.chk.twitter === true){
            publishStatusTwitter();
          }

          $ionicLoading.hide();

          setTimeout(function(){

            $ionicLoading.show({
              template: 'Published Successfully'
            });

            setTimeout(function(){

              deleteImages();

              resetForm();

              $ionicLoading.hide();

              //$state.go('app.publish','',{ reload: true });

            },3000);

          },2500);

        },11500);

      }else{

        $ionicLoading.show({
          template: 'Select at least 1 network to publish'
        });

        setTimeout(function(){
          $ionicLoading.hide();
        },5000);

      }

    }else{

      $ionicLoading.show({
        template: 'Select image and insert text'
      });

      setTimeout(function(){
        $ionicLoading.hide();
      },5000);

    }
  };

  var resetForm = function(){

    myDropzone.removeAllFiles();

    $scope.vUpload = true;
    $scope.vPhoto = false;
    $scope.upload.image = '';
    $scope.publish.status_photo = '';
  };

  var deleteImages = function(){

      angular.forEach(vImages, function(item){

        FileFactory.deleted(item);
              
      });

      vImages.length = 0;
  };

  var publishStatusFacebook = function(){

    $scope.publish.photos = vImages;
    $scope.publish.access_token = $scope.user.fb_token;
    $scope.publish.user_id = $scope.user.id_face;

    FacebookFactory.publishFeed($scope.publish).success(function(data, status, headers, config) {

      var vOptions = {timeout : 10000 , enableHighAccuracy : false};

      $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

        var vDataMedia = {
          user_id : $scope.user.user_id,
          media_id : data,
          social : 'facebook',
          lat : position.coords.latitude,
          lng : position.coords.longitude
        };

        UserMediaFactory.addMedia(vDataMedia).success(function(data){

          console.log("se registro la data" + JSON.stringify(vDataMedia));

        });

      },function(){});

    });

  };

  var publishStatusTwitter = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

      $scope.publish.photos = vImages;
      $scope.publish.access_token = $scope.user.twit_token;
      $scope.publish.access_token_secret = $scope.user.token_secret;

      if ($scope.publish.lat === "") {

        $scope.publish.lat = position.coords.latitude;
        $scope.publish.long = position.coords.longitude;
      }

      TwitterFactory.publishStatus($scope.publish);

    });

  };

  $scope.publishStatusInstagram = function(){

    $scope.publish.photos = vImages;

    console.log(JSON.stringify($scope.publish));

    InstagramFactory.publishStatus($scope.publish).success(function(data, status, headers, config) {

      console.log("res: "+JSON.stringify(data));

      angular.forEach(vImages, function(item){

        FileFactory.deleted(item);

      });

      vImages.length = 0;


    });
  };

  $scope.FacebookOptions = function() {
    if($scope.vfacebook === false){
      $scope.vfacebook = true;
    }else{
      $scope.vfacebook = false;
    }
  };

  $scope.TwitterOptions = function() {
    if($scope.vtwitter === false){
      $scope.vtwitter = true;
    }else{
      $scope.vtwitter = false;
    }
  };

  $scope.InstagramOptions = function() {
    if($scope.vinstagram === false){
      $scope.vinstagram = true;
    }else{
      $scope.vinstagram = false;
    }
  };

  $scope.capturePhoto = function(){

    CameraFactory.getPicture({
      quality: 85,
      targetWidth: 620,
      targetHeight: 620,
      saveToPhotoAlbum: false
    }).then(function(imageURI) {
    
      uploadPhoto(imageURI);
    }, function(err) {
      alert(err);
    });
  };

  var uploadPhoto = function(uriPhoto){

    var options = {
        fileKey: "file",
        fileName: uriPhoto.split("/").pop(),
        chunkedMode: true,
        mimeType: "image/jpg"
    };

    $cordovaFileTransfer.upload(ApiUrl+"/file/upload", uriPhoto,options)
      .then(function(result) {

        $scope.vPhoto = true;
        $scope.vUpload = false;

        if (localStorage.urlIntent) {

          window.FilePath.resolveNativePath(uriPhoto, 

            function(data){

              $scope.upload.image = data;

            }, function(error){

              alert("error " + error);

            }
          );

          localStorage.removeItem("urlIntent");

        }else{

          $scope.upload.image = uriPhoto;

          CordovaExif.readData(uriPhoto, 
            function(exifObject) {

              if (exifObject !==undefined && exifObject.GPSLatitude) {

                $scope.publish.lat = exifObject.GPSLatitude;
                $scope.publish.long = exifObject.GPSLongitude;

              }
            },function(error){
              //alert("error " + error);
            }
          );
        }

        vImages.push(options.fileName);
        
      }, function(err) {
         //alert("error" + err);
      }, function (progress) {
        // constant progress updates
      });
  };

  init();
})

.controller('MapCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,TwitterFactory,
  InstagramFactory,FacebookFactory,UserAccountFactory,$rootScope,$cordovaGeolocation,$timeout,
  $cordovaNetwork,ConnectivityFactory,$interval) {

  var markersArray = [];
  var bounds = new google.maps.LatLngBounds();
  var map = null;
  var infowindow = 0;
  var geocoder = new google.maps.Geocoder();
  var searchBox = null;
  var markerCluster = null;
  var markerMe = 0;
  var circle = null;

  $scope.listSocialMap = true;

  $scope.option = {
    facebook : false,
    twitter : false,
    instagram : false
  };

  $scope.listTwitter = true;
  $scope.listInstagram = true;
  $scope.listFacebook = true;

  $scope.user = UserService.getUser();

  $scope.feedLoad = {
    data : ""
  };

  var vContactInst = [];
  var vContactFace = [];
  var vContactTwit = [];

  var vIniContact = 0;
  var vFinContact = 20;

  var vConnectivity = "";

  var inputSearch = document.getElementById('search');

  var centerOnMe = function(position, noCenter){

    var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    if (markerMe !==0) {
     // markerMe.setMap(null)
    	markerMe.setPosition( myLatLng );
    } else {
 
	    markerMe = new google.maps.Marker({
	      position: myLatLng,
	      map: map,
	      title: 'You are here',
	      icon : 'img/blue_marker.png'
	    });
    }
    
    if( ! noCenter ){
	    map.setZoom(10);
	    map.setCenter(myLatLng);
    }
    var infoWindowMe = 0;
    
    google.maps.event.addListener(markerMe, "click", function() {
      if (infoWindowMe) infoWindowMe.close();

      infoWindowMe = new google.maps.InfoWindow({
        content: "<div style='font-weight:bold'>You  are here</div>"
        //maxWidth: 300,
    
      });

      infoWindowMe.open(map, markerMe);

    });
  };

  var onGeolocationError = function (error){
  
	 var message = 'GPS problem. Try again.';

    switch(error.code) {
      case error.PERMISSION_DENIED:
      message = "User denied the request for Geolocation. Please enable your GPS and restart the application";
      break;
      case error.POSITION_UNAVAILABLE:
      message = "Location information is unavailable. Please enable your GPS and restart the application";
      break;
      case error.TIMEOUT:
      message = "The request to get user location timed out. Please restart the application";
      break;
      case error.UNKNOWN_ERROR:
      message = "An unknown error occurred. Please restart the application";
      break;
    }
  	    
  	$ionicLoading.show({ template: message, noBackdrop: true, duration: 2000 });
	};
  
  var geoTracker = function (){
    console.log('geoTracker');

    var vOptions = { frequency: 1000, timeout: 30000 };

    var onSuccess = function(position){

      console.log('latitute: ' + position.coords.latitude);
      console.log('longitude: ' + position.coords.longitude);
      console.log('$scope.user.user_id: ' + $scope.user.user_id);

      centerOnMe(position, true);
      geoTracker();

	  };

	  var promise = $cordovaGeolocation.getCurrentPosition(vOptions);
	  //var watchID = navigator.geolocation.watchPosition(onSuccess, onGeolocationError, vOptions);
	  
	  promise.then(onSuccess, onGeolocationError);
  };

  $scope.visibleOptionSocial = function(){

    if($scope.listSocialMap === true){
      $scope.listSocialMap = false;
    }else{
      $scope.listSocialMap = true;
    }
  };

  $scope.selectSocialMap = function(){
    loadPosition("checked");  
  };

  var socialCheckedMap = function(position){

    var vData = {
        user_id : '',
        access_token : '',
        lat : position.coords.latitude,
        lng : position.coords.longitude,
        distance : '10000',
        geocode : position.coords.latitude + "," + position.coords.longitude + ",1mi",
    };

    clearMarkers();

    if ($scope.option.facebook === true || $scope.option.instagram === true || $scope.option.twitter === true) {

      $ionicLoading.show({
        template: 'Loading Map...'
      });

    }

    if($scope.option.facebook === true){

      console.log("listando data face");

      vData.user_id = $scope.user.id_face;
      vData.access_token = $scope.user.fb_token;

      friendsDataFacebook(vData);

    }

    if($scope.option.instagram === true){

      console.log("listando data insta");

      vData.user_id = $scope.user.id_inst;
      vData.access_token = $scope.user.insta_token;

      friendsDataInstagram(vData);
    }

    if($scope.option.twitter === true){

      console.log("listando data twitter");

      vData.user_id = $scope.user.id_twit;
      vData.access_token = $scope.user.twit_token;

      friendsDataTwitter(vData);
    }
  };
 
  $scope.loadPosition = function(vOpcion){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    switch(vOpcion) {

      case 'init':

        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          loadMaps(position);
        }, onGeolocationError);
      
      break;

      case 'search':
        $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          socialCheckedMap(position);
        }, onGeolocationError);

      break;

      case 'mi-ubicacion':

       $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){
          centerOnMe(position);
       }, onGeolocationError);
        
      break;
    }  
  };

  var addPin = function (lat, lng, name, id, type, content, icon,radius,status){

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat, lng),
      map: map,
      title: name,
      icon: icon,
      radius : radius,
      lat : lat,
      lng : lng,
      status : status
    });

    marker.type = type;

    google.maps.event.addListener(marker, "click", function() {
      if (infowindow) infowindow.close();

      infowindow = new google.maps.InfoWindow({
        content: content
        //maxWidth: 300,
    
      });

      if (marker.status == "1") {

        if (circle !==null) {
          circle.setMap(null);
        }

        circle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          map: map,
          center: {lat: marker.lat, lng: marker.lng},
          radius: marker.radius
          //radius: Math.sqrt(marker.radius)
        });

        circle.bindTo('center', marker, 'position');

      }

      infowindow.open(map, marker);


      google.maps.event.addListener(infowindow, 'closeclick', function() {
       //showLocationPhotos(1, id, name)
      });
      //showLocationPhotos(0, id, name)
    });
    markersArray.push(marker);

    return marker;
  };

  var escHtml = function (text) {
      return text.replace(/</g, '&lt;').replace(/>/g, '&gt;');
  };

  var clearMarkers = function () {
    while (markersArray.length) {
      markersArray.pop().setMap(null);
    }
    markerCluster.clearMarkers();
  };

  var mapBounds = function () {
      map.fitBounds(bounds);
   
      map.setZoom(2);
  
      //alert("total pines"+ markersArray.length);

      markerCluster = new MarkerClusterer(map,markersArray);

      $ionicLoading.hide();
  };

  $scope.enterSearch = function(keyEvent){

    if (keyEvent.which === 13){
      $scope.searchLoaction();
    }
  };

  $scope.searchLoaction = function(){

    if(inputSearch.value !==""){
      var places = searchBox.getPlaces();

      /*if (places.length == 0) {
        return;
      }*/

      places.forEach(function(place) {

        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();

        var myLatLng = new google.maps.LatLng(lat, lng);

        map.setCenter(myLatLng);

        map.setZoom(10);
        
      });
    }
  };

	var loadMaps = function(position) {

    $ionicLoading.show({
      template: 'Loading Map...'
    });

    var vData = {
      user_id : '',
      access_token : '',
      lat : position.coords.latitude,
      lng : position.coords.longitude,
      distance : '10000',
      geocode : position.coords.latitude + "," + position.coords.longitude + ",1mi"

    };

    $scope.feedLoad.data = "(Loading ...)";

    if($scope.user.id_face){

      vData.user_id = $scope.user.id_face;
      vData.access_token = $scope.user.fb_token;

      friendsDataFacebook(vData,vContactFace);

      $scope.listFacebook = false;
      $scope.option.facebook = true;

    }

    if($scope.user.id_twit){

      vData.user_id = $scope.user.id_twit;
      vData.access_token = $scope.user.twit_token;

      var vTwitterCont = vContactTwit.slice(vIniContact,vFinContact);

      friendsDataTwitter(vData,vTwitterCont);
      
      $scope.listTwitter = false;
      $scope.option.twitter = true;

    }

    if($scope.user.id_inst){

      vData.user_id = $scope.user.id_inst;
      vData.access_token = $scope.user.insta_token;

      //var vInstagramCont = vContactInst.slice(vIniContact,vFinContact);

      friendsDataInstagram(vData,"mapa");
      
      $scope.listInstagram = false;
      $scope.option.instagram = true;
 
    }

   /* vIniContact +=100;
    vFinContact +=100; */

    $timeout(function() {

     $ionicLoading.hide();

    }, 3000);
	};

  var friendsDataFacebook = function(vData,vFriends){

    FacebookFactory.getFriendsData(vData,vFriends).then(function(data){

      $scope.feedLoad.data = "";

      angular.forEach(data, function(friends) {

          var vUser = friends.user;

          angular.forEach(friends.feed, function(feed) {

            var locationName, id, locationType, content, picture;

            locationName = null;
            id = "loc-" + friends.id;
            locationType = "loc-gps";
            content = '';
            picture = '';

            angular.forEach(feed.image, function(photo) {

              picture = photo.source;

            });

            var styleImage = "";

            if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

              styleImage = "max-height:85%;";

            }else{

              styleImage = "max-width:45%;";

            }

            content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
            content += "<div  style='display:block;font-weight:bold;color: #000'>";

            if(feed.estado == "0"){

              if (typeof(feed.name) != "undefined") {
                content += "<div style='display:block;'>"+feed.name+"</div>";
              }

              content += "<div style='display:block;text-align:center'><img src='" + vUser[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#' target='_blank'>" + friends.name + "</a><br/></div>";

            }else{

              content += "<div style='display:block;padding-bottom: 10px;text-align: center;'>to view this photo you need to be in </br> radius of "+feed.visible_radius+"m from this photo.</div>";

              content += "<div style='display:block;text-align:center'><img src='" + vUser[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#/app/user/"+friends.id +"/social/facebook' nav-transition='none'>" + friends.name + "</a><br/></div>";

            }

            content += "</div></div>";
            
            addPin(feed.place.location.latitude, feed.place.location.longitude, locationName, id, locationType, content,"",feed.visible_radius,feed.estado);
            bounds.extend(new google.maps.LatLng(feed.place.location.latitude, feed.place.location.longitude));
                
          });

      });

      if(data.length === 0){

        bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
      }
      mapBounds();

      var vJsonUser = JSON.stringify($scope.user);

      if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_twit") == -1){

        if (vContactFace.length === 0 || data.length <=1) {

          $ionicLoading.show({ template: "Invite to your friends to use tada to see their geolocalized photos", noBackdrop: true, duration: 6000 });

          $timeout(function() {

           FacebookFactory.inviteFriends();

         }, 9000);

        }

        centerOnMe({
          coords: {
            latitude: vData.lat,
            longitude: vData.lng
          }
        });

      }

    },function(error){

      //alert("error" + JSON.stringify(error));
 
      bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
      mapBounds();

    });
  };

  var friendsDataTwitter = function(vData,vContact){

      TwitterFactory.getTweetsLocation(vData,vContact).then(function(data){

        $scope.feedLoad.data = "";

        if(data.length === 0){

          bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));
         
        }else{

          angular.forEach(data, function(photos) {

            var locationName, id, locationType, content, picture;

            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";
            content = '';

            picture = photos.entities.media[0].media_url ;

            var styleImage = "";

            if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

              styleImage = "max-height:85%;";

            }else{

              styleImage = "max-width:45%;";

            }

            content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_image_url + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#/app/user/"+photos.user.id_str+"/social/twitter' nav-transition='none'>" + photos.user.name+ "</a><br/></div>";

            content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' height='auto' style='"+styleImage+"' border='0'></div>";
            content += "<div  style='display:block;font-weight:bold;color: #000'>";

            if(photos.estado == "0"){

            content += "<div style='display:block;'>"+photos.text+"</div>";
        
            }else{

              content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of "+photos.user.visible_radius+"m from this photo.</div>";
            }

            content += "</div></div>";

            addPin(photos.geo.coordinates[0], photos.geo.coordinates[1], locationName, id, locationType, content,"",photos.user.visible_radius,photos.estado);
            bounds.extend(new google.maps.LatLng(photos.geo.coordinates[0], photos.geo.coordinates[1]));

          });

        }
        
        mapBounds();

        var vJsonUser = JSON.stringify($scope.user);

        if(vJsonUser.indexOf("id_inst") == -1 && vJsonUser.indexOf("id_face") == -1){

          centerOnMe({
            coords: {
              latitude: vData.lat,
              longitude: vData.lng
            }
          });

        }
 
      });

  };

  var friendsDataInstagram = function(vData,vOpcion){

    var vTotal = vContactInst.length;
    var vInstagramCont = [];

    do {

      vInstagramCont = vContactInst.slice(vIniContact,vFinContact);

      InstagramFactory.getFriendsLocation(vData,vInstagramCont,vOpcion).then(function(data) {

        if(data.length > 0){

         angular.forEach(data, function(photos) {

          var locationName, id, locationType, content;

          if ( photos.location.id) {
            locationName = escHtml(photos.location.name);
            id = photos.location.id;
            locationType = "loc-place";
            content = '';
          } else {
            locationName = null;
            id = "loc-" + photos.id;
            locationType = "loc-gps";
            content = '';
          }

          var picture = photos.images.low_resolution.url ? photos.images.low_resolution.url : "";

          var vDataUser = {

            userId : photos.user.id,
            picture : photos.user.profile_picture,
            name : photos.user.username

          };

          var styleImage = "";

          if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

            styleImage = "max-height:85%;";

          }else{

            styleImage = "max-width:45%;";

          }

          content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px'  href='#/app/user/"+photos.user.id +"/social/instagram' nav-transition='none' >" + photos.user.username + "</a><br/></div>";

          content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
          content += "<div  style='display:block;font-weight:bold;color: #000'>";

          if(photos.estado == "0"){

            content += "<div style='display:block;text-align:center'>"+photos.caption.text+"</div>";

          }else{

            content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of "+photos.user.visible_radius+"m from this photo.</div>";

          }

          content += "</div></div>";

          addPin(photos.location.latitude, photos.location.longitude, locationName, id, locationType, content,"",photos.user.visible_radius,photos.estado);
          bounds.extend(new google.maps.LatLng(photos.location.latitude, photos.location.longitude));

          });

        } else{

          bounds.extend(new google.maps.LatLng(vData.lat, vData.lng));

        }
      
      });

      vIniContact +=20;
      vFinContact +=20;

    }

    while (vInstagramCont.length > 0);

    $timeout(function() {

      mapBounds();
      $scope.feedLoad.data = "";

    }, 9000);
  };

  var init = function(){

    var vOptions = {timeout : 10000 , enableHighAccuracy : false};

    $cordovaGeolocation.getCurrentPosition(vOptions).then(function(position){

        var mapOptions = {
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center : {lat: position.coords.latitude, lng: position.coords.longitude}
        };

        map = new google.maps.Map(document.getElementById("map"),mapOptions);

        searchBox = new google.maps.places.SearchBox(inputSearch);

        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        listContacts();

        searchBox.addListener('places_changed', function() {

        });

      }, onGeolocationError);
  };

  $scope.more = function(){

    var vInsta = vContactInst.slice(vIniContact,vFinContact);
    var vTwit = vContactTwit.slice(vIniContact,vFinContact);

    if(vInsta.length > 0 || vTwit.length > 0){

      $scope.loadPosition("init");

    }else{
      $ionicLoading.show({ template: "No content", noBackdrop: true, duration: 2000 });
    }

    $scope.$broadcast('scroll.refreshComplete');
  };

  var listContacts = function (){

    var vParams = {};

    var vTwitterDefer = [];
    var vInstaDefer = [];
    var vFacebookDefer = [];

    var vDefer = $q.defer();

    if ($scope.user.id_twit) {

      vParams = {
        user_id : $scope.user.id_twit,
        access_token : $scope.user.twit_token
      };

      vTwitterDefer = listContactTwitter(vParams);

    }else{

      vDefer.resolve("");
      vTwitterDefer = vDefer.promise;

    }

    if ($scope.user.id_face) {

      vParams = {
        user_id : $scope.user.id_face,
        access_token : $scope.user.fb_token
      };

      vFacebookDefer = listContactFacebook(vParams);

    }else{

      vDefer.resolve("");
      vFacebookDefer = vDefer.promise;

    }

    if ($scope.user.insta_token) {

      vParams = {
        user_id : $scope.user.id_inst,
        access_token : $scope.user.insta_token
      };

      vInstaDefer = listContactInstagram(vParams);

    }else{

      vDefer.resolve("");
      vInstaDefer = vDefer.promise;

    }

    vTwitterDefer.then(function(data){

      vContactTwit = data;

    }).then(function(){

       vInstaDefer.then(function(data){

        vContactInst = data;

       }).then(function(){

        vFacebookDefer.then(function(data){

          vContactFace = data.data || data;

          $scope.loadPosition("init");

        });

       });

    });
  };

  var listContactInstagram = function(vData){

    var deferInsta = $q.defer();

    InstagramFactory.getContacts(vData).then(function(data) {

      deferInsta.resolve(data);

    }).catch(function(error){

      deferInsta.reject("[]");

    });

    return deferInsta.promise;
  };

  var listContactTwitter = function(vData){

    var deferTwitter = $q.defer();

    TwitterFactory.getContacts(vData).then(function(data) {

      deferTwitter.resolve(data);

    },function(error){

      deferTwitter.reject("[]");

    });

    return deferTwitter.promise;
  };

  var listContactFacebook = function(vData){

    var deferFacebook = $q.defer();

    FacebookFactory.getFriends(vData).success(function(data, status, headers, config) {

      //alert("status" + status);

      deferFacebook.resolve(data);

    }).error(function(error){

      alert("error" +JSON.stringify(error));

      deferFacebook.reject("[]");

    });

    return deferFacebook.promise;
  };
  

  ionic.Platform.ready(init);

})

.controller('ContactsCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,InstagramFactory,
  UserAccountFactory,TwitterFactory,FacebookFactory,$rootScope,$cordovaFacebook) {
  
  $scope.user = UserService.getUser();

  $scope.contact = {
    facebook : '',
    twitter : '',
    instagram : '',
  };

  $scope.select = {
    socialOptions : '',
    data : ''
  };

  $scope.boxSelect = false;

  $scope.totalContact = {
    facebook : '',
    twitter : '',
    instagram : '',
    total : ''
  };

  $scope.viewProfileContact = function(profile){

    $state.go('app.user', { id : profile.id,social : profile.social });

  };

  var listSocialSelect = function(){

    var vSocialData = [];
    var itemSocial = {};

    if($scope.user.id_face){

      itemSocial = {id: 'facebook',name: 'Facebook'};
      vSocialData.push(itemSocial);

    }

    if($scope.user.id_twit){

      itemSocial = {id: 'twitter',name: 'Twitter'};
      vSocialData.push(itemSocial);
      
    }

    if($scope.user.id_inst){

      itemSocial = {id: 'instagram',name: 'Instagram'};
      vSocialData.push(itemSocial);
      
    }

    $scope.select.socialOptions = vSocialData;

    switch(vSocialData.length) {

        case 2:
          $scope.select.socialDefault = vSocialData[1];
        break;

        case 1:
          $scope.select.socialDefault = vSocialData[0];
        break;

        default:
          $scope.select.socialDefault = vSocialData[0];
        break;

      }
  };

  $scope.visibleContactFacebook = function(option){

    if (option == "contacts") {

      $scope.tabs.facebookContact = false;
      $scope.tabs.facebookTada = true;

      $scope.totalContact.total = $scope.totalContact.facebook;

    }else{

      $scope.tabs.facebookContact = true;
      $scope.tabs.facebookTada = false;

      $scope.totalContact.total = $scope.totalContact.facebookTada;

    }
  };

  $scope.filterContact = function(select){

    switch(select.id) {

      case 'facebook':
        $scope.contacts = $scope.contact.facebook;
    
        $scope.totalContact.total = $scope.totalContact.facebook;

      break;

      case 'twitter':
        $scope.contacts = $scope.contact.twitter;

        $scope.totalContact.total = $scope.totalContact.twitter;

      break;

      case 'instagram':
        $scope.contacts = $scope.contact.instagram;

        $scope.totalContact.total = $scope.totalContact.instagram;

      break;
    }
  };

  var listContact = function(){

    $ionicLoading.show({
      template: 'Loading Contacts...'
    });

    var vParams = {};

    if ($scope.user.id_face) {

      vParams = {
        user_id : $scope.user.id_face,
        access_token : $scope.user.fb_token
      };

      listContactFacebook(vParams);
    }

    if ($scope.user.id_twit) {

      vParams = {
        user_id : $scope.user.id_twit,
        access_token : $scope.user.twit_token
      };

      listContactTwitter(vParams);
    }

    if ($scope.user.insta_token) {

      vParams = {
        user_id : $scope.user.id_inst,
        access_token : $scope.user.insta_token
      };

      listContactInstagram(vParams);
    }
  };

  var listContactTwitter = function(vData){

    var vContactData = [];

    TwitterFactory.getContacts(vData).then(function(data){

      var vContact = {};

      angular.forEach(data, function(item){

        var vFoto = getFoto(item.profile_image_url,'twitter');

        vContact = {
          name : item.name,
          picture : item.profile_image_url,
          picture_original : vFoto,
          location: item.location,
          id : item.id_str,
          social : 'twitter'
        };

        vContactData.push(vContact);

      });

      $scope.contact.twitter= vContactData;

      $scope.contacts = $scope.contact.twitter;

      $scope.totalContact.twitter = vContactData.length;

      $scope.totalContact.total = vContactData.length;

      $ionicLoading.hide();

    });
  };

  var listContactFacebook = function(vData){

    /*if(typeof ($scope.user.id_inst) == "undefined" && typeof($scope.user.id_twit) == "undefined"){

      $scope.tabsContact = true;
      $scope.tabsFacebook = false;

    }*/

   /* FacebookFactory.getTaggableFriends(vData).success(function(data, status, headers, config) {

      var vContact = {};
      var vContactData = [];

      angular.forEach(data.data, function(item){

        vContact = {
          name : item.name,
          picture : item.picture.data.url,
          social : "facebook"
        };

        vContactData.push(vContact);

      });

      $scope.contact.facebook = vContactData;

      $scope.totalContact.facebook = vContactData.length;

      $scope.totalContact.total = vContactData.length;
  
      $ionicLoading.hide();

    });*/

    FacebookFactory.getFriends(vData).success(function(data) {

      var vContact = {};

      var vContactFace = [];

      angular.forEach(data.data, function(item){

        vContact = {
          name : item.name,
          picture : item.picture.data.url,
          id : item.id,
          social : "facebook"
        };

        vContactFace.push(vContact);

      });

      $scope.contact.facebook = vContactFace;

      $scope.contacts = $scope.contact.facebook;

      $scope.totalContact.facebook= vContactFace.length;

      $scope.totalContact.total = vContactFace.length;

      $ionicLoading.hide();

    });
  };

  var listContactInstagram = function(vData){

    var vContactData = [];

    InstagramFactory.getContacts(vData).then(function(data) {

      var vContact = {};

      angular.forEach(data, function(item){

        var vFoto = getFoto(item.profile_picture,'instagram');

        vContact = {
          name : item.full_name ? item.full_name  : item.username ,
          picture : item.profile_picture,
          picture_original : vFoto,
          social : 'instagram',
          id : item.id
        };

        vContactData.push(vContact);
      });

      $scope.contact.instagram = vContactData;
      $scope.contacts = $scope.contact.instagram;

      $scope.totalContact.instagram= vContactData.length;

      $scope.totalContact.total = vContactData.length;

      $ionicLoading.hide();

    });

  };

  var getFoto = function(vUrl,vSocial){

    var vFoto = "";

    switch(vSocial) {

      case 'facebook':
      break;

      case 'twitter':
        vFoto = vUrl.replace("_normal", "");
      break;

      case 'instagram':
        vFoto = vUrl.replace("/s150x150/", "/");
      break;

    }

    return vFoto;

  };

  var getUrl = function(url) {
      var path = new URL(url).pathname;
     var parts = path.split('/');

      parts = parts.filter(function(part) {
        return part.length !== 0;
      });

      return parts[parts.length - 1];
  };

  var getPageId = function(vUrl) {
     var vPos1 = vUrl.search("_");

     var vUrl2 = vUrl.substring(vPos1);
     var vPos2 = vUrl2.search("_");

     var vUrl3 = vUrl2.substring(vPos2+1);
     var vPos3 = vUrl3.search("_");

     var vUrl4 = vUrl3.substring(0,vPos3);

     return vUrl4;
  };

  listSocialSelect();

  listContact();

})

.controller('SettingsCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading,$http,ApiUrl,
  UserFactory,LoginFaceFactory,UserAccountFactory,TwitterFactory,InstagramFactory,$ionicPopup,$rootScope,$cordovaOauth) {

  $scope.user = UserService.getUser(); 

  $scope.socialButton = {
    facebook : '',
    twitter : '',
    instagram : ''
  };

  $scope.userPass = {
    new_pass: '',
    user_pass: '',
    current_pass: '',
    valida_new_pass: '',
    valida_user_pass: ''
  };

  var vValChangePass = 0;

  $scope.showAlertSocialAccount = function() {
     var alertPopup = $ionicPopup.alert({
       title: 'Alert',
       template: 'Associate social networks'
     });
     alertPopup.then(function(res) {
       console.log('Thank you for not eating my delicious ice cream cone');
     });
  };

  $scope.changeVisibleRadius = function(){

    document.getElementById('label-radius').innerHTML = $scope.user.visible_radius;
  };

  $scope.getSocialAccount = function(){

    UserAccountFactory.getAllActive($scope.user.user_id).success(function(data, status, headers, config) {

       angular.forEach(data, function(item){

          if (item.socialAccount == "facebook") {

            $scope.socialButton.facebook = true;

            $scope.user.fb_token = item.accessToken;
            $scope.user.id_face = item.idPerfil;
            $scope.user.emailFace = item.email;
          }

          if (item.socialAccount == "twitter") {
            
            $scope.socialButton.twitter = true;

            $scope.user.twit_token = item.accessToken;
            $scope.user.id_twit = item.idPerfil;
            $scope.user.emailTwit = item.email; 
          }

          if (item.socialAccount == "instagram") {
            
            $scope.socialButton.instagram = true;

            $scope.user.insta_token = item.accessToken;
            $scope.user.id_inst = item.idPerfil;
            $scope.user.emailInsta = item.email;
          }

       });
    });
  };

  $scope.pushRedSocialChange = function(social){

    var vDataPerfil = {
      id_perfil: $scope.user.id,
      access_token : '',
      email: $scope.user.email,
      picture : ''
    };

     switch(social) {
      case "facebook":

        if ($scope.socialButton.facebook === true) {

          $ionicLoading.show({
            template: 'associating ...'
          });

          if($rootScope.social == 'facebook'){

            vDataPerfil.access_token = $scope.user.fb_token;
            vDataPerfil.id_perfil = $scope.user.id_face;
            vDataPerfil.picture = "http://graph.facebook.com/"+$scope.user.id_face +"/picture?type=large";

            if(typeof($scope.user.emailFace) != "undefined"){

              vDataPerfil.email = $scope.user.emailFace;

            }

            asociarCuentaSocial('facebook',vDataPerfil);

          }else{

            $cordovaOauth.facebook(FACEBOOK_APP_ID, 
              ['email',
              'public_profile',
              'user_location',
              'user_friends',
              'user_photos',
              'user_status'],{redirect_uri: "https://www.facebook.com/connect/login_success.html"}).then(function(result) {

              LoginFaceFactory.getFacebookProfileInfo(result.access_token).success(function(resultData) {

                 vDataPerfil.id_perfil = resultData.id;
                 vDataPerfil.access_token = result.access_token;
                 vDataPerfil.email = resultData.email;
                 vDataPerfil.picture = "http://graph.facebook.com/"+ resultData.id +"/picture?type=large";

                 $scope.user.id_face = resultData.id;
                 $scope.user.fb_token = result.access_token;
                 $scope.user.emailFace = resultData.email;

                 UserService.setUser($scope.user);

                 asociarCuentaSocial('facebook',vDataPerfil);

              }).error(function(error){
                $ionicLoading.hide();

                $scope.socialButton.facebook = false;

                $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
              });

            }, function(error) {
              $ionicLoading.hide();

              $scope.socialButton.facebook = false;

              $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
            });

          }

        }else{

          $ionicLoading.show({
            template: 'Disassociating ...'
          });

          if(typeof($scope.user.emailFace) != "undefined"){

            vDataPerfil.id_perfil = $scope.user.id_face;
            vDataPerfil.email = $scope.user.emailFace;
            vDataPerfil.picture = "http://graph.facebook.com/"+$scope.user.id_face +"/picture?type=large";

          }

          asociarCuentaSocial('facebook',vDataPerfil);

        }

      break;

      case "twitter":

        if ($scope.socialButton.twitter === true) {

          $ionicLoading.show({
            template: 'Associating. ...'
          });

          if ($rootScope.social == "twitter") {
              vDataPerfil.access_token = $scope.user.twit_token;  

              asociarCuentaSocial('twitter',vDataPerfil);       
   
          }else{

            var clientId = 'z3W5b6lvxAHNrueE1iREpJMxQ';
            var clientSecret = 'k0vab1S1drvsLxeryDF8BWR0gqy7Kr2Ct3Ppoyi7zSFg3unax1';

            $cordovaOauth.twitter(clientId,clientSecret,{redirect_uri: "https://tinyurl.com/krmpchb"}).then(function(result) {
              var vParams = {
                user_id : result.user_id,
                access_token : result.oauth_token
              };

              TwitterFactory.getPerfilData(vParams).success(function(data) {

                vDataPerfil.access_token = vParams.access_token;
                vDataPerfil.id_perfil = data.id;
                vDataPerfil.email = data.id +'@tada.com';

                $scope.user.twit_token = vParams.access_token;
                $scope.user.id_twit = data.id;
                $scope.user.emailTwit = data.id +'@tada.com';

                UserService.setUser($scope.user);

                asociarCuentaSocial('twitter',vDataPerfil);

              }).error(function(error){

                $ionicLoading.hide();

                error = error || 'Twitter login problems. Try again.';

                $scope.socialButton.twitter = false;
                $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });

              });

            }, function(error) {
              error = error || 'Twitter login problems. Try again.';

              $scope.socialButton.twitter = false;
              $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
            });


          }

        }else{

          $ionicLoading.show({
            template: 'Disassociating ...'
          });

          vDataPerfil.id_perfil = $scope.user.id_twit;
          vDataPerfil.email = $scope.user.emailTwit;

          asociarCuentaSocial('twitter',vDataPerfil);

        }
       
      break;

      case "instagram":

        if ($scope.socialButton.instagram === true) {

          $ionicLoading.show({
            template: 'Aassociating ...'
          });

          if ($rootScope.social == "instagram") {

            vDataPerfil.access_token = $scope.user.insta_token;  
            asociarCuentaSocial('instagram',vDataPerfil);  

          }else{

            $cordovaOauth.instagram("3d2337eca9ed4277a07962a821579891", ['likes','relationships']).then(function(result) {

              var vAccessToken = result.access_token;

              var vPos = vAccessToken.indexOf('.');
              var vId = vAccessToken.substring(0,vPos);

              var vParams = {
                user_id : vId,
                access_token : vAccessToken
              };

              InstagramFactory.getUserDataLogin(vParams).success(function(response) {

                vDataPerfil.access_token = vAccessToken;
                vDataPerfil.id_perfil = response.data.id;
                vDataPerfil.email = response.data.id +'@tada.com';

                $scope.user.twit_token = vAccessToken;
                $scope.user.id_inst = response.data.id;
                $scope.user.emailInsta = response.data.id +'@tada.com';

                UserService.setUser($scope.user);
                asociarCuentaSocial('instagram',vDataPerfil);

              });

            }, function(error) {
              $ionicLoading.hide();

              $scope.socialButton.instagram = false;

              $ionicLoading.show({ template: error, noBackdrop: true, duration: 2000 });
            });

          }     
   
        }else{

          $ionicLoading.show({
            template: 'Disassociating ...'
          });
       
          vDataPerfil.id_perfil = $scope.user.id_inst;
          vDataPerfil.email = $scope.user.emailInsta;
          
          asociarCuentaSocial('instagram',vDataPerfil);
        }

     
      break;
      
    }
  };

  var asociarCuentaSocial = function(social,dataPerfil){

      var vDataValida = {
        user_id : $scope.user.user_id,
        social : social
      };

      UserAccountFactory.associateAccount(vDataValida,dataPerfil).then(function(response){

        $ionicLoading.hide();

        if (response === 0){

          switch(social) {
            case "facebook":
              $scope.socialButton.facebook = false;
            break;

            case "twitter":
              $scope.socialButton.twitter = false;
            break;

            case "instagram":
              $scope.socialButton.instagram = false;
            break;
            
          }

          $ionicLoading.show({ template: "Account is registered", noBackdrop: true, duration: 2000 });
        }

      
      });
  };
  
  $scope.updateData = function(formData){

    var userData = {
        first_name: $scope.user.first_name,
        last_name:  $scope.user.last_name,
        user_email: $scope.user.email,
        user_pass:  $scope.user.current_pass,
        fb_token:   '',
        insta_token: '',
        twit_token: '',
        visible_radius : $scope.user.visible_radius,
        status: '',
        id: $scope.user.user_id
    };

    if (formData.$valid) {

      if($scope.userPass.new_pass !== ""){

          userData.user_pass = $scope.userPass.new_pass;
      }

      if ($scope.userPass.new_pass == $scope.userPass.user_pass) {

        $ionicLoading.show({
          template: 'Saved changes ...'
        });

        UserFactory.update(userData).success(function(data, status, headers, config) {

          $scope.user.name = userData.first_name +" "+ userData.last_name;
          //$scope.user.current_pass = userData.user_pass;
          
          UserService.setUser($scope.user);

          $ionicLoading.hide();

          if (vValChangePass == 1) {

            $ionicPopup.alert({
             title: 'Success!',
             template: 'Password Changed Successfully'
            });

          }

          $state.go('app.settings',{ reload: true });

        });

      }else{

       $ionicPopup.alert({
         title: 'Alert!',
         template: 'Error password '
       });

     }

    }
  };

  $scope.currentPass = function(vCurrentPass){

    if (vCurrentPass != $scope.user.password) {

      $ionicPopup.alert({
        title: 'Alert!',
        template: 'the password is incorrect'
      });

      $scope.userPass.current_pass = true;

    }else{
      $scope.userPass.current_pass = false;
    }
  };

  $scope.newPass = function(vNewPass){

    if (vNewPass == $scope.user.current_pass || vNewPass == $scope.user.password) {

      $ionicPopup.alert({
        title: 'Alert!',
        template: 'use different password than current'
      });

      $scope.userPass.valida_new_pass = true;

    }else{
      $scope.userPass.valida_new_pass = false;
    }

  };

  $scope.confirmPass = function(vConfirmPass){

    if (vConfirmPass != $scope.userPass.new_pass) {

      $scope.userPass.valida_user_pass = true;

      vValChangePass = 0;

    }else{

      $scope.userPass.valida_user_pass = false;

      vValChangePass = 1;
    }

  };

  var init = function(){
    document.getElementById('label-radius').innerHTML = $scope.user.visible_radius;
  };

  init();
  $scope.getSocialAccount();
})

.controller('InviteCtrl', function($scope, $state, UserService,FacebookFactory,$cordovaFacebook) {


  $scope.inviteFacebookContact = function(){

     //FacebookFactory.inviteFriends();
      
    /*$cordovaFacebook.appInvite(    
     {
      url: "https://fb.me/1730907037137823",
      picture: "http://www.tadageo.com/wp-content/uploads/2015/12/logo.png"
    }, 
    function(obj){

      alert("success " + JSON.stringify(obj));
        if(obj) {
            if(obj.completionGesture == "cancel") {
              $ionicLoading.show({ template: "Cancel", noBackdrop: true, duration: 2000 });
            } else {
               $ionicLoading.show({ template: "success!", noBackdrop: true, duration: 2000 });
            }
        } else {
            $ionicLoading.show({ template: "Cancel", noBackdrop: true, duration: 2000 });
        }
    }, 
    function(obj){

       alert("error " + JSON.stringify(obj));
        // error
        $ionicLoading.show({ template: "Cancel", noBackdrop: true, duration: 2000 });
    }
    );*/

  };

  $scope.inviteFacebookContact();


});




