angular.module('service.social', [])

.factory('LoginFaceFactory',function($q,$http){

  var fbLogged = $q.defer();

  return {
    fbLoginSuccess: function(response){

      if (!response.authResponse){
        fbLoginError("Cannot find the authResponse");
        return;
      }
      var expDate = new Date(
        new Date().getTime() + response.authResponse.expiresIn * 1000
        ).toISOString();

      var authData = {
        id: String(response.authResponse.userID),
        access_token: response.authResponse.accessToken,
        expiration_date: expDate
      };

      fbLogged.resolve(authData);

    },
    fbLoginError: function(error){
      fbLogged.reject(error);
      alert(error);
    },
    fbLogin: function(){

      facebookConnectPlugin.login(['email',
                                    'public_profile',
                                    'user_about_me',
                                    'user_likes',
                                    'user_location',
                                    'user_friends',
                                    'read_stream',
                                    'user_photos',
                                    'user_status'], this.fbLoginSuccess, this.fbLoginError);

       return fbLogged.promise;

    },
    getFacebookProfileInfo2: function(){
      var info = $q.defer();
      facebookConnectPlugin.api('/me', "",
        function (response) {
          info.resolve(response);
        },
        function (response) {
          info.reject(response);
        }
        );
      return info.promise;
    },
    getFacebookProfileInfo: function(vAccessToken){
      return $http.get("https://graph.facebook.com/me", {params: {access_token: vAccessToken, fields: "id,name,gender,location,picture.type(large),email", format: "json" }});
    }

  };

})

.factory('TwitterFactory',function($http,ApiSocialUrl,$q,UserAccountFactory,UserFactory,GeoFactory,ApiUrl,$cordovaNetwork){

  return{
    
    getFollowers : function(vAccessToken){
      return $http.post(ApiSocialUrl + '/twitter/followers',vAccessToken); 
    },
    getFollowing : function(vAccessToken){
      return $http.post(ApiSocialUrl + '/twitter/following',vAccessToken); 
    },
    getPerfilData : function(vData){
      return $http.get(ApiSocialUrl + '/twitter/me/'+vData.user_id+'/accessToken/'+vData.access_token); 
    },
    publishStatus : function(vData){
      return $http.post(ApiSocialUrl + '/twitter/status',vData);
    },
    getTweets : function(vData){
      return $http.post(ApiSocialUrl + '/twitter/tweets',vData);
    },
    getDataTweet : function(vData){
      return $http.post(ApiSocialUrl + '/twitter/show',vData);
    },
    getTweetsSearch : function(vData){
      return $http.post(ApiSocialUrl + '/twitter/tweets/search',vData);
    },
    getTweetsLocation : function(vData,vContact){

        var me = this;
        var deferTweetsFinal = $q.defer();

        var vAccessToken = vData.access_token;
        var vDataPhotos = [];

        angular.forEach(vContact, function(data) {

            var vUserData = {
              user_id : data.id_str,
              access_token : vAccessToken
            };

            var vContactMediaDefer = $q.defer();

            me.getTweets(vUserData).success(function(data) {

              var json = JSON.stringify(data);
              var vMediaPhotos = [];

              if(json.indexOf("place") != -1){

                angular.forEach(data,function(response){

                  var vEntities = JSON.stringify(response.entities);
                  var vGeo = JSON.stringify(response.geo);

                  if(vEntities.indexOf("media") != -1 && vGeo.indexOf("coordinates") != -1){

                    vMediaPhotos.push(response);

                  }

                });

                if (vMediaPhotos.length > 0) {

                  vContactMediaDefer.resolve(vMediaPhotos);

                }else{

                  vContactMediaDefer.resolve("[]");

                }

              }else{
                vContactMediaDefer.resolve("[]");
              }

            }).error(function(error){

              vContactMediaDefer.resolve("[]");

            });

            vDataPhotos.push(vContactMediaDefer.promise);
      
        });

        $q.all(vDataPhotos).then(function(result) {
        
            var tmp = [];
            var dataPhotos = [];

            angular.forEach(result,function(data){

              var valida = JSON.stringify(data);

              if(valida.indexOf("created_at") >-1 && data !="[]" && data.length > 0){

                tmp.push(data);

              }

            });

            angular.forEach(tmp,function(data){

              angular.forEach(data,function(response){

                 dataPhotos.push(response);

              });

            });

            return dataPhotos;

        }).then(function(response){

            var vDataFinal = [];

            angular.forEach(response,function(data){

                var vFeed = data;
                var vDataDefer = $q.defer();

                UserAccountFactory.existsPerfilSocial(vFeed.user.id_str).success(function(data) {

                  var vPerfilAccount = data;

                  if (vPerfilAccount.length > 0) {

                   // vFeed.user.visible_radius = vPerfilAccount[0].visibleRadius;

                   vFeed.user.visible_radius = 500;

                  }else{

                   vFeed.user.visible_radius = 500;

                  }

                  var vGeoData = {
                    unit : "M",
                    lat1 : vData.lat,
                    lon1 : vData.lng,
                    lat2 : vFeed.geo.coordinates[0],
                    lon2 : vFeed.geo.coordinates[1]

                  };

                  var vDistancia = GeoFactory.calculaDistancia(vGeoData);

                  vFeed.estado = "0";//dentro de visible radius

                  if(vDistancia > vFeed.user.visible_radius){

                    //vFeed.entities.media[0].media_url = ApiUrl+ "/images/no_photo.png";

                    vFeed.estado = "1"; //esta fuera de visible radius

                  }

                  vDataDefer.resolve(vFeed);

                });

                vDataFinal.push(vDataDefer.promise);

            });

            return vDataFinal;

        }).then(function(tmpResult){

            $q.all(tmpResult).then(function(result) {

              return result;

            }).then(function(data){

              deferTweetsFinal.resolve(data);

            });

        });

        return deferTweetsFinal.promise;
    },
    getContacts : function(vData){

        var me = this;
        var vFriendsUrl = [me.getFollowers(vData),me.getFollowing(vData)];
        var deferContacts = $q.defer();

        $q.all(vFriendsUrl).then(function(result) {

          var tmp = [];
          var vFriends = [];

          angular.forEach(result, function(response) {

            angular.forEach(response.data, function(users) {

               angular.forEach(users, function(data) {

                  var vId = JSON.stringify(data);

                  if(vId.indexOf(data.id) != -1){

                    tmp.push(data);

                  }

               });

            });

          });

          //verificamos que no se repitan los contactos
          angular.forEach(tmp, function(data) {

            var vId = JSON.stringify(vFriends);

            if(vId.indexOf(data.id_str) == -1){

              vFriends.push(data);
            }

          });

          return vFriends;

        }).then(function(tmpResult) {

          deferContacts.resolve(tmpResult);

        });

        return deferContacts.promise;

    }
  };

})

.factory('InstagramFactory',function($http,ApiSocialUrl,ApiUrl,$q,GeoFactory,UserAccountFactory,UserFactory){

  return{

    getUserDataLogin : function(vData){
      return $http.get('https://api.instagram.com/v1/users/'+vData.user_id+'/?access_token='+vData.access_token);
    }, 
   /* getFollowing : function(vData){
      return $http.post(ApiSocialUrl + '/instagram/following',vData); 
    },
    getFollowers : function(vData){
      return $http.post(ApiSocialUrl + '/instagram/followers',vData); 
    },*/
    getFollowers : function(vData){
      
      var deferMedia = $q.defer();

      var followersInsta = function(response, params) {
       
        deferMedia.resolve(response);

      };

      var fetcher = new Instafetch('3d2337eca9ed4277a07962a821579891',vData.access_token,$http);

      fetcher.followers({
        user: vData.user_id,
        limit: 10000,
        callback: followersInsta,
        params: 'instafollowers'
      });

      return deferMedia.promise;

    },
    getFollowing : function(vData){
      
      var deferMedia = $q.defer();

      var followingInsta = function(response, params) {
       
        deferMedia.resolve(response);

      };

      var fetcher = new Instafetch('3d2337eca9ed4277a07962a821579891',vData.access_token,$http);

      fetcher.followings({
        user: vData.user_id,
        limit: 5000,
        callback: followingInsta,
        params: 'instafollowings'
      });

      return deferMedia.promise;

    },
    publishStatus : function(vData){
      return $http.post(ApiSocialUrl + '/instagram/publish',vData); 
    },
    mediaSearch : function(vData){
      return $http.post(ApiSocialUrl + '/instagram/media/search',vData); 
    },
    getMediaUser : function(vData){

      var deferMedia = $q.defer();

      var feedInsta = function(response, params) {
       
        deferMedia.resolve(response);

      };

      var fetcher = new Instafetch('3d2337eca9ed4277a07962a821579891',vData.access_token,$http);

      fetcher.fetch({
        user: vData.user_id,
        limit: 10000,
        callback: feedInsta,
        params: 'instafeed'
      });

      return deferMedia.promise;

    },
    getMediaContacts : function(vData){

      return $http.get('https://api.instagram.com/v1/users/'+vData.user_id+"/media/recent/?access_token="+vData.access_token+"&count=33");

    },
    getFriendsLocation : function(vData,vContact,vOpcion){

      var me = this;
      var vFriendsData = [];
      var deferMediaFinal = $q.defer();
        
      var vAccessToken = vData.access_token;
      var vDataPhotos = [];

      angular.forEach(vContact,function(result){

        var vParams = {
          user_id : result.id,
          access_token : vAccessToken
        };

        var vContactMediaDefer = $q.defer();

        if (vOpcion == "perfil") {

          me.getMediaUser(vParams).then(function(data){

            var vMediaPhotos = [];

            if(data!="[]"){

              angular.forEach(data.data,function(feeds){

                if(feeds.location !== null){

                  vMediaPhotos.push(feeds);

                }

              });

              vContactMediaDefer.resolve(vMediaPhotos);

            }else{

              vContactMediaDefer.resolve("[]");

            }
        
          }).catch(function(error){

            //alert("error getMediaUserInstagram " + JSON.stringify(error));

            vContactMediaDefer.resolve("[]");

          });

        }else{

          me.getMediaContacts(vParams).success(function(data){

            var vMediaPhotos = [];

            angular.forEach(data.data,function(feeds){

              if(feeds.location !== null){

                vMediaPhotos.push(feeds);

              }

            });

            vContactMediaDefer.resolve(vMediaPhotos);

          }).error(function(error){

            console.log("error getMediaContactsInstagram " + JSON.stringify(error));

            vContactMediaDefer.resolve("[]");

          });

        }

        vDataPhotos.push(vContactMediaDefer.promise);

      });

      $q.all(vDataPhotos).then(function(result) {

            var tmp = [];

            var dataPhotos = [];

            angular.forEach(result,function(data){

              var valida = JSON.stringify(data);

              if(valida.indexOf("attribution") >-1 && data !="[]" && data.length > 0){

                tmp.push(data);

              }

            });

            angular.forEach(tmp,function(data){

              angular.forEach(data,function(response){

                dataPhotos.push(response);

              });

            });

            return dataPhotos;

      }).then(function(tmpResult) {

            //Listamos todos los feeds de nuestros contactos que tienen geo

            var vDataFinal = [];

            angular.forEach(tmpResult,function(data){

              var vFeed = data;
              var vDataDefer = $q.defer();

              UserAccountFactory.existsPerfilSocial(vFeed.user.id).success(function(data) {

                var vPerfilAccount = data;
        
                if (vPerfilAccount.length > 0) {

                  vFeed.user.visible_radius = vPerfilAccount[0].visibleRadius;

                }else{

                   vFeed.user.visible_radius = 500;

                }

                var vGeoData = {
                  unit : "M",
                  lat1 : vData.lat,
                  lon1 : vData.lng,
                  lat2 : vFeed.location.latitude,
                  lon2 : vFeed.location.longitude

                };

                var vDistancia = GeoFactory.calculaDistancia(vGeoData);

                vFeed.estado = "0";//dentro de visible radius

                if(vDistancia > vFeed.user.visible_radius){

                  //vFeed.images.low_resolution.url = ApiUrl+ "/images/no_photo.png";

                  vFeed.estado = "1"; //esta fuera de visible radius

                }

                vDataDefer.resolve(vFeed);

              });

              vDataFinal.push(vDataDefer.promise);

            });

            return vDataFinal;

      }).then(function(tmpResult){

            $q.all(tmpResult).then(function(result) {

              return result;

            }).then(function(data){

              deferMediaFinal.resolve(data);

            });

      });

      return deferMediaFinal.promise;

    },
    getContacts : function(vData){

      var me = this;
      var vFriendsUrl = [me.getFollowing(vData),me.getFollowers(vData)];
      var deferContacts = $q.defer();

      $q.all(vFriendsUrl).then(function(result) {

        var tmp = [];
        var vFriends = [];

        angular.forEach(result, function(response) {

          angular.forEach(response.data, function(data) {

            var jsonData = JSON.stringify(data);

            if (jsonData.indexOf("username") != -1) {

              //angular.forEach(data, function(result) {

                  tmp.push(data);

             //});

            }

          });

        });

        //verificamos que no se repitan los contactos
        angular.forEach(tmp, function(data) {

          var vId = JSON.stringify(vFriends);

          if(vId.indexOf(data.id) == -1){

            vFriends.push(data);
          }

        });

        return vFriends;

      }).then(function(tmpResult) {

        deferContacts.resolve(tmpResult);

      });

      return deferContacts.promise;
    }

  };

})

.factory('FacebookFactory',function($http,ApiSocialUrl,$q,GeoFactory,UserAccountFactory,UserFactory,ApiUrl,$cordovaFacebook, 
  $ionicLoading,UserMediaFactory){

  return{
   
    publishFeed : function(vData){
      return $http.post(ApiSocialUrl + '/facebook/feed',vData); 
    },
    getTaggableFriends : function(vData){
      return $http.get("https://graph.facebook.com/"+vData.user_id+"/taggable_friends?access_token="+vData.access_token);
    },
    inviteFriends : function(){
      var options = {
        method: "apprequests",
        message: "Come on man, check out my application.",
      };

      $cordovaFacebook.showDialog(options)
      .then(function(success) {
        $ionicLoading.show({ template: "success!", noBackdrop: true, duration: 2000 });

      }, function (error) {
        $ionicLoading.show({ template: "Cancel", noBackdrop: true, duration: 2000 });
      });
    },
    getFriends : function(vData){
      return $http.get(ApiSocialUrl + '/facebook/friends/'+vData.user_id+'/'+vData.access_token); 
    },
    getFeeds : function(vData){
      return $http.get(ApiSocialUrl + '/facebook/feed/'+vData.user_id+'/'+vData.access_token); 
    },
    getFotos : function(vData){
      return $http.get(ApiSocialUrl + '/facebook/photos/'+vData.user_id+'/'+vData.access_token);
    },
    getFoto : function(vData){
      return $http.get(ApiSocialUrl + '/facebook/photo-id/'+vData.photo_id+'/'+vData.access_token);
    },
    getAlbums : function(vData){
      return $http.get(ApiSocialUrl + '/facebook/albums/'+vData.user_id+'/'+vData.access_token);
    },
    getFotosAlbum : function(vData){
      return $http.get(ApiSocialUrl + '/facebook/album/photos/'+vData.album_id+'/'+vData.access_token);
    },
    getFriendsData : function(vData,vFriendsData){

      var deferPhotosPlace = $q.defer();
      var me = this;
      var friendsPlace = [];
      var friendsPromise = [];

      var vAccessToken = vData.access_token;
      var vUserId = "";

      if (vData.id) {
        vUserId = vData.id;
      }

      angular.forEach(vFriendsData, function(data) {

          var deferFriends = $q.defer();

          var userData = data;

          UserAccountFactory.existsPerfilSocial(userData.id).success(function(perfil) {

            if(perfil.length > 0){
              deferFriends.resolve(perfil);
            }
            else{
              deferFriends.resolve("[]");
            }

          });

          friendsPromise.push(deferFriends.promise);
          
      });

      $q.all(friendsPromise).then(function(result) {

          var tmp = [];

          angular.forEach(result, function(data) {

            var valida = JSON.stringify(data);

            if (valida.indexOf("id") > -1) {

              tmp.push(data);

            }
            
          });

          return tmp;

      }).then(function(result){

          if(result.length === 0){
            deferPhotosPlace.resolve("[]");
          }

          var userFeed = [];

          angular.forEach(result, function(data) {

            var vUser = data;

            var dataDefer = $q.defer();

            var vUserData = {
              user_id : vUser[0].idPerfil,
              access_token : vAccessToken
            };

            var vFriends = {
              name : '',
              id : vUser[0].idPerfil,
              feed : ''
            };

            UserFactory.getUser(vUser[0].userId).success(function(user) {

              var deferFeed = $q.defer();
              var deferPhotos = $q.defer();
              var deferAlbumPhotos = $q.defer();

              user[0].picture = vUser[0].picture;

              vFriends.name = user[0].firstName;

              //alert("UserFactory.getUser " + JSON.stringify(vUserData));

              me.getFeeds(vUserData).success(function(data) {

                  var fotoIds = [];

                  angular.forEach(data.data, function(dataFeed) {

                    var feed = JSON.stringify(dataFeed);

                    if (feed.indexOf("object_id") > -1) {
                        fotoIds.push(dataFeed.object_id);
                    }

                  });

                 // console.log("feeds  " + user[0].firstName+ " " +JSON.stringify(data));
              
                  deferFeed.resolve(fotoIds);
              });

              me.getFotos(vUserData).success(function(data) {

                var fotoIds = [];

                angular.forEach(data.data, function(fotos) {

                  fotoIds.push(fotos.id);

                });

                //alert("fotos " + user[0].firstName+ " " +JSON.stringify(data));

                deferPhotos.resolve(fotoIds);
              });

              me.getAlbums(vUserData).success(function(data, status, headers, config) {

                var albumsId = [];
                var fotoIds = [];  

                //console.log("albums  " + user[0].firstName+ " " +JSON.stringify(data));

                angular.forEach(data.data, function(album) {

                  if(album.count > 0){

                    albumsId.push(album.id);

                  }

                });

                angular.forEach(albumsId, function(album) {

                  var vAlbumData = {
                    access_token : vAccessToken,
                    album_id : album
                  };

                  me.getFotosAlbum(vAlbumData).success(function(data, status, headers, config) {                  

                    angular.forEach(data.data, function(foto) {

                      fotoIds.push(foto.id);

                      //console.log("albums foto id  " + album+ " " +JSON.stringify(fotoIds));

                    });

                    deferAlbumPhotos.resolve(fotoIds);
                  });

                  

                });
                
              });

              setTimeout(function(){

                var vIdsPhotos = [deferFeed.promise,deferPhotos.promise,deferAlbumPhotos.promise];

                $q.all(vIdsPhotos).then(function(result) {

                  var tmp = [];

                  angular.forEach(result, function(response) {

                    angular.forEach(response, function(data) {

                    if (tmp.indexOf(data) == -1) {
                      tmp.push(data);
                    }

                  });

                });

                  return tmp;

                }).then(function(tmpResult) {

                  var vFotoPlaceData = [];

                  angular.forEach(tmpResult, function(response) {

                    var vFotoId = {
                      photo_id : response,
                      access_token : vAccessToken
                    };

                    var deferFotoPlace = $q.defer();

                    var vMediaParams = {

                      media : response,
                      social : 'facebook'

                    };

                    me.getFoto(vFotoId).success(function(data, status, headers, config) {

                      UserMediaFactory.getMedia(vMediaParams).success(function(mediaData){

                        if (mediaData.length > 0) {

                         //console.log("media " + JSON.stringify(mediaData));

                         var vPlace = {
                            location : {
                              longitude : mediaData[0].lng,
                              latitude : mediaData[0].lat
                            }
                         };

                         data.place = vPlace;

                         deferFotoPlace.resolve(data);

                        }else{

                          deferFotoPlace.resolve(data);

                        }

                      });

                    });

                    vFotoPlaceData.push(deferFotoPlace.promise);

                  });

                  $q.all(vFotoPlaceData).then(function(result) {

                    var tmp = [];

                    angular.forEach(result, function(response) {

                      var data = JSON.stringify(response);

                      if (data.indexOf("place") > -1) {

                        tmp.push(response);

                      }

                    });

                    return tmp;

                  }).then(function(tmpResult) {

                    var dataPhoto = [];

                    angular.forEach(tmpResult, function(response) {

                      var vGeoData = {
                        unit : "M",
                        lat1 : vData.lat,
                        lon1 : vData.lng,
                        lat2 : response.place.location.latitude,
                        lon2 : response.place.location.longitude
                      };

                      var vDistancia = GeoFactory.calculaDistancia(vGeoData);

                      // console.log("Place:"+ response.place.name + " "+ vDistancia);

                      var vPhotoData = {
                        id : response.id,
                        place : response.place,
                        name : '',
                        image : '',
                        estado : '0', //0 = dentro del rango , 1 = fuera de rango (visible radius)
                        visible_radius : ''
                        };

                        var json = JSON.stringify(response);

                        if (json.indexOf("name") > -1) {
                          vPhotoData.name = response.name;
                        }

                        var photosSize = [];

                        if (user[0].visibleRadius === null) {
                          user[0].visibleRadius = 500;
                        }

                        //if((vDistancia >= 500 && vDistancia <= user[0].visibleRadius) || vUserId !==""){
                            //Hay fotos que no tienen extension, obtenemos los correctos

                            angular.forEach(response.images, function(data) {

                              var source = data.source;

                              //if (data.width >= 300 && source.match(/\.(jpg)$/) ) {

                                photosSize.push(data);

                             //}

                            });

                        /*}else{

                          var vNoFoto = {
                            //source : vPerfilAccount[0].picture,
                            source : ApiUrl+ "/images/no_photo.png",
                            width : 500
                          };

                          photosSize.push(vNoFoto);
                          vPhotoData.estado = "1";

                        }*/

                        vPhotoData.image = photosSize;
                        vPhotoData.visible_radius = user[0].visibleRadius;

                        dataPhoto.push(vPhotoData);
                        
                    });

                    vFriends.feed = dataPhoto;
                    vFriends.user = user;

                    // friendsPlace.push(vFriends);

                    dataDefer.resolve(vFriends);

                });

              });

              },6000);

       

            });

            userFeed.push(dataDefer.promise);

          });


          $q.all(userFeed).then(function(result) {

            return result;

          }).then(function(result){

            deferPhotosPlace.resolve(result);

          });

      });

      return deferPhotosPlace.promise;
    },
    getContactInfo: function(vData){
      return $http.get("https://graph.facebook.com/"+vData.user_id, {params: {access_token: vData.access_token, fields: "id,name,gender,location,picture.type(large),email", format: "json" }});
    }
  };

})

;