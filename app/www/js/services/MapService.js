angular.module('service.map', [])

.factory('MapFactory',function($http,$q,$ionicLoading){

	return{

		onGeolocationError : function(error){

			var message = 'GPS problem. Try again.';

			switch(error.code) {
				case error.PERMISSION_DENIED:
				message = "User denied the request for Geolocation. Please enable your GPS and restart the application";
				break;
				case error.POSITION_UNAVAILABLE:
				message = "Location information is unavailable. Please enable your GPS and restart the application";
				break;
				case error.TIMEOUT:
				message = "The request to get user location timed out. Please restart the application";
				break;
				case error.UNKNOWN_ERROR:
				message = "An unknown error occurred. Please restart the application";
				break;
			}

			$ionicLoading.show({ template: message, noBackdrop: true, duration: 2000 });
		},
		mapBounds : function(map,bounds,markerCluster,markersArray){

			map.fitBounds(bounds);
   
      		map.setZoom(2);

      		console.log("markersArray " + markersArray.length);

      		if (markerCluster !== "") {
      			markerCluster = new MarkerClusterer(map,markersArray);
      		}
		},
		searchLocation : function(inputSearch,searchBox,map){

			if(inputSearch.value !==""){
				var places = searchBox.getPlaces();

				places.forEach(function(place) {

					var lat = place.geometry.location.lat();
					var lng = place.geometry.location.lng();

					var myLatLng = new google.maps.LatLng(lat, lng);

					map.setCenter(myLatLng);

					map.setZoom(13);

				});
			}
		},
		centerOnMe : function(position,noCenter,map,markerMe){

			var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

			if (markerMe !==0) {

				map.setCenter(myLatLng);

			} else {

				markerMe = new google.maps.Marker({
					position: myLatLng,
					map: map,
					title: 'You are here',
					icon : 'img/blue_marker.png'
				});

				var infoWindowMe = 0;

				google.maps.event.addListener(markerMe, "click", function() {
					if (infoWindowMe) infoWindowMe.close();

					infoWindowMe = new google.maps.InfoWindow({
						content: "<div style='font-weight:bold'>You  are here</div>"


					});

					infoWindowMe.open(map, markerMe);

				});
			}

			if( ! noCenter ){
				map.setZoom(10);
				map.setCenter(myLatLng);
			}
		},
		addPin : function(lat, lng, name, id, type, content, icon,radius,status,markersArray,circle,map){

			var infowindow = 0;
			//var circle = null;
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(lat, lng),
				map: map,
				title: name,
				icon: icon,
				radius : radius,
				lat : lat,
				lng : lng,
				status : status,
				animation: google.maps.Animation.DROP
			});

			marker.type = type;

			google.maps.event.addListener(marker, "click", function() {
				if (infowindow) infowindow.close();

				infowindow = new google.maps.InfoWindow({
					content: content
					//maxWidth: 300,
				});

				if (marker.status == "1") {

					circle = new google.maps.Circle({
						strokeColor: '#FF0000',
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: '#FF0000',
						fillOpacity: 0.35,
						map: map,
						center: {lat: marker.lat, lng: marker.lng},
						radius: marker.radius
					});

					circle.bindTo('center', marker, 'position');

				}

				infowindow.open(map, marker);

				google.maps.event.addListener(infowindow, 'closeclick', function() {

					if (circle !== null) {
						circle.setMap(null);
					}

				});

			});

			markersArray.push(marker);

			return circle;
		},
		infoMarkerFacebook : function(feed,friends,user,link){

			var styleImage = "";
			var content = "";
			var picture = "";
			var linkProfile = "";

			 angular.forEach(feed.image, function(photo) {

              picture = photo.source;

            });

            if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

              //styleImage = "max-height:85%;";

              styleImage = "max-width:80%;";

            }else{

              styleImage = "max-width:45%;";

            }

            if (link == "si") {
				linkProfile = "#/app/user/"+friends.id +"/social/facebook";
			}

            content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
            content += "<div  style='display:block;font-weight:bold;color: #000'>";

            if(feed.estado == "0"){

              if (typeof(feed.name) != "undefined") {
                content += "<div style='display:block;'>"+feed.name+"</div>";
              }

              content += "<div style='display:block;text-align:center'><img src='" + user[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='#' target='_blank'>" + friends.name + "</a><br/></div>";

            }else{

              content += "<div style='display:block;padding-bottom: 10px;text-align: center;'>to view this photo you need to be in </br> radius of "+feed.visible_radius+"m from this photo.</div>";

              content += "<div style='display:block;text-align:center'><img src='" + user[0].picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px' href='"+linkProfile+"' nav-transition='none'>" + friends.name + "</a><br/></div>";

            }

            content += "</div></div>";

            return content;
		},
		infoMarkerTwitter : function(photos,link){

			var content = "";
			var picture = "";
			var linkProfile = "";

			picture = photos.entities.media[0].media_url ;

			var styleImage = "";

			if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {

				styleImage = "max-height:85%;";

			}else{

				styleImage = "max-width:45%;";

			}

			if (link == "si") {
				linkProfile = "#/app/user/"+photos.user.id_str+"/social/twitter";
			}

			content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_image_url + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px;font-weight: 600;' href='"+linkProfile+"' nav-transition='none'>" + photos.user.name+ "</a><br/></div>";

			content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' height='auto' style='"+styleImage+"' border='0'></div>";
			content += "<div  style='display:block;font-weight:bold;color: #000'>";

			if(photos.estado == "0"){

				content += "<div style='display:block;'>"+photos.text+"</div>";

			}else{

				content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of "+photos.user.visible_radius+"m from this photo.</div>";
			}

			content += "</div></div>";

			return content;
		},
		infoMarkerInstagram : function(photos,link){

			var styleImage = "";
			var content = "";
			var linkProfile = "";
			var picture = photos.images.low_resolution.url ? photos.images.low_resolution.url : "";

			if (ionic.Platform.isWebView() && ionic.Platform.isAndroid()) {
				styleImage = "max-height:85%;";
			}else{
				styleImage = "max-width:45%;";
			}

			if (link == "si") {
				linkProfile = "#/app/user/"+photos.user.id +"/social/instagram";
			}

			content += "<div style='display:block;text-align:center'><img src='" + photos.user.profile_picture + "' height='auto' width='40' border='0'><a style='color: #2ba6cb;position:relative;left:5px;top:-10px'  href='"+linkProfile+"' nav-transition='none' >" + photos.user.username + "</a><br/></div>";
			content += "<div style ='display:block;'><div style ='display:block;text-align:center'><img src='" + picture + "' border='0' style='"+styleImage+"'></div>";
			content += "<div  style='display:block;font-weight:bold;color: #000'>";

			if(photos.estado == "0"){
				if (photos.caption !== null) {
					content += "<div style='display:block;text-align:center'>"+photos.caption.text +"</div>";
				}
			}else{
				content += "<div style='display:block;padding-bottom: 10px;text-align:center'>to view this photo you need to be in </br> radius of "+photos.user.visible_radius+"m from this photo.</div>";
			}

			content += "</div></div>";

			return  content;

		}

	};

})

;