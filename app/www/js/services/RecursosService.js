angular.module('service.recursos', [])

.factory('GeoFactory',function($http,ApiSocialUrl,$q){

  return{
    calculaDistancia1 : function(vData){

      var radlat1 = Math.PI * vData.lat1/180;
      var radlat2 = Math.PI * vData.lat2/180;
      var radlon1 = Math.PI * vData.lon1/180;
      var radlon2 = Math.PI * vData.lon2/180;
      var theta = vData.lon1-vData.lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      if (vData.unit=="K") { dist = dist * 1.609344; }
      if (vData.unit=="N") { dist = dist * 0.8684 ;}
      return dist;
    },
    calculaDistancia : function(vData){

     var ini = new google.maps.LatLng(vData.lat1, vData.lon1); 
     var fin = new google.maps.LatLng(vData.lat2, vData.lon2); 

     var vDistancia = google.maps.geometry.spherical.computeDistanceBetween(ini, fin); 

     return vDistancia;
    },
    gpsSexaToDecimal : function(dec){
      var str="";
      var deg=0, mnt=0, sec=0;
      dec=Math.abs(dec);
      deg=Math.floor(dec);
      dec=(dec-Math.floor(dec))*60;
      mnt=Math.floor(dec);
      dec=(dec-Math.floor(dec))*60;
      sec=Math.floor(dec*100)/100;
      str+=deg+" "+mnt+"' "+sec+"\"";
      return str;
    },
    gpsDecimalToSexa : function(){

      var deg=0;
      var regDigit = ".0123456789";
      var word="";
      var found;
      var pond;

      for(var n=1; n<=3; n++ )
      {
        pond=1; //ponderability, divide by 60 or 3600
        found=false;
        for(var i=0; i<str.length; i++ )
          if(regDigit.indexOf(str.charAt(i))!=-1)
          {
            found=true;
            break;
          }
        if(!found) return deg;  // no more digits?
        
        str=str.substring(i,str.length);    // left trimming
        //find word end
        for(i=0; i<str.length; i++) if( regDigit.indexOf(str.charAt(i))==-1 )
        {
          switch(str.charAt(i))
          {
            case "'" : pond=60;
            break;
            case "\"": pond=3600;
          }
          break;
        }
        word=str.substring(0,i);
        str=str.substring(i,str.length);    //left trim 
        //find the degree type: deg, minute or second
        if(pond==1)
        {
          if(n==2) pond=60;
          if(n==3) pond=3600;
        }
        if(word === "") {deg=0;} else deg+=parseFloat(word)/pond;
      }

      if (direccion == "W" || direccion == "S") {
        deg = deg * -1;
      }
      return deg;

    }

  };

})

.factory('MailFactory',function($http,ApiUrl,$q){

  return {
    registerNotify : function(vData){
      return $http.post(ApiUrl + '/mail/send',vData); 
    }
  };
})

.factory('CameraFactory',function($q){

  return {
    getPicture: function(options) {
      var q = $q.defer();
      
      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);
      
      return q.promise;
    }
  };

})

.factory('ImageFactory',function($q){

  return {
    getPictureFullSize: function(vUrl,vSocial) {

      var vFoto = "";

      switch(vSocial) {

        case 'facebook':
        break;

        case 'twitter':
        vFoto = vUrl.replace("_normal", "");
        break;

        case 'instagram':
        vFoto = vUrl.replace("/s150x150/", "/");
        break;

      }

      return vFoto;
 
    }
  };

})

.factory('ConnectivityFactory', function($rootScope, $cordovaNetwork){
 
  return {
    isOnline: function(){
      if(ionic.Platform.isWebView()){
        return $cordovaNetwork.isOnline();    
      } else {
        return navigator.onLine;
      }
    },
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();    
      } else {
        return !navigator.onLine;
      }
    },
    startWatching: function(){
        if(ionic.Platform.isWebView()){
 
          $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            console.log("went online");
          });
 
          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("went offline");
          });
 
        }
        else {
 
          window.addEventListener("online", function(e) {
            console.log("went online");
          }, false);    
 
          window.addEventListener("offline", function(e) {
            console.log("went offline");
          }, false);  
        }       
    }
  };
})

.factory('FileFactory',function($http,ApiUrl){
  return{
    deleted : function(vData){
      return $http.delete(ApiUrl + '/file/public/images/'+vData); 
    }
    
  };

})
;