/*
Navicat MySQL Data Transfer

Source Server         : JOSUE
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : db_tada

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2016-02-08 15:55:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `user_media`
-- ----------------------------
DROP TABLE IF EXISTS `user_media`;
CREATE TABLE `user_media` (
  `id_media` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `media` varchar(100) DEFAULT NULL,
  `social` varchar(20) DEFAULT NULL,
  `lat` varchar(150) DEFAULT NULL,
  `lng` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_media`),
  KEY `user_media_id` (`user_id`),
  CONSTRAINT `user_media_id` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_media
-- ----------------------------
