/*
Navicat MySQL Data Transfer

Source Server         : JOSUE
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : db_tada

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2015-10-02 13:46:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `user_email` varchar(85) DEFAULT NULL,
  `user_pass` varchar(45) DEFAULT NULL,
  `visible_radius` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('47', 'Josue', 'Diaz Rimaches', 'jose.rimache12@gmail.com', '', '50000', '0');
INSERT INTO `user` VALUES ('48', 'Kael Taysuk Tenma', 'Diaz Alderete', 'kaeltaysuktenma@gmail.com', '', '28480', '0');

-- ----------------------------
-- Table structure for `user_account`
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `id_perfil` varchar(100) DEFAULT NULL,
  `social_account` varchar(20) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_account_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES ('6', '48', '2216972173', 'instagram', '', '2216972173@tada.com', '');
INSERT INTO `user_account` VALUES ('8', '48', '1673519492868928', 'facebook', 'CAAWZAnYK9oXMBAHw1GuyJNioK7aBrxpYcRGiHR134fcoKJOBN1dqlUOZArKVIqHVtiJfkpqLm1JkOWCrh0EMsybq5OZA8KHwuVNyswEYJQFrpd199RAK8z1QtkY5JmejqzRZBUKCjcSKWQshESgIRhx3VgyJmZCLJ7z8KN9y9J3HHCBVSuqGFpSN7JdaCCmJGHg7nTsoxowZDZD', 'kaeltaysuktenma@gmail.com', 'http://graph.facebook.com/1673519492868928/picture?type=large');
INSERT INTO `user_account` VALUES ('9', '47', '1341023568', 'twitter', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1NjA5YmMyYzc4YjAwNGRjMTljMzM0MTIiLCJpYXQiOjE0NDM4MDIyMDUsImV4cCI6MTQ0NTAxMTgwNX0.PbAg30JXPwi8rANdndJ1v5RkvUrftaYZaAbVM8Gb59s', '1341023568@tada.com', '');
INSERT INTO `user_account` VALUES ('10', '47', '1386634229', 'instagram', '', '1386634229@tada.com', '');
