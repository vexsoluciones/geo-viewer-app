"use strict";

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User",{
     id: {
      type: DataTypes.INTEGER,
      field: 'id',
      primaryKey: true,
      autoIncrement: true
     },
     firstName: {
      type: DataTypes.STRING(20),
      field: 'first_name'
     },
     lastName: {
      type: DataTypes.STRING(20),
      field: 'last_name'
     },
     userEmail: {
      type: DataTypes.STRING(85),
      field: 'user_email'
     },
     userPass: {
      type: DataTypes.STRING(45),
      field: 'user_pass'
     },
     visibleRadius: {
      type: DataTypes.INTEGER,
      field: 'visible_radius'
     },
     status: {
      type: DataTypes.BOOLEAN,
      field: 'status'
     }
     
  }, 
  {
    classMethods: {
        getAll: function(){ return this.findAll(); }
    },

    freezeTableName: true, // Model tableName will be the same as the model 
    nametableName: 'user',
    timestamps: false,
      
  }
   
  );

  //User.sync({force: true});

  return User;
};
