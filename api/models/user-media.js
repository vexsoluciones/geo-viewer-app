"use strict";

module.exports = function(sequelize, DataTypes) {
  var user_media= sequelize.define("user_media",{
     idMedia: {
      type: DataTypes.INTEGER,
      field: 'id_media',
      primaryKey: true,
      autoIncrement: true
     },
     userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      references: {
         model: 'user',
         key: 'id',
         //Solo Postgret
         //deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
     },
     media: {
      type: DataTypes.STRING(100),
      field: 'media'
     },
     social: {
      type: DataTypes.STRING(20),
      field: 'social'
     },
     lat: {
      type: DataTypes.STRING(150),
      field: 'lat'
     },
     lng:{
      type: DataTypes.STRING(150),
      field: 'lng'
     }
  
     
  }, 

  {
    freezeTableName: true, // Model tableName will be the same as the model 
    nametableName: 'user_media',
    timestamps: false,
      
  }
   
  );

  //este comando crea la tabla
  //user_media.sync({force: true});

  return user_media;
};
