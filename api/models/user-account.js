"use strict";

module.exports = function(sequelize, DataTypes) {
  var user_account = sequelize.define("user_account",{
     id: {
      type: DataTypes.INTEGER,
      field: 'id',
      primaryKey: true,
      autoIncrement: true
     },
     userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      references: {
         model: 'user',
         key: 'id',
         //Solo Postgret
         //deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
     },
     idPerfil: {
      type: DataTypes.STRING(100),
      field: 'id_perfil'
     },
     socialAccount: {
      type: DataTypes.STRING(20),
      field: 'social_account'
     },
     accessToken: {
      type: DataTypes.STRING,
      field: 'access_token'
     },
     email:{
      type: DataTypes.STRING,
      field: 'email'
     },
     picture:{
      type: DataTypes.STRING,
      field: 'picture'
     }
  
     
  }, 

  {
    freezeTableName: true, // Model tableName will be the same as the model 
    nametableName: 'user_account',
    timestamps: false,
      
  }
   
  );

  //este comando crea la tabla
  //user_account.sync({force: true});

  return user_account;
};
