var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/get-all',function(req,res){
	models.User.getAll().then(function(data){
	    res.json(200,data);
	});          
});

router.get('/email/:email/password/:password',function(req,res){
	models.User.findAll({
		 where: { 
		 	userEmail: req.params.email,
		 	userPass : req.params.password
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

router.get('/email/:email',function(req,res){
	models.User.findAll({
		 where: { 
		 	userEmail: req.params.email
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

router.get('/id/:id',function(req,res){
	models.User.findAll({
		 where: { 
		 	id: req.params.id
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

router.post('',function(req,res){

	models.User.create({
		firstName: req.body.first_name,
		lastName: req.body.last_name,
		userEmail: req.body.user_email,
		userPass: req.body.user_pass,
		fbToken: req.body.fb_token,
		instaToken: req.body.insta_token,
		twitToken: req.body.twit_token,
		visibleRadius: req.body.visible_radius,
		status: req.body.status
	}).then(function() {
		res.json(200,"ok");
	});   
});

router.put('',function(req,res){

	models.User.update({
		firstName: req.body.first_name,
		lastName: req.body.last_name,
		userEmail: req.body.user_email,
		userPass: req.body.user_pass,
		fbToken: req.body.fb_token,
		instaToken: req.body.insta_token,
		twitToken: req.body.twit_token,
		visibleRadius: req.body.visible_radius,
		status: req.body.status
		}, 
		{
		where: {
			id: req.body.id
		}
	}).then(function(data){
		res.json(200,"ok");
	});

});

module.exports = router;
