var express = require('express');
var router = express.Router();

var path = require('path');
var qs = require('querystring');
var async = require('async');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var colors = require('colors');
var logger = require('morgan');
var jwt = require('jwt-simple');
var moment = require('moment');
//var mongoose = require('mongoose');
var request = require('request');
var FormData = require('form-data');

var config = require('../config');

var Twitter = require('twitter');

var Instagram = require('instagram-node').instagram();

Instagram.use({ 
  client_id: config.INSTAGRAM_CLIENT_ID,
  client_secret: config.INSTAGRAM_CLIENT_SECRET
});


/*
 |--------------------------------------------------------------------------
 | Login with Twitter
 |--------------------------------------------------------------------------
 */
router.post('/auth/twitter', function(req, res) {
  var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
  var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
  var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';

  // Part 1 of 2: Initial request from Satellizer.
  if (!req.body.oauth_token || !req.body.oauth_verifier) {
    var requestTokenOauth = {
      consumer_key: config.TWITTER_KEY,
      consumer_secret: config.TWITTER_SECRET,
      callback: req.body.redirectUri
    };

    // Step 1. Obtain request token for the authorization popup.
    request.post({ url: requestTokenUrl, oauth: requestTokenOauth }, function(err, response, body) {
      var oauthToken = qs.parse(body);

      // Step 2. Send OAuth token back to open the authorization screen.
      res.send(oauthToken);
    });
  } else {
    // Part 2 of 2: Second request after Authorize app is clicked.
    var accessTokenOauth = {
      consumer_key: config.TWITTER_KEY,
      consumer_secret: config.TWITTER_SECRET,
      token: req.body.oauth_token,
      verifier: req.body.oauth_verifier
    };

    // Step 3. Exchange oauth token and oauth verifier for access token.
    request.post({ url: accessTokenUrl, oauth: accessTokenOauth }, function(err, response, accessToken) {

      accessToken = qs.parse(accessToken);

      var profileOauth = {
        consumer_key: config.TWITTER_KEY,
        consumer_secret: config.TWITTER_SECRET,
        oauth_token: accessToken.oauth_token
      };

      console.log("data token login"+ JSON.stringify(accessToken));

      res.send({ token: accessToken.oauth_token, id : accessToken.user_id, token_secret : accessToken.oauth_token_secret});
    });
  }
});


/*
 |--------------------------------------------------------------------------
 | Perfil data twitter
 |--------------------------------------------------------------------------
 */

 router.get('/twitter/me/:user_id/accessToken/:access_token',function(req,res){

    var profileOauth = {
        consumer_key: config.TWITTER_KEY,
        consumer_secret: config.TWITTER_SECRET,
        oauth_token: req.params.access_token
    };

    request.get({
        url: 'https://api.twitter.com/1.1/users/show.json?user_id='+req.params.user_id,
        oauth: profileOauth,
        json: true
      }, function(err, response, data) {

        res.json(200,data);

      });  
});


/*
 |--------------------------------------------------------------------------
 | Lista de Seguidores twitter
 |--------------------------------------------------------------------------
 */

 router.post('/twitter/followers',function(req,res){

    var profileOauth = {
        consumer_key: config.TWITTER_KEY,
        consumer_secret: config.TWITTER_SECRET,
        oauth_token: req.body.access_token
    };

    request.get({
        url: 'https://api.twitter.com/1.1/followers/list.json?cursor=-1&user_id='+req.body.user_id+'&count=5000',
        oauth: profileOauth,
        json: true
      }, function(err, response, data) {

        res.json(200,data);

      });  
});

 /*
 |--------------------------------------------------------------------------
 | Lista de Personas que sigues en twitter
 |--------------------------------------------------------------------------
 */

 router.post('/twitter/following',function(req,res){

    var profileOauth = {
        consumer_key: config.TWITTER_KEY,
        consumer_secret: config.TWITTER_SECRET,
        oauth_token: req.body.access_token
    };

    request.get({
        url: 'https://api.twitter.com/1.1/friends/list.json?cursor=-1&user_id='+req.body.user_id+'&count=5000',
        oauth: profileOauth,
        json: true
      }, function(err, response, data) {

        res.json(200,data);

      });  
});

 /*
 |--------------------------------------------------------------------------
 | Search tweets 
 |--------------------------------------------------------------------------
 */

 router.post('/twitter/tweets/search',function(req,res){

    var profileOauth = {
        consumer_key: config.TWITTER_KEY,
        consumer_secret: config.TWITTER_SECRET,
        oauth_token: req.body.access_token
    };

    request.get({
        url: 'https://api.twitter.com/1.1/search/tweets.json?geocode='+req.body.geocode,
        oauth: profileOauth,
        json: true
      }, function(err, response, data) {

        res.json(200,data);

      });  
});


 /*
 |--------------------------------------------------------------------------
 | Obtener Tweets
 |--------------------------------------------------------------------------
 */

router.post('/twitter/tweets',function(req,res){

    var profileOauth = {
        consumer_key: config.TWITTER_KEY,
        consumer_secret: config.TWITTER_SECRET,
        oauth_token: req.body.access_token
    };

    request.get({
        url: 'https://api.twitter.com/1.1/statuses/user_timeline.json?user_id='+req.body.user_id+'&count=100',
        oauth: profileOauth,
        json: true
      }, function(err, response, data) {

        res.json(200,data);

      });  
});

/*
 |--------------------------------------------------------------------------
 | Obtener Data de un Tweet
 |--------------------------------------------------------------------------
 */

 router.post('/twitter/show',function(req,res){

    var profileOauth = {
        consumer_key: config.TWITTER_KEY,
        consumer_secret: config.TWITTER_SECRET,
        oauth_token: req.body.access_token
    };

    request.get({
        url: 'https://api.twitter.com/1.1/statuses/show.json?id='+req.body.id,
        oauth: profileOauth,
        json: true
      }, function(err, response, data) {

        res.json(200,data);

      });  
});

  /*
 |--------------------------------------------------------------------------
 | Publicar status en  twitter
 |--------------------------------------------------------------------------
 */
router.post('/twitter/status',function(req,res){
 
  var clientTwitter = new Twitter({
    consumer_key: config.TWITTER_KEY,
    consumer_secret: config.TWITTER_SECRET,
    access_token_key: req.body.access_token,
    access_token_secret: req.body.access_token_secret
  });

  if (req.body.status_photo !== "" && req.body.photos !== "") {

    var dataImage = require('fs').readFileSync('./public/images/'+req.body.photos);

    clientTwitter.post('media/upload', {media: dataImage}, function(error, media, response){

      if (!error) {

        console.log(media);

        var status = {
          status: req.body.status_photo,
          media_ids: media.media_id_string,
          lat : req.body.lat,
          long : req.body.long,
          display_coordinates : true
        };

        clientTwitter.post('statuses/update', status, function(error, tweet, response){

          if (!error) {
            console.log(tweet);
            res.json(200,tweet);
          }
        });

      }

    });

  }else{

    console.log("status");

    clientTwitter.post('statuses/update', {status: req.body.message},  function(error, tweet, response){
      if(error) res.json(500,error);
      res.json(200,response);
    });

  }

});


 /*
 |--------------------------------------------------------------------------
 | Lista de amigos Facebook (app)
 |--------------------------------------------------------------------------
 */
router.get('/facebook/friends/:user_id/:access_token',function(req,res){

   var profileOauth = {
    access_token : req.params.access_token,

   };

    request.get({
        url: 'https://graph.facebook.com/'+req.params.user_id+'/friends?fields=picture,name,email,location',
        qs: profileOauth,
        json: true
      }, function(err, response, data) {
        res.json(200,data);
      }); 
});

 /*
 |--------------------------------------------------------------------------
 | Lista de Feeds usuario (app)
 |--------------------------------------------------------------------------
 */

 router.get('/facebook/feed/:user_id/:access_token',function(req,res){

   var profileOauth = {
    access_token : req.params.access_token,

   };

    request.get({
        url: 'https://graph.facebook.com/'+req.params.user_id+'/feed?fields=object_id,name',
        qs: profileOauth,
        json: true
      }, function(err, response, data) {

        console.log("feed data" + JSON.stringify(data));
        console.log("feed err" + JSON.stringify(err));
        console.log("feed response" + JSON.stringify(response));

        res.json(200,data);
      }); 
});


/*
 |--------------------------------------------------------------------------
 | Lista de Photos usuario (app)
 |--------------------------------------------------------------------------
 */

 router.get('/facebook/photos/:user_id/:access_token',function(req,res){

   var profileOauth = {
    access_token : req.params.access_token,

   };

    request.get({
        url: 'https://graph.facebook.com/'+req.params.user_id+'/photos',
        qs: profileOauth,
        json: true
      }, function(err, response, data) {
        res.json(200,data);
      }); 
});

 /*
 |--------------------------------------------------------------------------
 | Obtener data de una Photo del usuario (app)
 |--------------------------------------------------------------------------
 */

  router.get('/facebook/photo-id/:photo_id/:access_token',function(req,res){

   var profileOauth = {
    access_token : req.params.access_token,

   };

    request.get({
        url: 'https://graph.facebook.com/'+req.params.photo_id+'?fields=place,link,images,name',
        qs: profileOauth,
        json: true
      }, function(err, response, data) {
        res.json(200,data);
      }); 
});

/*
 |--------------------------------------------------------------------------
 | Lista de Albums usuario (app)
 |--------------------------------------------------------------------------
 */

 router.get('/facebook/albums/:user_id/:access_token',function(req,res){

   var profileOauth = {
    access_token : req.params.access_token,

   };

    request.get({
        url: 'https://graph.facebook.com/'+req.params.user_id+'/albums?fields=count,name',
        qs: profileOauth,
        json: true
      }, function(err, response, data) {
        res.json(200,data);
      }); 
});

 /*
 |--------------------------------------------------------------------------
 | Obtener fotos del Album (app)
 |--------------------------------------------------------------------------
 */

router.get('/facebook/album/photos/:album_id/:access_token',function(req,res){

   var profileOauth = {
    access_token : req.params.access_token,

   };

    request.get({
        url: 'https://graph.facebook.com/'+req.params.album_id+'/photos?fields=from,name',
        qs: profileOauth,
        json: true
      }, function(err, response, data) {
        res.json(200,data);
      }); 
});

 /*
 |--------------------------------------------------------------------------
 | Publicar en  facebook
 |--------------------------------------------------------------------------
 */

router.post('/facebook/feed',function(req,res){

    if (req.body.status_photo != "" && req.body.photos !="") {

      var dataImage = require('fs').createReadStream('./public/images/'+req.body.photos);

      var profileOauth = {
        access_token : req.body.access_token,
        no_story : true
      };

      var photo = {
        source : dataImage
      };

      request.post({
        url: 'https://graph.facebook.com/'+req.body.user_id+'/photos?',
        qs: profileOauth,
        formData : photo,
        json: true
     
      },function(err, response, data) {

        console.log(data);

        var vImage = data.id;

        var OAuth = {
          access_token : req.body.access_token
        };

        var vData = {
          message : req.body.status_photo,
          object_attachment : data.id
        };

        request.post({
          url: 'https://graph.facebook.com/'+req.body.user_id+'/feed?',
          qs: OAuth,
          formData :vData,
          json: true
        },function(err, response, data) {

          res.json(200,vImage);

        });

      }); 

    }else{

     var profileOauth = {
        access_token : req.body.access_token
      };

     request.post({
      url: 'https://graph.facebook.com/'+req.body.user_id+'/feed?message='+req.body.message,
      qs: profileOauth,
      json: true
    },function(err, response, data) {

      res.json(200,data);

    });  

    }


});

/* Siguiendo Instagram*/

router.post('/instagram/following',function(req,res){

    request.get({
        url: 'https://api.instagram.com/v1/users/'+req.body.user_id+'/follows?access_token='+req.body.access_token+"&count=500",
        //oauth: profileOauth,
        json: true
      }, function(err, response, profile) {

        res.json(200,profile);

      });
       
});

/* Seguidores Instagram*/

router.post('/instagram/followers',function(req,res){

    request.get({
        url: 'https://api.instagram.com/v1/users/'+req.body.user_id+'/followed-by?access_token='+req.body.access_token+"&count=500",
        //oauth: profileOauth,
        json: true
      }, function(err, response, profile) {

        res.json(200,profile);

      });
       
});

/*
 |--------------------------------------------------------------------------
 | Search photo instagram
 |--------------------------------------------------------------------------
 */

 router.post('/instagram/media/search',function(req,res){
  
  request.get({
       // url: 'https://api.instagram.com/v1/media/search?',
        url: 'https://api.instagram.com/v1/media/search?lat='+req.body.lat+'&lng='+req.body.lng+'&distance='+req.body.distance+'&access_token='+req.body.access_token,
        //qs : vData,
        json: true
      }, function(err, response, profile) {

        res.json(200,profile);

      });
       
});



/*
 |--------------------------------------------------------------------------
 | Publicar en  Instagram
 |--------------------------------------------------------------------------
 */

router.post('/instagram/publish',function(req,res){

  var dataImage = [];

  req.body.photos.forEach(function(data) {
    dataImage.push(require('fs').readFileSync('./public/images/'+data));
  });

  request.get(
      {
        url: 'https://instagram.com/api/v1/media/upload/'+req.body.user_id+'/follows?access_token='+req.body.access_token,
        json: true
      }, function(err, response, profile) {

        res.json(200,profile);

      });

});

/*
 |--------------------------------------------------------------------------
 | Leer Feed Instagram
 |--------------------------------------------------------------------------
 */

router.get('/instagram/user/media/:user_id/:access_token',function(req,res){
  
  request.get({
       // url: 'https://api.instagram.com/v1/media/search?',
        url: 'https://api.instagram.com/v1/users/'+req.params.user_id+'/media/recent/?&access_token='+req.params.access_token,
        //qs : vData,
        json: true
      }, function(err, response, data) {

        res.json(200,data);

      });
       
});



module.exports = router;
