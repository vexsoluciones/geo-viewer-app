var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.post('',function(req,res){

	models.user_account.create({
		userId: req.body.user_id,
		idPerfil: req.body.id_perfil,
		socialAccount: req.body.social_account,
		accessToken: req.body.access_token,
		email: req.body.email,
		picture : req.body.picture
			
	}).then(function() {
		res.json(200,"ok");
	});   
});

router.put('',function(req,res){

	models.user_account.update({
		
		idPerfil: req.body.id_perfil,
		accessToken: req.body.access_token,
		email: req.body.email,
		picture : req.body.picture
		}, 
		{
		where: {
			userId: req.body.user_id,
			socialAccount: req.body.social_account,
		}
	}).then(function(data){
		res.json(200,"ok");
	});

});

//Verificamos si el usuario tiene una cuenta asociada
router.get('/userId/:userId/social/:social',function(req,res){
	models.user_account.findAll({
		 where: { 
		 	userId: req.params.userId,
		 	socialAccount : req.params.social
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

router.get('/userId/:userId',function(req,res){
	models.user_account.findAll({
		 where: { 
		 	userId: req.params.userId,
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

/* obtenemos la asociacion , activa */

router.get('/userId/:userId/active',function(req,res){
	models.user_account.findAll({
		 where: { 
		 	userId: req.params.userId,
		 	accessToken : {$ne: ""}
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

router.get('/id-perfil/:idPerfil',function(req,res){
	models.user_account.findAll({
		 where: { 
		 	idPerfil: req.params.idPerfil,
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

module.exports = router;
