var express = require('express');
var router = express.Router();
var models = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.post('',function(req,res){

	models.user_media.create({
		userId: req.body.user_id,
		media: req.body.media_id,
		social: req.body.social,
		lat: req.body.lat,
		lng: req.body.lng
	
	}).then(function() {
		res.json(200,"ok");
	});   
});

router.get('/userId/:id/social/:social',function(req,res){
	models.user_media.findAll({
		 where: { 
		 	userId: req.params.id,
		 	social : req.params.social
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});

router.get('/mediaId/:id/social/:social',function(req,res){
	models.user_media.findAll({
		 where: { 
		 	media: req.params.id,
		 	social : req.params.social
		 } 
	}).then(function(data){
	    res.json(200,data);
	});          
});




module.exports = router;
