var express = require('express');
var router = express.Router();

var optionsUpload = {
  tmpDir: __dirname + '/../public/images/tmp',
  uploadDir: __dirname + '/../public/images',
  uploadUrl: '/public/images/',
  storage: {
    type: 'local'
  }
};

var uploader = require('blueimp-file-upload-expressjs')(optionsUpload);

router.post('/upload', function(req, res) {
    uploader.post(req, res, function(obj) {
      res.send(JSON.stringify(obj));
    });
});
 
router.delete('/public/images/:name', function(req, res) {
    uploader.delete(req, res, function(obj) {
      res.send(JSON.stringify(obj));
    });
});

module.exports = router;